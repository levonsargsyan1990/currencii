import { Meteor } from 'meteor/meteor';

import { Rates } from '../rates';

Meteor.publish('rates.list', () => Rates.find({}, {
  fields: Rates.publicFields,
}));

Meteor.publish('rates.listActive', () => Rates.find({ active: true }, {
  fields: Rates.publicFields,
}));

Meteor.publish('rates.getRateByCurrencies', ({
  buyCurrency, sellCurrency,
}) => Rates.find({
  'buy.currency': buyCurrency,
  'sell.currency': sellCurrency,
}, {
  fields: {
    marketRate: 1,
  },
}));
