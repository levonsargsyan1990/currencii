// TODO: updatedAd time to be set automatically

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

class RatesCollection extends Mongo.Collection {
  update(selector, modifier) {
    const result = super.update(selector, modifier);
    return result;
  }
}

export const Rates = new RatesCollection('rates');

// Deny all client-side updates since we will be using methods to manage this collection
Rates.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

export const CurrencySchema = new SimpleSchema({
  currency: {
    type: String,
  },
  fullName: {
    type: String,
    optional: true,
    autoValue() { return this.field('currency').value; },
  },
});

export const RateNumberSchema = new SimpleSchema({
  rate: {
    type: Number,
  },
  updatedAt: {
    type: Date,
    optional: true,
    autoValue() { return new Date(); },
  },
});

export const RatesSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  sell: {
    type: CurrencySchema,
  },
  buy: {
    type: CurrencySchema,
  },
  guaranteedRate: {
    type: RateNumberSchema,
  },
  marketRate: {
    type: RateNumberSchema,
  },
  active: {
    type: Boolean,
    optional: true,
    defaultValue: true,
  },
});

Rates.attachSchema(RatesSchema);

// This represents the keys from Lists objects that should be published
// to the client. If we add secret properties to List objects, don't list
// them here to keep them private to the server.
Rates.publicFields = {
  _id: 1,
  sell: 1,
  buy: 1,
  guaranteedRate: 1,
  marketRate: 1,
  active: 1,
};
