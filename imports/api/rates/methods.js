// Check for duplications when adding and updating rates

import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin';

import { Rates, RatesSchema } from './rates';

export const addRate = new ValidatedMethod({
  name: 'rate.add',
  mixins: [CallPromiseMixin],
  validate: RatesSchema
    .omit('_id')
    .validator(),
  run({
    sell,
    buy,
    guaranteedRate,
    marketRate,
  }) {
    return Rates.insert({
      sell,
      buy,
      guaranteedRate,
      marketRate,
    });
  },
});

export const updateRate = new ValidatedMethod({
  name: 'rate.update',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    rateId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    marketRate: {
      type: Number,
      optional: true,
    },
    guaranteedRate: {
      type: Number,
      optional: true,
    },
  }).validator(),
  // eslint-disable-next-line consistent-return
  run({ rateId, marketRate, guaranteedRate }) {
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        const updateObj = {};
        if (marketRate) {
          updateObj['marketRate.rate'] = marketRate;
          updateObj['marketRate.updatedAt'] = new Date();
        }
        if (guaranteedRate) {
          updateObj['guaranteedRate.rate'] = guaranteedRate;
          updateObj['guaranteedRate.updatedAt'] = new Date();
        }
        return Rates.update({ _id: rateId }, {
          $set: updateObj,
        });
      }
      throw new Meteor.Error('update-rate', 'Access denied');
    }
  },
});

export const enableRate = new ValidatedMethod({
  name: 'rate.enable',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    rateId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator(),
  // eslint-disable-next-line consistent-return
  run({ rateId }) {
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        return Rates.update({ _id: rateId }, {
          $set: { active: true },
        });
      }
      throw new Meteor.Error('enable-rate', 'Access denied');
    }
  },
});

export const disableRate = new ValidatedMethod({
  name: 'rate.disable',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    rateId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator(),
  // eslint-disable-next-line consistent-return
  run({ rateId }) {
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        return Rates.update({ _id: rateId }, {
          $set: { active: false },
        });
      }
      throw new Meteor.Error('disable-rate', 'Access denied');
    }
  },
});

export const getRate = new ValidatedMethod({
  name: 'rate.get',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    sell: {
      type: String,
    },
    buy: {
      type: String,
    },
  }).validator(),
  run({
    sell,
    buy,
  }) {
    if (Meteor.isServer) {
      const rate = Rates.findOne({
        'sell.currency': sell,
        'buy.currency': buy,
      });
      if (!rate) {
        throw new Meteor.Error(
          'rate-error',
          `We are not converting ${sell.toUpperCase()} to ${buy.toUpperCase()} yet`,
        );
      }
      return rate;
    }
    return null;
  },
});

export const getActiveRate = new ValidatedMethod({
  name: 'rate.getActive',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    sell: {
      type: String,
    },
    buy: {
      type: String,
    },
  }).validator(),
  run({
    sell,
    buy,
  }) {
    if (Meteor.isServer) {
      const rate = Rates.findOne({
        'sell.currency': sell,
        'buy.currency': buy,
        active: true,
      });
      if (!rate) {
        throw new Meteor.Error(
          'rate-error',
          `We are not converting ${sell.toUpperCase()} to ${buy.toUpperCase()} at this moment`,
        );
      }
      return rate;
    }
    return null;
  },
});
