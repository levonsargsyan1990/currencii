import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import moment from 'moment';

const { title } = Meteor.settings.public;

export const passwordChanged = {
  subject() { return i18n.__('email.passwordChanged.subject'); },
  content({ name }) { return i18n.__('email.passwordChanged.content', { name }); },
};

export const identityFile = {
  subject({ name, position, company }) { return i18n.__('email.identityFile.subject', { name, position, company }); },
  content({
    url,
    name,
    company,
  }) {
    return i18n.__('email.identityFile.content', { url, name, company });
  },
};

export const businessFile = {
  subject({ name, position, company }) { return i18n.__('email.businessFile.subject', { name, position, company }); },
  content({
    url,
    name,
    company,
  }) {
    return i18n.__('email.businessFile.content', { url, name, company });
  },
};

export const contact = {
  subject() { return i18n.__('email.contact.subject', { title }); },
  content({
    name,
    email,
    message,
  }) {
    return i18n.__('email.contact.content', {
      name, email, message, title,
    });
  },
};

export const verificationApprove = {
  subject({ verificationType }) {
    const type = verificationType === 'business' ? 'Business registration' : 'Identity';
    return i18n.__('email.verificationApprove.subject', { type });
  },
  content({
    name,
    verificationType,
  }) {
    const type = verificationType === 'business' ? 'Business registration' : 'Identity';
    return i18n.__('email.verificationApprove.content', { type, name, title });
  },
};

export const verificationDecline = {
  subject({ verificationType }) {
    const type = verificationType === 'business' ? 'Business registration' : 'Identity';
    return i18n.__('email.verificationDecline.subject', { type });
  },
  content({
    name,
    verificationType,
  }) {
    const type = verificationType === 'business' ? 'Business registration' : 'Identity';
    return i18n.__('email.verificationDecline.content', { type, name, title });
  },
};

export const transactionReceived = {
  subject() {
    return 'Transaction received';
  },
  content({
    name,
    transaction,
  }) {
    const GUARANTEED_HOURS = 24;
    const {
      reference, createdAt, receivedAt, rate, buy,
    } = transaction;
    const createdMoment = moment(createdAt);
    const receivedMoment = moment(receivedAt);
    const hours = moment
      .duration(receivedMoment.diff(createdMoment))
      .asHours();
    const isPeriodPassed = hours > GUARANTEED_HOURS;
    return `
      <div>
        <p>Hello ${name},</p><br />
        <p>
          Your transaction at ${title} with reference number ${reference} has been received. 
          ${isPeriodPassed ? `The guaranteed 24 hour period has already passed, so we will convert the money with current rate: ${rate}.` : ''}
          ${buy.currency.toUpperCase()} ${buy.amount} will be sent to your account. We will notify you as soon as money has been sent.
        </p><br />
        <p>Thanks</p>
      </div>
    `;
  },
};

export const transactionSent = {
  subject() {
    return 'Transaction sent';
  },
  content({
    name,
    transaction,
  }) {
    const {
      reference, bankAccount: { accountNumber }, buy,
    } = transaction;
    return `
      <div>
        <p>Hello ${name},</p><br />
        <p>
          Your transaction at ${title} with reference number ${reference} has been sent.
          You will receive ${buy.currency.toUpperCase()} ${buy.amount} on your bank account: ${accountNumber}.
        </p><br />
        <p>Thanks</p>
      </div>
    `;
  },
};
