import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Email } from 'meteor/email';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin';
import SimpleSchema from 'simpl-schema';

import { Transactions } from '../transactions/transactions';

import {
  passwordChanged,
  identityFile,
  businessFile,
  verificationApprove,
  verificationDecline,
  transactionReceived,
  transactionSent,
  contact,
} from './templates';

const { title, email } = Meteor.settings.public;

export const sendPasswordChangeEmail = new ValidatedMethod({
  name: 'email.sendPasswordChangeEmail',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    const user = Meteor.users.findOne(this.userId);
    if (Meteor.isServer) {
      return Email.send({
        to: user.emails[0].address,
        from: `${title} <${email}>`,
        subject: passwordChanged.subject(),
        html: passwordChanged.content({ name: user.profile.firstName }),
      });
    }
    return user;
  },
});

export const sendIdentityFileEmail = new ValidatedMethod({
  name: 'email.sendIdentityFileEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    name: {
      type: String,
    },
    fileType: {
      type: String,
    },
    file: {
      type: String,
    },
  }).validator(),
  run({ name, file }) {
    const user = Meteor.users.findOne(this.userId);
    const {
      profile: {
        firstName, lastName, position, companyName,
      },
    } = user;
    const fullName = `${firstName} ${lastName}`;
    if (Meteor.isServer) {
      const { private: { admin: { email: adminEmail } } } = Meteor.settings;
      return Email.send({
        to: adminEmail,
        from: `${title} <${email}>`,
        subject: identityFile.subject({
          name: fullName,
          position,
          company: companyName,
        }),
        html: identityFile.content({
          name: fullName,
          company: companyName,
          url: Meteor.absoluteUrl('/admin/verifications'),
        }),
        attachments: [{
          filename: name,
          content: file,
          encoding: 'binary',
        }],
      });
    }
    return user;
  },
});

export const sendBusinessFileEmail = new ValidatedMethod({
  name: 'email.sendBusinessFileEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    name: {
      type: String,
    },
    fileType: {
      type: String,
    },
    file: {
      type: String,
    },
  }).validator(),
  run({ name, file }) {
    const user = Meteor.users.findOne(this.userId);
    const {
      profile: {
        firstName, lastName, position, companyName,
      },
    } = user;
    const fullName = `${firstName} ${lastName}`;
    if (Meteor.isServer) {
      const { private: { admin: { email: adminEmail } } } = Meteor.settings;
      return Email.send({
        to: adminEmail,
        from: `${title} <${email}>`,
        subject: businessFile.subject({
          name: fullName,
          position,
          company: companyName,
        }),
        html: businessFile.content({
          name: fullName,
          company: companyName,
          url: Meteor.absoluteUrl('/admin/verifications'),
        }),
        attachments: [{
          filename: name,
          content: file,
          encoding: 'binary',
        }],
      });
    }
    return user;
  },
});

export const sendContactEmail = new ValidatedMethod({
  name: 'email.sendContactEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.Email,
    },
    name: {
      type: String,
    },
    message: {
      type: String,
    },
  }).validator(),
  run({ name, email: contactEmail, message }) {
    if (Meteor.isServer) {
      const { private: { admin: { email: adminEmail } } } = Meteor.settings;
      return Email.send({
        to: adminEmail,
        from: `${title} <${email}>`,
        subject: contact.subject(),
        html: contact.content({ name, email: contactEmail, message }),
      });
    }
    return null;
  },
});

export const sendApprovalEmail = new ValidatedMethod({
  name: 'email.sendApprovalEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    verificationType: {
      type: String,
      allowedValues: ['business', 'identity'],
    },
  }).validator(),
  run({ userId, verificationType }) {
    const user = Meteor.users.findOne(userId);
    if (Meteor.isServer) {
      const {
        profile: {
          firstName, lastName,
        },
      } = user;
      const fullName = `${firstName} ${lastName}`;
      return Email.send({
        to: user.emails[0].address,
        from: `${title} <${email}>`,
        subject: verificationApprove.subject({
          name: fullName,
          verificationType,
        }),
        html: verificationApprove.content({
          name: fullName,
          verificationType,
        }),
      });
    }
    return user;
  },
});

export const sendDeclineEmail = new ValidatedMethod({
  name: 'email.sendDeclineEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    verificationType: {
      type: String,
      allowedValues: ['business', 'identity'],
    },
  }).validator(),
  run({ userId, verificationType }) {
    const user = Meteor.users.findOne(userId);
    if (Meteor.isServer) {
      const {
        profile: {
          firstName, lastName,
        },
      } = user;
      const fullName = `${firstName} ${lastName}`;
      return Email.send({
        to: user.emails[0].address,
        from: `${title} <${email}>`,
        subject: verificationDecline.subject({
          name: fullName,
          verificationType,
        }),
        html: verificationDecline.content({
          name: fullName,
          verificationType,
        }),
      });
    }
    return user;
  },
});

export const sendTransactionReceivedEmail = new ValidatedMethod({
  name: 'email.sendTransactionReceivedEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    transactionId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator(),
  run({ userId, transactionId }) {
    const user = Meteor.users.findOne(userId);
    const transaction = Transactions.findOne(transactionId);
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        const {
          profile: {
            firstName, lastName,
          },
          emails,
        } = user;
        const fullName = `${firstName} ${lastName}`;
        return Email.send({
          to: emails[0].address,
          from: `${title} <${email}>`,
          subject: transactionReceived.subject(),
          html: transactionReceived.content({
            name: fullName,
            transaction,
          }),
        });
      }
      throw new Meteor.Error('receive-transaction-email', 'Access denied');
    }
    return user;
  },
});

export const sendTransactionSentEmail = new ValidatedMethod({
  name: 'email.sendTransactionSentEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    transactionId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator(),
  run({ userId, transactionId }) {
    const user = Meteor.users.findOne(userId);
    const transaction = Transactions.findOne(transactionId);
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        const {
          profile: {
            firstName, lastName,
          },
          emails,
        } = user;
        const fullName = `${firstName} ${lastName}`;
        return Email.send({
          to: emails[0].address,
          from: `${title} <${email}>`,
          subject: transactionSent.subject(),
          html: transactionSent.content({
            name: fullName,
            transaction,
          }),
        });
      }
      throw new Meteor.Error('receive-transaction-email', 'Access denied');
    }
    return user;
  },
});
