// TODO: add postal code regex
import SimpleSchema from 'simpl-schema';

import {
  nameRegex,
  phoneNumberRegex,
  emailRegex,
  passwordRegex,
} from '../../lib/constants';

const VerificationSchema = new SimpleSchema({
  status: {
    type: String,
    optional: true,
    defaultValue: 'not-verified',
    allowedValues: ['not-verified', 'verified', 'in-progress'],
  },
  fileId: {
    type: String,
    optional: true,
    regEx: SimpleSchema.RegEx.Id,
  },
  verifiedAt: {
    type: Date,
    optional: true,
  },
});

export const UserProfileSchema = new SimpleSchema({
  firstName: {
    type: String,
    regEx: nameRegex,
    max: 25,
    min: 1,
  },
  lastName: {
    type: String,
    regEx: nameRegex,
    max: 25,
    min: 1,
  },
  position: {
    type: String,
  },
  companyName: {
    type: String,
    max: 50,
  },
  phoneCode: {
    type: String,
  },
  phoneNumber: {
    type: String,
    regEx: phoneNumberRegex,
  },
  country: {
    type: String,
  },
  state: {
    type: String,
    optional: true,
  },
  city: {
    type: String,
  },
  address: {
    type: String,
  },
  postalCode: {
    type: String,
  },
  verification: {
    type: Object,
    optional: true,
    defaultValue: {},
  },
  'verification.business': {
    type: VerificationSchema,
    optional: true,
    defaultValue: {},
  },
  'verification.identity': {
    type: VerificationSchema,
    optional: true,
    defaultValue: {},
  },
});

export const UserSchema = new SimpleSchema({
  email: {
    type: String,
    regEx: emailRegex,
  },
  password: {
    type: String,
    regEx: passwordRegex,
  },
  profile: {
    type: UserProfileSchema,
  },
});

// This represents the keys from Lists objects that should be published
// to the client. If we add secret properties to List objects, don't list
// them here to keep them private to the server.
export const userPublicFields = {
  _id: 1,
  emails: 1,
  username: 1,
  profile: 1,
  roles: 1,
  createdAt: 1,
};
