
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import SimpleSchema from 'simpl-schema';
import { userPublicFields } from '../users';

Meteor.publish('users.list', function() {
  if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
    return Roles.getUsersInRole('client', Roles.GLOBAL_GROUP, {
      fields: userPublicFields,
    });
  }
  // user not authorized. do not publish secrets
  this.stop();
  return null;
});

Meteor.publish('users.findById', function(params) {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);


  if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
    const { userId } = params;
    return Meteor.users.find({ _id: userId }, {
      fields: userPublicFields,
    });
  }
  // user not authorized. do not publish secrets
  this.stop();
  return null;
});

Meteor.publish('users.verification', function() {
  if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
    return Meteor.users.find({
      $or: [
        { 'profile.verification.business.status': 'in-progress' },
        { 'profile.verification.identity.status': 'in-progress' },
      ],
    }, {
      fields: userPublicFields,
    });
  }
  // user not authorized. do not publish secrets
  this.stop();
  return null;
});
