import SimpleSchema from 'simpl-schema';
import moment from 'moment';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin';

import { UserSchema, UserProfileSchema } from './users';
import { emailRegex, passwordRegex } from '../../lib/constants';
import { Images } from '../images/images';

export const userWithEmailExists = new ValidatedMethod({
  name: 'users.userWithEmailExists',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: emailRegex,
    },
  }).validator(),
  run({ email }) {
    const user = Meteor.users.findOne({ 'emails.address': email });
    return !!user;
  },
});

export const allowAdminLogin = new ValidatedMethod({
  name: 'users.allowAdminLogin',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: emailRegex,
    },
  }).validator(),
  run({ email }) {
    const user = Meteor.users.findOne({ 'emails.address': email });
    if (user && !Roles.userIsInRole(user._id, ['admin'])) {
      throw new Meteor.Error('permission-error', 'user is not allowed to sign in as admin');
    }
  },
});

export const allowClientLogin = new ValidatedMethod({
  name: 'users.allowClientLogin',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: emailRegex,
    },
  }).validator(),
  run({ email }) {
    const user = Meteor.users.findOne({ 'emails.address': email });
    if (user && !Roles.userIsInRole(user._id, ['client'])) {
      throw new Meteor.Error('permission-error', 'User is not allowed to sign in as client');
    }
  },
});

export const addUser = new ValidatedMethod({
  name: 'users.add',
  mixins: [CallPromiseMixin],
  validate: UserSchema.validator({ clean: true }),
  // eslint-disable-next-line consistent-return
  run({
    email,
    password,
    profile,
  }) {
    if (Meteor.isServer) {
      const userId = Accounts.createUser({
        email, password, profile,
      });
      Meteor.users.update(userId, {
        $set: {
          'emails.0.verified': true,
        },
      });
      Roles.addUsersToRoles(userId, 'client', Roles.GLOBAL_GROUP);
      return userId;
    }
  },
});

export const updatePersonalInformation = new ValidatedMethod({
  name: 'users.updatePersonalInformation',
  mixins: [CallPromiseMixin],
  validate: UserProfileSchema
    .pick('firstName', 'lastName', 'position')
    .validator(),
  run({
    firstName,
    lastName,
    position,
  }) {
    return Meteor.users.update(this.userId, {
      $set: {
        'profile.firstName': firstName,
        'profile.lastName': lastName,
        'profile.position': position,
      },
    });
  },
});

export const updateCompanyInformation = new ValidatedMethod({
  name: 'users.updateCompanyInformation',
  mixins: [CallPromiseMixin],
  validate: UserProfileSchema
    .pick(
      'country', 'state', 'city', 'address',
      'postalCode', 'phoneCode', 'phoneNumber',
      'companyName',
    )
    .validator(),
  run({
    country, state, city, address,
    postalCode, phoneCode, phoneNumber,
    companyName,
  }) {
    return Meteor.users.update(this.userId, {
      $set: {
        'profile.country': country,
        'profile.state': state,
        'profile.city': city,
        'profile.address': address,
        'profile.postalCode': postalCode,
        'profile.phoneCode': phoneCode,
        'profile.phoneNumber': phoneNumber,
        'profile.companyName': companyName,
      },
    });
  },
});

export const addVerificationImage = new ValidatedMethod({
  name: 'users.addVerificationImage',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    verificationType: {
      type: String,
      allowedValues: ['business', 'identity'],
    },
  }).validator(),
  run({
    fileId,
    verificationType,
  }) {
    const updateString = `profile.verification.${verificationType}`;
    return Meteor.users.update(this.userId, {
      $set: {
        [updateString]: {
          fileId,
          status: 'in-progress',
        },
      },
    });
  },
});

export const approveVerification = new ValidatedMethod({
  name: 'users.approveVerification',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    verificationType: {
      type: String,
      allowedValues: ['business', 'identity'],
    },
  }).validator(),
  // eslint-disable-next-line consistent-return
  run({
    userId,
    fileId,
    verificationType,
  }) {
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        const updateString = `profile.verification.${verificationType}`;
        return Meteor.users.update(userId, {
          $set: {
            [updateString]: {
              fileId,
              status: 'verified',
              verifiedAt: new Date(),
            },
          },
        });
      }
      throw new Meteor.Error('verify-image', 'Access denied');
    }
  },
});

export const declineVerification = new ValidatedMethod({
  name: 'users.declineVerification',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    verificationType: {
      type: String,
      allowedValues: ['business', 'identity'],
    },
  }).validator(),
  // eslint-disable-next-line consistent-return
  run({
    userId,
    fileId,
    verificationType,
  }) {
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        const updateString = `profile.verification.${verificationType}`;
        Meteor.users.update(userId, {
          $set: {
            [updateString]: {
              status: 'not-verified',
            },
          },
        });
        return Images.remove(fileId);
      }
      throw new Meteor.Error('verify-image', 'Access denied');
    }
  },
});

export const checkPassword = new ValidatedMethod({
  name: 'users.checkPassword',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    password: {
      type: String,
      regEx: passwordRegex,
    },
  }).validator(),
  run({ password }) {
    const user = Meteor.users.findOne(this.userId);
    if (Meteor.isServer) {
      const { userId, error } = Accounts._checkPassword(user, password);
      if (error) {
        throw new Meteor.Error('password-check', error.reason, error);
      }
      return userId;
    }
    return this.userId;
  },
});

export const addEmail = new ValidatedMethod({
  name: 'users.addEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: emailRegex,
    },
  }).validator(),
  run({ email }) {
    if (Meteor.isServer) {
      try {
        Accounts.addEmail(this.userId, email);
        const expiresIn = moment().add(1, 'day').toDate().getTime();
        Accounts.sendVerificationEmail(this.userId, email, { expiresIn });
      } catch (error) {
        throw new Meteor.Error('add-email', 'unable to add email', error);
      }
    }
  },
});

export const verifyAndChangeEmail = new ValidatedMethod({
  name: 'users.changeEmail',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    token: {
      type: String,
    },
  }).validator(),
  run({ token }) {
    if (Meteor.isServer) {
      const user = Meteor.users.findOne({
        'services.email.verificationTokens.token': token,
      });
      if (!user) {
        throw new Meteor.Error(403, 'Verify email link expired');
      }
      const userId = user._id;
      const tokenRecord = user.services.email.verificationTokens.find(t => t.token === token);
      if (!tokenRecord) {
        throw new Meteor.Error(403, 'Verify email link expired');
      }

      const emailsRecord = user.emails.find(e => e.address === tokenRecord.address);
      if (!emailsRecord) {
        throw new Meteor.Error(403, 'Verify email link is for unknown address');
      }

      Meteor.users.update({
        _id: userId,
        'emails.address': tokenRecord.address,
      },
      {
        $pull: {
          'services.email.verificationTokens': {
            address: tokenRecord.address,
          },
        },
        $set: {
          emails: [{
            address: tokenRecord.address,
            verified: true,
          }],
        },
      });
    }
  },
});
