import SimpleSchema from 'simpl-schema';
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin';

import { Transactions, TransactionsSchema } from './transactions';

export const addTransaction = new ValidatedMethod({
  name: 'transactions.add',
  mixins: [CallPromiseMixin],
  validate: TransactionsSchema
    .omit('_id', 'userId')
    .validator(),
  run(transaction) {
    if (Meteor.isServer) {
      const user = Meteor.users.findOne(this.userId);
      if (user.profile.verification.identity.status !== 'verified' || user.profile.verification.identity.status !== 'verified') {
        throw new Meteor.Error('transaction-error', 'Identity and business must be verified');
      }
    }
    return Transactions.insert({ ...transaction, userId: this.userId });
  },
});

export const receiveTransaction = new ValidatedMethod({
  name: 'transactions.receive',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    transactionId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    rate: {
      type: Number,
    },
    buyAmount: {
      type: Number,
    },
    receivedAt: {
      type: Date,
    },
  }).validator(),
  // eslint-disable-next-line consistent-return
  run({
    transactionId, rate, receivedAt, buyAmount,
  }) {
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        return Transactions.update({ _id: transactionId }, {
          $set: {
            status: 'received',
            rate,
            receivedAt,
            'buy.amount': buyAmount,
          },
        });
      }
      throw new Meteor.Error('receive-transaction', 'Access denied');
    }
  },
});

export const sendTransaction = new ValidatedMethod({
  name: 'transactions.send',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    transactionId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    sentAt: {
      type: Date,
    },
  }).validator(),
  // eslint-disable-next-line consistent-return
  run({
    transactionId, sentAt,
  }) {
    if (Meteor.isServer) {
      if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
        return Transactions.update({ _id: transactionId }, {
          $set: {
            status: 'sent',
            sentAt,
          },
        });
      }
      throw new Meteor.Error('receive-transaction', 'Access denied');
    }
  },
});
