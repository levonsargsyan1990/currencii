
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import SimpleSchema from 'simpl-schema';

import { Transactions } from '../transactions';

Meteor.publish('transactions.listByUser', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  return Transactions.find({ userId }, {
    fields: Transactions.publicFields,
  });
});

Meteor.publish('transactions.all.created', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  if (Roles.userIsInRole(userId, ['admin'], Roles.GLOBAL_GROUP)) {
    return Transactions.find({
      status: 'created',
    }, {
      fields: Transactions.publicFields,
    });
  }

  // user not authorized. do not publish secrets
  this.stop();
  return null;
});

Meteor.publish('transactions.all.received', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  if (Roles.userIsInRole(userId, ['admin'], Roles.GLOBAL_GROUP)) {
    return Transactions.find({
      status: 'received',
    }, {
      fields: Transactions.publicFields,
    });
  }

  // user not authorized. do not publish secrets
  this.stop();
  return null;
});

Meteor.publish('transactions.all.sent', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  if (Roles.userIsInRole(userId, ['admin'], Roles.GLOBAL_GROUP)) {
    return Transactions.find({
      status: 'sent',
    }, {
      fields: Transactions.publicFields,
    });
  }

  // user not authorized. do not publish secrets
  this.stop();
  return null;
});

Meteor.publish('transactions.all.completed', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  if (Roles.userIsInRole(userId, ['admin'], Roles.GLOBAL_GROUP)) {
    return Transactions.find({
      status: 'completed',
    }, {
      fields: Transactions.publicFields,
    });
  }

  // user not authorized. do not publish secrets
  this.stop();
  return null;
});

Meteor.publish('transactions.pendingByUser', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  return Transactions.find({
    userId,
    status: {
      $nin: ['sent', 'completed'],
    },
  }, {
    fields: Transactions.publicFields,
  });
});

Meteor.publish('transactions.executedByUser', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  return Transactions.find({
    userId,
    status: {
      $in: ['sent', 'completed'],
    },
  }, {
    fields: Transactions.publicFields,
  });
});
