import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { BankAccountsSchema } from '../bankAccounts/bankAccounts';

export const Transactions = new Mongo.Collection('transactions');

// Deny all client-side updates since we will be using methods to manage this collection
Transactions.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

export const TransactionsSizeSchema = new SimpleSchema({
  amount: {
    type: Number,
  },
  currency: {
    type: String,
  },
});

export const TransactionsSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  status: {
    type: String,
    optional: true,
    defaultValue: 'created',
    allowedValues: ['created', 'received', 'sent', 'completed'],
  },
  userId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  reference: {
    type: String,
  },
  sell: {
    type: TransactionsSizeSchema,
  },
  buy: {
    type: TransactionsSizeSchema,
  },
  rate: {
    type: Number,
    optional: true,
  },
  marketRate: {
    type: Number,
  },
  guaranteedRate: {
    type: Number,
  },
  savedAmount: {
    type: Number,
  },
  bankAccount: {
    type: BankAccountsSchema.omit('userId', 'createdAt'),
  },
  sentAt: {
    type: Date,
    optional: true,
  },
  receivedAt: {
    type: Date,
    optional: true,
  },
  createdAt: {
    type: Date,
    optional: true,
    // eslint-disable-next-line consistent-return
    autoValue() {
      if (this.isInsert) {
        return new Date();
      }
    },
  },
});

Transactions.attachSchema(TransactionsSchema);

// This represents the keys from Lists objects that should be published
// to the client. If we add secret properties to List objects, don't list
// them here to keep them private to the server.
Transactions.publicFields = {
  _id: 1,
  status: 1,
  userId: 1,
  bankName: 1,
  reference: 1,
  buy: 1,
  sell: 1,
  marketRate: 1,
  guaranteedRate: 1,
  bankAccount: 1,
  sentAt: 1,
  receivedAt: 1,
  createdAt: 1,
};
