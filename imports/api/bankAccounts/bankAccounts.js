import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { swiftRegex, ibanRegex, bankAccountNumberRegex } from '../../lib/constants';

export const BankAccounts = new Mongo.Collection('bankAccounts');

// Deny all client-side updates since we will be using methods to manage this collection
BankAccounts.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

export const BankAccountsSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  userId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  bankName: {
    type: String,
  },
  swift: {
    type: String,
    regEx: swiftRegex,
  },
  iban: {
    type: String,
    regEx: ibanRegex,
  },
  accountNumber: {
    type: String,
    regEx: bankAccountNumberRegex,
  },
  createdAt: {
    type: Date,
    optional: true,
    // eslint-disable-next-line consistent-return
    autoValue() {
      if (this.isInsert) {
        return new Date();
      }
    },
  },
});

BankAccounts.attachSchema(BankAccountsSchema);

// This represents the keys from Lists objects that should be published
// to the client. If we add secret properties to List objects, don't list
// them here to keep them private to the server.
BankAccounts.publicFields = {
  _id: 1,
  userId: 1,
  bankName: 1,
  swift: 1,
  iban: 1,
  accountNumber: 1,
  createdAt: 1,
};
