
import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

import { BankAccounts } from '../bankAccounts';

Meteor.publish('bankAccounts.listByUser', (params) => {
  new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  const { userId } = params;

  return BankAccounts.find({ userId }, {
    fields: BankAccounts.publicFields,
  });
});
