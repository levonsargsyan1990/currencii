import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin';

import { BankAccounts, BankAccountsSchema } from './bankAccounts';

export const addBankAccount = new ValidatedMethod({
  name: 'bankAccount.add',
  mixins: [CallPromiseMixin],
  validate: BankAccountsSchema
    .omit('_id', 'userId')
    .validator(),
  run({
    bankName,
    swift,
    iban,
    accountNumber,
  }) {
    return BankAccounts.insert({
      bankName,
      swift,
      iban,
      accountNumber,
      userId: this.userId,
    });
  },
});

export const editBankAccount = new ValidatedMethod({
  name: 'bankAccount.edit',
  mixins: [CallPromiseMixin],
  validate: BankAccountsSchema
    .omit('userId')
    .validator(),
  run({
    _id,
    bankName,
    swift,
    iban,
    accountNumber,
  }) {
    return BankAccounts.update({
      _id, userId: this.userId,
    }, {
      $set: {
        bankName,
        swift,
        iban,
        accountNumber,
      },
    });
  },
});

export const deleteBankAccount = new ValidatedMethod({
  name: 'bankAccount.delete',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    accountId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator(),
  run({ accountId }) {
    return BankAccounts.remove({
      _id: accountId,
      userId: this.userId,
    });
  },
});
