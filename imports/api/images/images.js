import { Roles } from 'meteor/alanning:roles';

// eslint-disable-next-line no-undef
const imageStore = new FS.Store.GridFS('images');

// eslint-disable-next-line no-undef
export const Images = new FS.Collection('images', {
  stores: [imageStore],
});

Images.allow({
  insert() { return true; },
  download(userId) {
    return Roles.userIsInRole(userId, ['admin'], Roles.GLOBAL_GROUP);
  },
});

// Deny all client-side updates and removes
Images.deny({
  update() { return true; },
  remove() { return true; },
});
