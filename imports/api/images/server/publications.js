
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import SimpleSchema from 'simpl-schema';

import { Images } from '../images';

Meteor.publish('images.one', function(params) {
  new SimpleSchema({
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validate(params);

  if (Roles.userIsInRole(this.userId, ['admin'], Roles.GLOBAL_GROUP)) {
    const { fileId } = params;
    return Images.find({ _id: fileId });
  }
  // user not authorized. do not publish secrets
  this.stop();
  return null;
});
