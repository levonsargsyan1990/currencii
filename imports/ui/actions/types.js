// Registration actions
export const SIGNUP_RESET = 'signup-reset';
export const SIGNUP_CHANGE_STEP = 'signup-change-step';
export const SIGNUP_SUBMIT_SECTION_1 = 'signup-submit-section-1';
export const SIGNUP_SUBMIT_SECTION_2 = 'signup-submit-section-2';
export const SIGNUP_SUBMIT_SECTION_3 = 'signup-submit-section-3';

// Email change
export const EMAIL_CHANGE_SET_EMAIL = 'email-change-set-email';
export const EMAIL_CHANGE_SET_EMAIL_SENT = 'email-change-set-email-sent';
export const EMAIL_CHANGE_SET_ERROR = 'email-change-set-error';
export const EMAIL_CHANGE_RESET = 'email-change-reset';

// Exchange
export const EXCHANGE_CHANGE_STEP = 'exchange-change-step';
export const EXCHANGE_SUBMIT_SECTION_1 = 'exchange-submit-section-1';
export const EXCHANGE_SUBMIT_SECTION_2 = 'exchange-submit-section-2';
export const EXCHANGE_SUBMIT_SECTION_3 = 'exchange-submit-section-3';
export const EXCHANGE_SET_TRANSACTION_ID = 'exchange-set-transaction-id';
export const EXCHANGE_RESET = 'exchange-reset';
