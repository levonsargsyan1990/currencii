import {
  EMAIL_CHANGE_SET_EMAIL,
  EMAIL_CHANGE_RESET,
  EMAIL_CHANGE_SET_EMAIL_SENT,
  EMAIL_CHANGE_SET_ERROR,
} from './types';

export function setEmail(email) {
  return {
    type: EMAIL_CHANGE_SET_EMAIL,
    payload: email,
  };
}

export function setEmailSent() {
  return {
    type: EMAIL_CHANGE_SET_EMAIL_SENT,
  };
}

export function setError(error = '') {
  return {
    type: EMAIL_CHANGE_SET_ERROR,
    payload: error,
  };
}

export function reset() {
  return {
    type: EMAIL_CHANGE_RESET,
  };
}
