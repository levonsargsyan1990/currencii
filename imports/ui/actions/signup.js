import {
  SIGNUP_RESET,
  SIGNUP_CHANGE_STEP,
  SIGNUP_SUBMIT_SECTION_1,
  SIGNUP_SUBMIT_SECTION_2,
  SIGNUP_SUBMIT_SECTION_3,
} from './types';

export function reset() {
  return {
    type: SIGNUP_RESET,
  };
}

export function changeStep(step) {
  return {
    type: SIGNUP_CHANGE_STEP,
    payload: step,
  };
}

export function submitSection1({ email, password }) {
  return {
    type: SIGNUP_SUBMIT_SECTION_1,
    payload: { email, password },
  };
}

export function submitSection2({
  companyName, country, city, address, postalCode, state,
}) {
  return {
    type: SIGNUP_SUBMIT_SECTION_2,
    payload: {
      companyName, country, city, address, postalCode, state,
    },
  };
}

export function submitSection3({
  firstName, lastName, position, phoneCode, phoneNumber,
}) {
  return {
    type: SIGNUP_SUBMIT_SECTION_3,
    payload: {
      firstName, lastName, position, phoneCode, phoneNumber,
    },
  };
}
