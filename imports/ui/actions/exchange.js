import {
  EXCHANGE_CHANGE_STEP,
  EXCHANGE_SUBMIT_SECTION_1,
  EXCHANGE_SUBMIT_SECTION_2,
  EXCHANGE_SET_TRANSACTION_ID,
  EXCHANGE_RESET,
} from './types';

export function changeStep(step) {
  return {
    type: EXCHANGE_CHANGE_STEP,
    payload: step,
  };
}

export function submitSection1(data) {
  return {
    type: EXCHANGE_SUBMIT_SECTION_1,
    payload: data,
  };
}

export function submitSection2({
  bankAccount,
}) {
  return {
    type: EXCHANGE_SUBMIT_SECTION_2,
    payload: { bankAccount },
  };
}

export function setTransactionId({
  transactionId,
}) {
  return {
    type: EXCHANGE_SET_TRANSACTION_ID,
    payload: { transactionId },
  };
}

export function reset() {
  return {
    type: EXCHANGE_RESET,
  };
}
