import {
  EXCHANGE_CHANGE_STEP,
  EXCHANGE_SUBMIT_SECTION_1,
  EXCHANGE_SUBMIT_SECTION_2,
  EXCHANGE_SET_TRANSACTION_ID,
  EXCHANGE_RESET,
} from '../actions/types';
import { generateReferenceNumber } from '../../lib/helpers';

const INITIAL_STATE = {
  inProgress: false,
  step: 1,
  sellAmount: '',
  sellCurrency: 'usd',
  buyAmount: '',
  buyCurrency: 'eur',
  marketRate: 0,
  guaranteedRate: 0,
  savedAmount: 0,
  bankAccount: {
    _id: '',
    bankName: '',
    swift: '',
    iban: '',
    accountNumber: '',
  },
  reference: generateReferenceNumber(),
  transactionId: '',
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case EXCHANGE_CHANGE_STEP:
      return { ...state, step: action.payload };
    case EXCHANGE_SUBMIT_SECTION_1:
      return { ...state, ...action.payload, inProgress: true };
    case EXCHANGE_SUBMIT_SECTION_2:
      return { ...state, ...action.payload };
    case EXCHANGE_SET_TRANSACTION_ID:
      return { ...state, ...action.payload };
    case EXCHANGE_RESET:
      return INITIAL_STATE;
    default:
      return state;
  }
}
