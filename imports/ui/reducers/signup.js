// TODO: remove test state

import {
  SIGNUP_CHANGE_STEP,
  SIGNUP_SUBMIT_SECTION_1,
  SIGNUP_SUBMIT_SECTION_2,
  SIGNUP_SUBMIT_SECTION_3,
  SIGNUP_RESET,
} from '../actions/types';

const INITIAL_STATE = {
  step: 1,
  email: '',
  password: '',
  companyName: '',
  country: '',
  city: '',
  address: '',
  postalCode: '',
  state: '',
  firstName: '',
  lastName: '',
  position: '',
  phoneCode: '',
  phoneNumber: '',
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SIGNUP_RESET:
      return INITIAL_STATE;
    case SIGNUP_CHANGE_STEP:
      return { ...state, step: action.payload };
    case SIGNUP_SUBMIT_SECTION_1:
      return { ...state, ...action.payload };
    case SIGNUP_SUBMIT_SECTION_2:
      return { ...state, ...action.payload };
    case SIGNUP_SUBMIT_SECTION_3:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
