import {
  EMAIL_CHANGE_SET_EMAIL,
  EMAIL_CHANGE_RESET,
  EMAIL_CHANGE_SET_EMAIL_SENT,
  EMAIL_CHANGE_SET_ERROR,
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  emailSent: false,
  error: '',
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case EMAIL_CHANGE_SET_EMAIL:
      return { ...state, email: action.payload };
    case EMAIL_CHANGE_SET_EMAIL_SENT:
      return { ...state, emailSent: true };
    case EMAIL_CHANGE_SET_ERROR:
      return { ...state, error: action.payload };
    case EMAIL_CHANGE_RESET:
      return INITIAL_STATE;
    default:
      return state;
  }
}
