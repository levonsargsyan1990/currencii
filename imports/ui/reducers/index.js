import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import SignupReducer from './signup';
import EmailChangeReducer from './emailChange';
import ExchangeReducer from './exchange';

const rootReducer = combineReducers({
  signup: SignupReducer,
  emailChange: EmailChangeReducer,
  form: formReducer,
  exchange: ExchangeReducer,
});

export default rootReducer;
