// TODO: find a better solution for overriding Semantic UI styling

import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  /* Senatic UI overrides */
  .ui.teal.segment:not(.inverted) {
    border-top: 2px solid ${props => props.theme.brand.green}!important;
  }

  .ui.green.button:not(.basic) {
    background-image: linear-gradient(90deg, ${props => props.theme.brand.blue}, ${props => props.theme.brand.green}) !important;
  }

  .ui.button { 
    border-radius: 18px;
    padding-left: 2.5em;
    padding-right: 2.5em;
  }
`;
