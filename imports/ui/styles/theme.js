const theme = {
  // Colors
  brand: {
    green: '#3ad5a8',
    blue: '#00d2ff',
    pink: '#ff6aa4',
    orange: '#ff9549',
  },
  backgroundGrey: '#f7f7f7',
  anchorBlue: '#4183c4',
  red: '#db2828',
  teal: '#00b5ad',
  green: '#16ab39',
  // Responsive breakpoints
  breakpoints: {
    mobile: '480px',
    tablet: '767px',
    desktop: '1280px',
  },
};

export default theme;
