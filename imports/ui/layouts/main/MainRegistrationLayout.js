import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ReactRouterPropTypes from 'react-router-prop-types';
import i18n from 'meteor/universe:i18n';
import { Grid, Header, Divider } from 'semantic-ui-react';

import SignupPoint from '../../components/main/SignupPoint';

const { title } = Meteor.settings.public;

const StyledLayout = styled.div`
  .grid {
    height: 100%;
    margin-top: 0;
    margin-bottom: 0;
    .row {
      padding-top: 0;
      padding-bottom: 0;
    }
  }
  & > .grid {
    height: 100vh;
    & > .row {
      /* align-items: center; */
      align-items: stretch;
    }
    .column {
      height: 100%;
    }
    .content-col {
      display: flex !important;
      align-items: center;
      & > div {
        width: 100%;
      }
    }
  }
  .left-col {
    padding-top: 150px;
    background-size: cover;
    background-position: center;
  }
  .layout-content-row {
    align-items: center;
  }
  @media only screen and (max-width: ${props => props.theme.breakpoints.tablet}) {
    .ui.grid {
      margin: 0;
      .row {
        .left-col {
          display: none !important;
        }
        .right-col {
          width: 100% !important;
          padding: 0 !important;
          .ten {
            width: 100% !important;
          }
          .one {
            display: none !important;
          }
        }
      }
    }
  }
`;

const StyledLinks = styled.div`
  text-align: center;
`;

const StyledLink = styled.span`
  margin: 0 10px;
  display: inline-block;
`;

const MainRegistrationLayout = ({ children, location: { pathname, search } }) => {
  const isSignup = pathname.includes('signup');
  const path = isSignup ? { pathname: '/login', search } : { pathname: '/signup', search };
  const text = isSignup
    ? i18n.__('main.onboarding.layout.signup.subtitle')
    : i18n.__('main.onboarding.layout.login.subtitle');
  const linkText = isSignup
    ? i18n.__('main.onboarding.layout.signup.button')
    : i18n.__('main.onboarding.layout.login.button');

  const signupPoints = [];
  if (isSignup) {
    for (let i = 0; i < 3; i += 1) {
      signupPoints.push(<SignupPoint key={i} step={i + 1} />);
    }
  }

  const bgImageName = isSignup ? 'signup-bg.jpg' : 'login-bg.jpg';

  return (
    <StyledLayout>
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column
            className="left-col"
            style={{ backgroundImage: `url('img/${bgImageName}')` }}
          />
          <Grid.Column className="right-col">
            <Grid>
              <Grid.Row className="layout-content-row" columns={3}>
                <Grid.Column mobile={1} width={3} />
                <Grid.Column className="content-col" width={10}>
                  <div>
                    <Header textAlign="center" as="h1">
                      <a href="/">
                        <img src="img/blackLogo.svg" alt="Currencii" />
                      </a>
                    </Header>
                    <Divider hidden />
                    {children}
                    <StyledLinks>
                      {text}
                      <StyledLink>
                        <Link to={path}>{linkText}</Link>
                      </StyledLink>
                    </StyledLinks>
                    <Divider hidden />
                  </div>
                </Grid.Column>
                <Grid.Column className="content-col" width={3} mobile={1}>
                  <div>{signupPoints}</div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </StyledLayout>
  );
};

MainRegistrationLayout.propTypes = {
  children: PropTypes.element.isRequired,
  location: ReactRouterPropTypes.location.isRequired,
};

export default withRouter(MainRegistrationLayout);
