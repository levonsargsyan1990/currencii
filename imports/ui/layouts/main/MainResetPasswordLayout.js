import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Container, Grid, Header } from 'semantic-ui-react';
import i18n from 'meteor/universe:i18n';

const T = i18n.createComponent();

const { title } = Meteor.settings.public;

const StyledLayout = styled.div`
  padding-top: 150px;
  & > .grid {
    min-height: 370px;
    .row {
      align-items: center;
    }
  }
`;

const StyledLink = styled.span`
  margin: 0 10px;
  display: inline-block;
`;

const MainRegistrationLayout = ({ children }) => (
  <Container>
    <StyledLayout>
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column width={6} textAlign="center">
            <Header as="h1">
              {title}
              <Header.Subheader>
                <StyledLink>
                  <Link to="/signup">
                    <T>main.onboarding.layout.reset.signup</T>
                  </Link>
                </StyledLink>
                <T>main.onboarding.layout.reset.or</T>
                <StyledLink>
                  <Link to="/login">
                    <T>main.onboarding.layout.reset.login</T>
                  </Link>
                </StyledLink>
              </Header.Subheader>
            </Header>
          </Grid.Column>
          <Grid.Column width={8}>{children}</Grid.Column>
        </Grid.Row>
      </Grid>
    </StyledLayout>
  </Container>
);

MainRegistrationLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainRegistrationLayout;
