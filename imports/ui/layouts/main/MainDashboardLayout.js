import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import ReactRouterPropTypes from 'react-router-prop-types';
import IdleTimer from 'react-idle-timer';
import { withRouter } from 'react-router-dom';

import ExchangeTransactionNav from '../../components/main/ExchangeTransactionNav';
import Header from '../../components/main/Header';
import SessionTimoutModal from '../../components/main/SessionTimout';

class MainDashboardLayout extends Component {
  state = {
    isTimeoutModalOpen: false,
  }

  handleTimeoutModalToggle() {
    const { isTimeoutModalOpen } = this.state;
    this.setState({ isTimeoutModalOpen: !isTimeoutModalOpen });
  }

  render() {
    const { isTimeoutModalOpen } = this.state;
    const { children, location: { pathname } } = this.props;
    const pathsShown = ['exchange', 'transactions'];
    const path = pathname.split('/')[1];
    const showNav = pathsShown.includes(path);

    return (
      <div>
        <Header />
        {showNav ? <ExchangeTransactionNav path={path} /> : null}
        {children}
        <IdleTimer
          onIdle={() => this.handleTimeoutModalToggle()}
          timeout={1000 * 60 * Meteor.settings.public.timeoutInMinutes}
        />
        <SessionTimoutModal
          onClose={() => this.handleTimeoutModalToggle()}
          isOpen={isTimeoutModalOpen}
        />
      </div>
    );
  }
}

MainDashboardLayout.propTypes = {
  children: PropTypes.element.isRequired,
  location: ReactRouterPropTypes.location.isRequired,
};

export default withRouter(MainDashboardLayout);
