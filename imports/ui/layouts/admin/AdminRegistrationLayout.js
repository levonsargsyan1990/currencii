import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Container } from 'semantic-ui-react';

const StyledLayout = styled.div`
  background-color: ${props => props.theme.backgroundGrey};
  min-height: 100vh;
  padding: 100px 0;
`;

const AdminRegistrationLayout = ({ children }) => (
  <StyledLayout>
    <Container text>
      {children}
    </Container>
  </StyledLayout>
);

AdminRegistrationLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default AdminRegistrationLayout;
