import React from 'react';
import PropTypes from 'prop-types';
import { Sidebar as SemanticSidebar } from 'semantic-ui-react';
import styled from 'styled-components';

import Header from '../../components/admin/Header';
import Sidebar from '../../components/admin/Sidebar';

const StyledLayout = styled.div`
  background-color: ${props => props.theme.backgroundGrey};
  min-height: 100vh;
  .pushable {
    min-height: calc(100vh - 52px);
  }
`;

const AdminDashboardLayout = ({ children }) => (
  <StyledLayout>
    <Header />
    <div>
      <Sidebar />
      <SemanticSidebar.Pusher>
        {children}
      </SemanticSidebar.Pusher>
    </div>
  </StyledLayout>
);

AdminDashboardLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default AdminDashboardLayout;
