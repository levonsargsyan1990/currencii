import React from 'react';
import styled from 'styled-components';
import { Container, Divider } from 'semantic-ui-react';

import PendingTransactions from '../../components/main/transactions/PendingTransactions';
import ExecutedTransactions from '../../components/main/transactions/ExecutedTransactions';

const StyledContainer = styled.div`
  padding-bottom: 100px;
`;

const Transactions = () => (
  <StyledContainer>
    <Container>
      <PendingTransactions />
      <Divider hidden />
      <Divider hidden />
      <ExecutedTransactions />
    </Container>
  </StyledContainer>
);

export default Transactions;
