import React from 'react';
import i18n from 'meteor/universe:i18n';
import {
  Container, Segment, Header,
} from 'semantic-ui-react';
import styled from 'styled-components';

import VerificationUpload from '../../components/main/verification/VerificationUpload';

const T = i18n.createComponent();

const StyledContainer = styled.div`
  padding: 100px 0;
`;

const Verification = () => (
  <StyledContainer>
    <Container text>
      <Segment>
        <Header as="h3">
          <T>main.verification.title</T>
        </Header>
        <VerificationUpload />
      </Segment>
    </Container>
  </StyledContainer>
);

export default Verification;
