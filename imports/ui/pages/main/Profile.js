// TODO: Change all relative imports to absolute in all files

import React from 'react';
import i18n from 'meteor/universe:i18n';
import { Container, Segment, Header } from 'semantic-ui-react';
import styled from 'styled-components';

import PersonalInformation from '../../components/main/profile/PersonalInformation';
import CompanyInformation from '../../components/main/profile/Companylnformation';
import BankAccounts from '../../components/main/profile/BankAccounts';

const T = i18n.createComponent();

const StyledContainer = styled.div`
  padding: 100px 0;
`;

const Profile = () => (
  <StyledContainer>
    <Container text>
      <Segment>
        <Header as="h3">
          <T>main.profile.title</T>
        </Header>
        <PersonalInformation />
        <CompanyInformation />
        <BankAccounts />
      </Segment>
    </Container>
  </StyledContainer>
);

export default Profile;
