import React from 'react';

import { Segment } from 'semantic-ui-react';

import LoginForm from '../../components/main/LoginForm';

const Login = () => (
  <Segment color="teal">
    <LoginForm />
  </Segment>
);

export default Login;
