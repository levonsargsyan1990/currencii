// TODO: change exported object

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { connect } from 'react-redux';
import { withTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import {
  Container, Segment, Header, Divider, Table, Input,
} from 'semantic-ui-react';
import styled from 'styled-components';

import { reset as resetAction } from '../../actions/emailChange';

import EmailChange from '../../components/main/settings/EmailChange';
import PasswordChange from '../../components/main/settings/PasswordChange';

const T = i18n.createComponent();

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  padding: 0 1rem;
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

const StyledContainer = styled.div`
  padding: 100px 0;
`;

const StyledPassword = styled.div`
  .input.disabled {
    opacity: 1;
    input {
      border: none;
      padding-left: 0;
    }
  }
`;

class Settings extends Component {
  state = {
    isEmailModalOpen: false,
    isPasswordModalOpen: false,
  };

  handleEmailModalToggle() {
    const { isEmailModalOpen } = this.state;
    const { reset } = this.props;
    this.setState({ isEmailModalOpen: !isEmailModalOpen, isPasswordModalOpen: false });
    if (isEmailModalOpen) {
      reset();
    }
  }

  handlePasswordModalToggle() {
    const { isPasswordModalOpen } = this.state;
    this.setState({ isPasswordModalOpen: !isPasswordModalOpen, isEmailModalOpen: false });
  }

  render() {
    const { user, email } = this.props;
    const { isEmailModalOpen, isPasswordModalOpen } = this.state;
    return (
      <StyledContainer>
        <Container text>
          <Segment>
            <Header as="h3">
              <T>main.settings.title</T>
            </Header>
            <Segment basic>
              <Header as="h4">
                <T>main.settings.account.title</T>
              </Header>
              <Divider />
              <Segment basic loading={!user || !email}>
                <StyledTable>
                  <Table stackable basic="very">
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>
                          <T>main.settings.account.email</T>
                        </Table.Cell>
                        <Table.Cell>{email}</Table.Cell>
                        <Table.Cell textAlign="right">
                          <AnchorStyledText onClick={() => this.handleEmailModalToggle()}>
                            <T>main.settings.account.change</T>
                          </AnchorStyledText>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>
                          <T>main.settings.account.password</T>
                        </Table.Cell>
                        <Table.Cell>
                          <StyledPassword>
                            <Input disabled value="password" type="password" />
                          </StyledPassword>
                        </Table.Cell>
                        <Table.Cell textAlign="right">
                          <AnchorStyledText onClick={() => this.handlePasswordModalToggle()}>
                            <T>main.settings.account.change</T>
                          </AnchorStyledText>
                        </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </StyledTable>
              </Segment>
            </Segment>
          </Segment>
          <EmailChange
            onClose={() => this.handleEmailModalToggle()}
            isOpen={isEmailModalOpen}
          />
          <PasswordChange
            onClose={() => this.handlePasswordModalToggle()}
            isOpen={isPasswordModalOpen}
          />
        </Container>
      </StyledContainer>
    );
  }
}


Settings.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
  }),
  email: PropTypes.string,
  reset: PropTypes.func.isRequired,
};

Settings.defaultProps = {
  user: null,
  email: '',
};

const mapDataToProps = () => {
  const user = Meteor.user();
  let email = '';
  if (user) {
    email = user.emails[0].address;
  }
  return { user, email };
};

// const meteorWrappedComponent = withTracker(() => ({ user: Meteor.user(), email: '' }))(Settings);
const meteorWrappedComponent = withTracker(mapDataToProps)(Settings);

export default connect(null, { reset: resetAction })(meteorWrappedComponent);
