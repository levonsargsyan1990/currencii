import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Container, Message, Divider,
} from 'semantic-ui-react';
import styled from 'styled-components';
import i18n from 'meteor/universe:i18n';

import { reset as resetAction } from '../../actions/exchange';

import ExchangeProgressBar from '../../components/main/exchange/ExchangeProgressBar';
import ExchangeStep1 from '../../components/main/exchange/ExchangeStep1';
import ExchangeStep2 from '../../components/main/exchange/ExchangeStep2';
import ExchangeStep3 from '../../components/main/exchange/ExchangeStep3';
import ExchangeStep4 from '../../components/main/exchange/ExchangeStep4';

const T = i18n.createComponent();

const StyledContainer = styled.div`
  padding-bottom: 100px;
`;

class Exchange extends Component {
  componentWillUnmount() {
    const { reset } = this.props;
    reset();
  }

  render() {
    const { step } = this.props;
    let section = null;
    switch (step) {
      case 1:
        section = <ExchangeStep1 />;
        break;
      case 2:
        section = <ExchangeStep2 />;
        break;
      case 3:
        section = <ExchangeStep3 />;
        break;
      case 4:
        section = <ExchangeStep4 />;
        break;
      default:
        section = (
          <Message negative>
            <Message.Header><T>pages.main.exchange.messageHeader</T></Message.Header>
            <p><T>pages.main.exchange.reload</T></p>
          </Message>
        );
    }
    return (
      <StyledContainer>
        <Container text>
          <ExchangeProgressBar />
          <Divider hidden />
          {section}
        </Container>
      </StyledContainer>
    );
  }
}


Exchange.propTypes = {
  step: PropTypes.number.isRequired,
  reset: PropTypes.func.isRequired,
};

const mapStateToProps = ({ exchange: { step } }) => ({ step });

export default connect(mapStateToProps, { reset: resetAction })(Exchange);
