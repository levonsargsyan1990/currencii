import React from 'react';

import { Segment } from 'semantic-ui-react';

import ForgotPasswordForm from '../../components/main/ForgotPassword';

const ForgotPassword = () => (
  <Segment color="teal">
    <ForgotPasswordForm />
  </Segment>
);

export default ForgotPassword;
