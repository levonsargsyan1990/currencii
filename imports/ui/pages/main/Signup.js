import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Message, Segment } from 'semantic-ui-react';

import { reset as resetAction } from '../../actions/signup';

import SignupStep1 from '../../components/main/SignupStep1';
import SignupStep2 from '../../components/main/SignupStep2';
import SignupStep3 from '../../components/main/SignupStep3';

class Signup extends Component {
  componentWillUnmount() {
    const { reset } = this.props;
    reset();
  }

  render() {
    const { step } = this.props;
    let section = null;
    switch (step) {
      case 1:
        section = <SignupStep1 />;
        break;
      case 2:
        section = <SignupStep2 />;
        break;
      case 3:
        section = <SignupStep3 />;
        break;
      default:
        section = (
          <Message negative>
            <Message.Header>Something went wrong!</Message.Header>
            <p>Please try to reload the page</p>
          </Message>
        );
    }

    return (
      <Segment color="teal">
        {section}
      </Segment>
    );
  }
}

Signup.propTypes = {
  step: PropTypes.number.isRequired,
  reset: PropTypes.func.isRequired,
};

const mapStateToProps = ({ signup: { step } }) => ({ step });

export default connect(mapStateToProps, { reset: resetAction })(Signup);
