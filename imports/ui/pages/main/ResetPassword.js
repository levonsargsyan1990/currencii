import React from 'react';
import { Segment } from 'semantic-ui-react';

import ResetPasswordForm from '../../components/main/ResetPassword';
import Layout from '../../layouts/main/MainResetPasswordLayout';

const ResetPassword = () => (
  <Layout>
    <Segment color="blue">
      <ResetPasswordForm />
    </Segment>
  </Layout>
);

export default ResetPassword;
