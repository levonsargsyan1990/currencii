import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import {
  Header, Loader, Icon, Dimmer,
} from 'semantic-ui-react';
import i18n from 'meteor/universe:i18n';

import { verifyAndChangeEmail } from '../../../api/users/methods';

const T = i18n.createComponent();

class VerifyEmail extends Component {
  state = {
    error: '',
    loading: true,
  };

  componentDidMount() {
    const { match: { params: { token } } } = this.props;
    if (!token) {
      this.setState({
        loading: false,
        error: 'Invalid URL',
      });
    } else {
      verifyAndChangeEmail.callPromise({ token })
        .then(() => this.setState({
          loading: false,
        }))
        .catch(err => this.setState({
          loading: false,
          error: err.reason,
        }));
    }
  }

  componentDidUpdate() {
    const { loading, error } = this.state;
    const { history } = this.props;
    if (!loading && !error) {
      setTimeout(history.push.bind(null, '/login'), 1000);
    }
  }

  renderContent() {
    const { loading, error } = this.state;
    if (loading) {
      return (
        <Loader size="huge" />
      );
    }
    if (error) {
      return (
        <Header as="h2" icon textAlign="center" color="red">
          <Icon name="delete" color="red" circular />
          <Header.Content>{error}</Header.Content>
        </Header>
      );
    }
    return (
      <Header as="h2" icon textAlign="center" color="green">
        <Icon name="check" color="green" circular />
        <Header.Content><T>pages.main.verifyEmail.headerContent</T></Header.Content>
      </Header>
    );
  }

  render() {
    return (
      <Dimmer active inverted>
        {this.renderContent()}
      </Dimmer>
    );
  }
}

VerifyEmail.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  history: ReactRouterPropTypes.history.isRequired,
};

export default withRouter(VerifyEmail);
