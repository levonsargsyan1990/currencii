// TODO add 404 page

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { withTracker } from 'meteor/react-meteor-data';
import {
  withRouter, Route, Switch, Redirect,
} from 'react-router-dom';
import { Loader, Dimmer } from 'semantic-ui-react';
import isEqual from 'lodash/isEqual';

import Layout from '../../layouts/main/MainLayout';
import RegistrationLayout from '../../layouts/main/MainRegistrationLayout';
import DashboardLayout from '../../layouts/main/MainDashboardLayout';
import Exchange from './Exchange';
import Transactions from './Transactions';
import Verification from './Verification';
import Settings from './Settings';
import Profile from './Profile';
import Signup from './Signup';
import Login from './Login';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';
import VerifyEmail from './VerifyEmail';
import withGoogleTracker from '../../components/main/withTracker';

// Route wrapper
const DashboardRoute = ({ component: Content, user, ...rest }) => {
  if (!user || !Roles.userIsInRole(user._id, ['client'])) {
    const { location: { search } } = rest;
    return (
      <Redirect to={{
        pathname: '/login',
        search,
      }}
      />
    );
  }
  return (
    <Route
      {...rest}
      render={matchProps => (
        <DashboardLayout>
          <Content {...matchProps} />
        </DashboardLayout>
      )}
    />
  );
};

DashboardRoute.propTypes = {
  component: PropTypes.func.isRequired,
  user: PropTypes.shape({}),
};

DashboardRoute.defaultProps = {
  user: null,
};

const RegistrationRoute = ({ component: Content, user, ...rest }) => {
  if (user) {
    if (Roles.userIsInRole(user._id, ['client'])) {
      const { location: { search } } = rest;
      return (
        <Redirect to={{
          pathname: '/exchange',
          search,
        }}
        />
      );
    }
  }
  return (
    <Route
      {...rest}
      render={matchProps => (
        <RegistrationLayout>
          <Content {...matchProps} />
        </RegistrationLayout>
      )}
    />
  );
};

RegistrationRoute.propTypes = {
  component: PropTypes.func.isRequired,
  user: PropTypes.shape({}),
};

RegistrationRoute.defaultProps = {
  user: null,
};

class Main extends Component {
  shouldComponentUpdate({ user, location: { pathname } }) {
    const { user: oldUser, location: { pathname: oldPathname } } = this.props;
    return !(isEqual(user, oldUser) && pathname === oldPathname);
  }

  render() {
    const { isLoggingIn, user } = this.props;
    if (isLoggingIn) {
      return (
        <Dimmer active inverted>
          <Loader size="huge" />
        </Dimmer>
      );
    }
    return (
      <Layout>
        <Switch>
          <DashboardRoute user={user} exact path="/exchange" component={withGoogleTracker(Exchange)} />
          <DashboardRoute user={user} exact path="/transactions" component={withGoogleTracker(Transactions)} />
          <DashboardRoute user={user} exact path="/verification" component={withGoogleTracker(Verification)} />
          <DashboardRoute user={user} exact path="/settings" component={withGoogleTracker(Settings)} />
          <DashboardRoute user={user} exact path="/profile" component={withGoogleTracker(Profile)} />
          <RegistrationRoute user={user} path="/signup" component={withGoogleTracker(Signup)} />
          <RegistrationRoute user={user} path="/login" component={withGoogleTracker(Login)} />
          <RegistrationRoute user={user} path="/forgot-password" component={withGoogleTracker(ForgotPassword)} />
          <Route path="/reset-password/:token" component={withGoogleTracker(ResetPassword)} />
          <Route path="/verify-email/:token" component={withGoogleTracker(VerifyEmail)} />
        </Switch>
      </Layout>
    );
  }
}

Main.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
  }),
  isLoggingIn: PropTypes.bool.isRequired,
  location: ReactRouterPropTypes.location.isRequired,
};

Main.defaultProps = {
  user: null,
};

const meteorWrappedComponent = withTracker(() => ({
  user: Meteor.user(),
  isLoggingIn: Meteor.loggingIn(),
}))(Main);

export default withRouter(meteorWrappedComponent);
