import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Element, scroller } from 'react-scroll';

import Calculator from '../components/landing/Calculator';
import SendMessageForm from '../components/landing/SendMessageForm';

const StyledLanding = styled.div`
  padding: 0px;
  margin: 0px;
  background-color: #ffffff;
  overflow-x: hidden;

  div {
    font-family: 'Montserrat', sans-serif;
  }

  background-repeat: no-repeat;
  background-position: center;
  background-position-y: -8px;

  .container {
    max-width: 1450px;
    height: 100%;
    margin: 0 auto;
  }

  .frame {
    padding-left: 120px;
    padding-right: 120px;
  }

  .shadow-text {
    font-size: 110px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.73;
    letter-spacing: normal;
  }

  a {
    text-decoration: none;
  }

  textarea:focus, input:focus {
    outline: none;
  }

  button {
    cursor: pointer;
  }

  button:active {
    outline: none;
    border: none;
  }

  button:focus {
    outline: 0;
  }

  input::-webkit-outer-spin-button, input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input[type='number'] {
    -moz-appearance: textfield;
  }

  /* end */

  .sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 2;
    top: 0;
    right: 0;
    background-color: #ffffff;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    text-align: center;
  }

  .sidenav * {
    margin-bottom: 50px;
  }

  .sidenav span, a {
    transition: 0.3s;
    display: block;
    font-family: 'Montserrat';
    font-size: 15px;
    font-weight: 500;
    color: #393248;
    cursor: pointer;
  }

  .open-currency-dropdown-content {
    display: block !important;
  }

  .sidenav .closebtn {
    position: absolute;
    top: 14px;
    right: 30px;
    font-size: 36px;
    margin-left: 50px;
  }

  header {
    padding-top: 12px;
  }

  #header-section {
    padding-bottom: 270px;
    background-image: url('img/landing/AdobeStock_105493483.png');
    background-size: 100%;
    background-repeat: no-repeat;
    background-size: cover;
    /* background-position: center bottom; */
    clip-path: ellipse(84% 80% at 53% 16%);
  }

  .for-explorer {
    display: none;
  }

  @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
    #header-section {
      padding-bottom: 300px !important;
      position: relative;
    }
    .header-section-aside {
      align-items: flex-start !important;
    }
    .for-explorer {
      display: inline-block;
      position: absolute;
      width: 100%;
      max-height: 300px;
      bottom: 0;
    }
    .carusel-users-section .main {
      justify-content: space-around !important;
    }
    .img-container {
      width: 110px;
      justify-content: flex-start !important;
    }
    .how-it-works-item2-text {
      max-width: 300px;
    }
    .ready-to-start-main {
      align-items: flex-start !important;
    }
    .our-team {
      position: relative;
    }
  }

  @supports (-ms-ime-align:auto) {
    #header-section {
      padding-bottom: 300px !important;
      position: relative;
    }
    /* .header-section-aside {
      align-items: flex-start !important;
    } */
    .for-explorer {
      display: inline-block;
      position: absolute;
      width: 100%;
      max-height: 300px;
      bottom: 0;
    }
    .carusel-users-section .main {
      justify-content: space-around !important;
    }
    .img-container {
      width: 110px;
      justify-content: flex-start !important;
    }
    .how-it-works-item2-text {
      max-width: 300px;
    }
    .ready-to-start-main {
      align-items: flex-start !important;
    }
    .our-team {
      position: relative;
    }
  }

  .main-for-header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    position: relative;
  }

  nav ul {
    margin: 0;
    margin-top: -1.5px;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    list-style: none;
  }

  nav ul li {
    padding-right: 50px;
    display: inline-block;
    font-size: 15px;
    font-weight: 500;
  }

  nav ul li span, nav ul li a {
    font-family: 'Montserrat';
    color: #ffffff;
    cursor: pointer;
  }

  nav ul li:last-child {
    padding-right: 0px !important;
  }

  .navbar-toggle {
    display: none !important;
    cursor: pointer;
  }

  .signup {
    padding-right: 0px;
  }

  .for-sign-up {
    border: 1px solid #ffffff;
    border-radius: 100px;
    width: 160px;
    height: 46px;
  }

  .mobile-sign-up {
    display: flex;
    justify-content: center;
  }

  .border-for-mobile-sign-up {
    border: 1px solid blue;
    height: 46px;
    border-radius: 100px;
  }

  .header-section-aside {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .exchange-for-business {
    margin-top: 215px;
    padding-left: 10px;
    padding-right: 10px;
  }

  .exchange-for-business .big-text {
    font-family: 'Montserrat';
    max-width: 645px;
    height: 116px;
    font-size: 50px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.14;
    letter-spacing: normal;
    color: #ffffff;
  }

  .small-text {
    margin-top: 20px;
    font-family: 'Montserrat';
    font-size: 18px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #ffffff;
  }

  .calc {
    height: auto;
    width: 404px;
    border: 1px solid #ffffff;
    border-radius: 10px;
    background-color: #ffffff;
    margin-top: 32px;
  }

  .calc-main {
    display: flex;
    flex-direction: column;
    margin-top: 5px;
  }

  .calc-inputs {
    display: flex;
    flex-direction: column;
  }

  .calc-input {
    display: flex;
    justify-content: space-between;
    padding-left: 10px;
    padding-right: 10px;
    height: 67px;
    margin: 15px 20px 15px 20px;
    box-shadow: 0 0 10px 0 rgba(71, 71, 71, 0.1);
    background-color: #ffffff;
  }

  .calc-input:last-child {
    margin-top: 5px;
    margin-bottom: 20px !important;
  }

  .calc-input label {
    position: absolute;
    margin-top: 9px;
    width: 53px;
    height: 18px;
    opacity: 0.52;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.4px;
    color: #393248;
  }

  .calc-input input {
    border: none;
    padding-top: 31px;
    padding-bottom: 16px;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 16px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.5px;
    color: #393248;
  }

  .currency-dropdown {
    display: inline-block;
    position: relative;
    top: 13px;
    width: 89px;
    height: 40px;
    border-radius: 4px;
    background-color: #f2eded;
    cursor: pointer;
  }

  .currency-dropdown-content {
    display: none;
    position: absolute;
    width: 100%;
    z-index: 1;
    background: #f2eded;
    margin-top: -12px;
    border-bottom-left-radius: 5px
  }

  .currency-dropdown-content .currency {
    padding-right: 13px;
  }

  .currency-dropdown-content>div {
    padding-top: 15px !important;
  }

  .currency-dropdown-row {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    border-radius: 4px;
    height: 100%;
    img {
      width: 21px;
      height: 15px;
    }
  }

  .currency-dropdown-row:first-child {
    padding-left: 2px;
    padding-right: 2px;
  }

  .currency-dropdown-content .currency-dropdown-row>div {
    width: 48px;
    text-align: start;
  }

  .currency {
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.3px;
    color: #393248;
  }

  .market-rate {
    display: flex;
    justify-content: space-between;
    padding-right: 20px;
    padding-left: 20px;
    margin-bottom: 20px;
  }

  .market-rate-text {
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 15px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 40px;
    letter-spacing: 0.5px;
    color: #393248;
    text-align: center;
    vertical-align: middle;
  }

  .market-rate-chip {
    width: 88px;
    height: 40px;
    border-radius: 26.5px;
    background-color: #f7f7f7;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 16px;
    font-weight: 900;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: 0.5px;
    color: #6050b2;
    text-align: center;
    vertical-align: middle;
    line-height: 40px;
  }

  .save-up {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    background-color: #f7f7f7;
    margin-top: 10px;
    height: 40px;
  }

  .save-up-text {
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #6050b2;
  }

  .proceed {
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-top: 25px;
    padding-bottom: 25px;
  }

  .save-up-text span {
    font-weight: 800;
  }

  .compare-prices {
    margin-top: 16px;
    display: flex;
    justify-content: center;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #13d3e3;
    padding-bottom: 15px;
  }

  #exchange-marketplace-section {
    padding-bottom: 60px;
    margin-top: -65px;
  }

  #exchange-marketplace-section .container {
    position: relative;
  }

  .exchange-marketplace-article {
    display: flex;
  }

  .market-place {
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.83;
    letter-spacing: normal;
    color: #393248;
    margin-top: 133px;
    position: relative;
  }

  .info {
    margin-top: 38px;
    max-width: 688px;
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.75;
    letter-spacing: normal;
    color: #657280;
  }

  .exchange-marketplace-main {
    margin-top: 120px;
  }

  .exchange-marketplace-row {
    display: flex;
    flex-direction: row;
  }

  .exchange-marketplace-row:last-child {
    margin-top: 90px;
  }

  .item-1 {
    display: flex;
    flex-direction: column;
  }

  .item-2 {
    display: flex;
    flex-direction: column;
    margin-left: 128px;
  }

  .item-header {
    font-family: 'Montserrat';
    font-size: 22px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 4.4px;
    color: #6050b2;
  }

  .mobile-item-header {
    font-family: 'Montserrat';
    display: none;
    font-size: 22px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 4.4px;
    color: #6050b2;
  }

  .save-money {
    display: flex;
    justify-content: flex-end;
    width: 193px;
  }

  .consistent {
    display: flex;
    justify-content: flex-end;
    width: 449px;
  }

  .fees {
    display: flex;
    justify-content: flex-end;
    width: 257px;
  }

  .secure {
    display: flex;
    justify-content: flex-end;
    width: 210px;
  }

  .item-content {
    margin-top: 29px;
    max-width: 484px;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.64;
    letter-spacing: normal;
    color: #657280;
  }

  .get-started {
    margin-top: 90px;
    display: flex;
    justify-content: center;
  }

  #best-rates-section {
    padding-bottom: 80px;
    background-image: linear-gradient(101deg, #ff6aa4, #ff9549);
  }

  .best-rates {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  .best-rates-header {
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.83;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
    margin-top: 80px;
  }

  .banks-rates-info {
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.75;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
    margin-top: 40px;
  }

  #why-cheaper-section {
    height: 100%;
    padding-bottom: 20px;
  }

  .cheaper-currency {
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding-top: 22px;
  }

  .cheaper-currency-header {
    opacity: 0.14;
    font-size: 80px !important;
    line-height: 1;
    color: #000000;
  }

  .online-platform {
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-top: 35px;
  }

  .online-platform-text {
    font-family: 'Montserrat';
    font-size: 28px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.07;
    letter-spacing: normal;
    text-align: center;
    color: #393248;
  }

  .tabel-section {
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-top: 32px;
  }

  table {
    border-radius: 10px;
    background-color: #f7f7f7;
  }

  table th {
    font-family: Montserrat;
    font-size: 20px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #393248;
  }

  table td {
    text-align: center;
    vertical-align: middle;
  }

  .dot-col {
    width: 20px;
  }

  .td-dot {
    text-align: center;
    vertical-align: middle;
    text-align: center;
  }

  .td-text {
    font-family: Montserrat;
    font-size: 16px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.88;
    letter-spacing: normal;
    color: #393248;
    text-transform: uppercase;
    text-align: left !important;
    vertical-align: text-bottom !important;
  }

  .dot {
    width: 4px;
    height: 4px;
    background-color: #393248;
    display: inline-block;
    border-radius: 50%;
  }

  .costs {
    text-align: left;
    width: 436px;
  }

  .currency-th {
    width: 159px;
  }

  .banks {
    width: 162px;
  }

  .about-currency {
    margin-top: 29px;
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  .about-currency-info {
    border-radius: 20px;
    width: 591px;
    height: 40px;
    background-color: #f7f7f7;
    text-align: center;
    vertical-align: middle;
    line-height: 40px;
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal;
    color: #6050b2;
  }

  #start-saving-section {
    padding-bottom: 50px;
    background-color: #393248;
  }

  .ready-to-start-main {
    height: 100%;
    display: flex;
    justify-content: space-around;
  }

  .ready-to-start-section1 {
    margin-top: 50px;
  }

  .ready-to-start-item1 {
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.83;
    letter-spacing: normal;
    color: #ffffff;
  }

  .ready-to-start-item2 {
    margin-top: 32px;
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.75;
    letter-spacing: normal;
    color: #ffffff;
  }

  .ready-to-start-section2 {
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-right: 11px;
    padding-top: 55px;
  }

  #certification-section {
    padding-bottom: 70px;
    background-color: #f7f7f7;
  }

  .certification-logo {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  .certification-logo img {
    margin-top: 70px;
  }

  .central-bank {
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-top: 40px;
  }

  .central-bank-text {
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.83;
    letter-spacing: normal;
    text-align: center;
    color: #393248;
  }

  .learn-more {
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-top: 38px;
  }

  .learn-more-text {
    font-family: Montserrat;
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.75;
    letter-spacing: normal;
    text-align: center;
    color: #657280;
  }

  .link {
    display: none;
    color: #6050b2;
  }

  #how-it-works-section {
    padding-bottom: 91px;
  }

  #how-it-works-section .container {
    position: relative;
  }

  .how-it-works-main {
    height: 100%;
    display: flex;
    flex-direction: column;
  }

  .how-it-works-section1 {
    display: flex;
    flex-direction: row;
    margin-top: 137px;
    align-items: center;
  }

  .how-it-works-item1 {
    position: relative;
    z-index: 1;
  }

  .gif {
    width: 556px;
    height: 308px;
    position: absolute;
    top: 24px;
    left: 65px;
  }

  .how-it-works-item2 {
    margin-left: 24px;
    margin-top: 90px;
    text-align: center;
  }

  .how-it-works-item2-text {
    font-family: Montserrat;
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.75;
    letter-spacing: normal;
    color: #657280;
  }

  .how-it-works-button-section {
    margin-top: 40px;
  }

  .how-it-works-section2 {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    margin-top: 70px;
  }

  .how-it-works-section2>div {
    display: flex;
  }

  .how-it-works-section2-text {
    float: right;
    margin-left: 15px;
    margin-top: 10px;
    font-family: 'Montserrat';
    font-size: 20px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.44;
    letter-spacing: normal;
    color: #393248;
  }

  .oval {
    float: left;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    border: 2px solid #4b4b4b;
    line-height: 40px;
    text-align: center;
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: 3.2px;
    color: #6050b2;
  }

  #feedbacks-section {
    position: relative;
    background-color: #f7f7f7;
  }

  .carusel {
    position: relative;
    height: 690px;
    background: linear-gradient(114deg, #00d2ff, #3ad5a8);
    -webkit-clip-path: ellipse(84% 77% at 53% 22%);
    clip-path: ellipse(84% 77% at 53% 22%);
  }

  .carusel-header-section {
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-top: 39px;
  }

  .carusel-header {
    opacity: 0.05;
    color: #000000;
  }

  .carusel-img-section {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  .carusel-img-section img {
    margin-top: 53px;
  }

  .carusel-info-section {
    display: flex;
    flex-direction: row;
    justify-content: center;
    margin-top: -45px;
  }

  .carusel-info-text {
    height: 130px;
    width: 882px;
    font-family: 'Montserrat';
    font-size: 26px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.77;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
  }

  .carusel-users-section {
    padding-right: 208px;
    padding-left: 208px;
  }

  .main {
    width: 100%;
    height: 100%;
    border: 1px solid;
    border-top: none;
    border-right: none;
    border-left: none;
    border-bottom-color: rgba(255, 255, 255, 0.36);
    border-bottom-width: 3px;
    display: flex;
    justify-content: space-between;
  }

  .img-container {
    width: 120px;
    height: 120px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: flex-end;
  }

  .main-for-circle {
    width: 110px;
    height: 110px;
    position: absolute;
    top: 10px;
  }

  .circle-flex {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
  }

  .circle {
    width: 67px;
    height: 67px;
    border-radius: 50%;
    background-size: 67px 67px;
  }

  .customer_1 {
    background-image: url('../img/landing/Customers/Ansis\ Lipenitis\ -\ CEO\ at\ Motivio.jpg');
    background-repeat: no-repeat;
    background-position: center;
  }

  .customer_2 {
    background-image: url('../img/landing/Customers/Fabrice\ Amalaman\ -\ CEO\ of\ PayQin.jpg');
    background-repeat: no-repeat;
    background-position: center;
  }

  .customer_3 {
    background-image: url('../img/landing/Customers/Aleksandr\ \ Shvaikov\ -\ CEO\ of\ Orocon.jpg');
    background-repeat: no-repeat;
    background-position: center;
  }

  .customer_4 {
    background-image: url('../img/landing/Customers/Ieva\ Johnsson.jpg');
    background-repeat: no-repeat;
    background-position: center;
  }

  .active {
    border: 1px solid #ffffff;
    border-top: none;
    border-right: none;
    border-left: none;
    border-bottom-width: 3px;
  }

  .active-img {
    width: 103px;
    height: 103px;
    background-size: 103px 103px !important;
  }

  .user {
    display: none;
  }

  .user-position {
    display: none;
  }

  .carusel-users-names {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  .carusel-users-names-main {
    display: flex;
    flex-direction: column;
    margin-top: 27px;
  }

  .carusel-selected-user-name {
    font-family: 'Montserrat';
    font-size: 22px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.45;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
  }

  .positions {
    margin-top: 9px;
    font-family: 'Montserrat';
    font-size: 12px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.4px;
    text-align: center;
    color: #ffffff;
  }

  .customers {
    background-color: #f7f7f7;
    padding-bottom: 40px;
  }

  .about-customers-main {
    margin-top: 93px;
  }

  .about-customers {
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.39;
    letter-spacing: normal;
    color: #393248;
  }

  .solutions {
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.44;
    letter-spacing: normal;
    color: #242424;
    margin-top: 15px;
  }

  .areas-row {
    display: flex;
    padding-right: 70px;
    flex: 1;
  }

  .areas-row>div {
    flex: 1;
    align-items: center;  
    display: flex;
  }

  .areas-items {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-top: 40px;
  }

  .areas-img1 {
    background-image: url('img/landing/software.png');
  }

  .areas-img2 {
    background-image: url('img/landing/travel.png');
  }

  .areas-img3 {
    background-image: url('img/landing/trade.png');
  }

  .areas-img4 {
    background-image: url('img/landing/pluas.png');
  }

  .areas-img {
    width: 78px;
    height: 78px;
    border-radius: 50%;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
    background-color: #ffffff;
    max-height: 100%;
    max-width: 100%;
    background-position: 50% 50%;
    background-repeat: no-repeat;
  }

  .areas-items-text {
    padding-left: 20px;
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.44;
    letter-spacing: normal;
    color: #212121;
  }

  .areas-items-text1 {
    width: 207px;
  }

  .areas-items-text2 {
    width: 169px;
  }

  .areas-items-text3 {
    width: 189px;
  }

  .areas-items-text4 {
    width: 139px;
  }

  #questions-section {
    height: 100%;
    padding-bottom: 70px;
  }

  .questions-section-subheader {
    padding-top: 90px;
    position: relative;
    z-index: 1;
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.83;
    letter-spacing: normal;
    text-align: center;
    color: #393248;
  }

  .questions-section-header {
    margin-top: -10px;
    width: 100%;
    font-size: 107px;
    color: #f7f7f7;
  }

  .questions {
    position: relative;
    margin-top: -20px;
  }

  .questions-main {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
  }

  .questions-column1>div {
    max-width: 586px;
    padding-bottom: 40px
  }

  .questions-column2>div {
    max-width: 586px;
    padding-bottom: 40px
  }

  .questions-header {
    font-family: 'Montserrat';
    font-size: 18px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.44;
    letter-spacing: normal;
    color: #393248;
  }

  .questions-info {
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.44;
    letter-spacing: normal;
    color: #242424;
  }

  .questins-button-section {
    clear: both;
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  #about-us-section {
    padding-bottom: 30px;
    background-color: #f7f7f7;
  }

  .read-about-us {
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.11;
    letter-spacing: normal;
    color: #393248;
    padding-top: 30px;
  }

  .about-us-links {
    margin-top: 43px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: wrap;
  }

  .about-us-links img {
    height: 62px;
    max-width: 350px;
  }

  .about-us-links>div {
    padding-left: 50px;
  }

  .about-us-links-row>img:last-child {
    padding-left: 50px;
  }

  #help-section {
    padding-bottom: 57px;
    background-color: #393248;
  }

  .our-team {
    height: 344.7px;
    background: linear-gradient(102deg, #ff6aa4, #ff9549);
    -webkit-clip-path: ellipse(84% 77% at 53% 22%);
    clip-path: ellipse(84% 77% at 53% 22%);
  }

  .help-section-subheader {
    position: relative;
    z-index: 1;
    padding-top: 60px;
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.83;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
  }

  .help-section-header {
    margin-top: -30px;
    opacity: 0.05;
    color: #000000;
    text-align: center;
  }

  .team-members {
    display: flex;
    justify-content: space-between;
    margin-top: -5px;
  }

  .team-members-img {
    background-size: 62px 62px;
    background-repeat: no-repeat;
    background-position: center;
    width: 60px;
    height: 60px;
    border-radius: 50%;
  }

  .member {
    display: flex;
    justify-content: stretch;
    align-items: center;
  }

  .member_1 {
    background-image: url('../img/landing/Team-members/Vahagn\ Grigoryan\ -\ CEO.jpg');
  }

  .member_2 {
    background-image: url('../img/landing/Team-members/Aram\ Keryan\ -\ CTO.jpg');
  }

  .member_3 {
    background-image: url('../img/landing/Team-members/Levon\ Sargsyan\ -\ Developer.jpg');
  }

  .member_4 {
    background-image: url('../img/landing/Team-members/Anahit\ -\ Support.jpg');
  }

  .member-info-container {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }

  .member-info-container div {
    margin: 4px;
    margin-left: 14px;
  }

  .team-members-info {
    font-family: 'Montserrat';
    font-size: 18px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ffffff;
  }

  .team-member-position {
    opacity: 0.76;
    font-family: 'Montserrat';
    font-size: 10px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.4px;
    text-align: center;
    color: #ffffff;
  }

  .start-saving {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .start-saving>div {
    align-items: flex-start;
  }

  .start-saving-header {
    text-align: center;
    margin-top: 90.3px;
    font-family: 'Montserrat';
    font-size: 36px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: 0.83;
    letter-spacing: normal;
    color: #ffffff;
  }

  .start-saving-info {
    text-align: center;
    margin-top: 30px;
    font-family: 'Montserrat';
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.75;
    letter-spacing: normal;
    color: #ffffff;
  }

  .start-saving-button {
    margin-top: 40px;
    text-align: center;
  }

  #contacts-section {
    padding-bottom: 20px;
    background-color: #f7f7f7;
  }

  #contacts-section .container {
    position: relative;
  }

  .contacts-header {
    top: 305px;
    width: 100%;
    position: absolute;
    font-size: 107px;
    color: #000000;
    opacity: 0.03;
    pointer-events: none;
  }

  .contacts-section-main {
    display: flex;
    flex-direction: row;
  }

  .main-section1, .contacts {
    display: flex;
    flex-direction: column;
  }

  .main-section1-info {
    margin-top: 26px;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.86;
    letter-spacing: normal;
    color: #6c6c6c;
  }

  .address-subheader {
    font-family: 'Montserrat';
    font-size: 10px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.4px;
    color: #9fa3a7;
  }

  .address-info {
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #212121;
  }

  .contacts-info>div {
    margin-top: 30px;
  }

  .contacts>div {
    margin-top: 7px;
  }

  .main-section2 {
    margin-top: 123px;
    padding-right: 120px;
    display: flex;
    flex-direction: column;
  }

  .send-message-section {
    display: flex;
    flex-direction: column;
  }

  .user-info {
    display: flex;
    flex-direction: row;
    max-width: 570px;
  }

  .user-info>div {
    margin-right: 11px;
  }

  .input1 {
    flex: 1;
    height: 46px;
    font-family: 'Montserrat';
    border-radius: 5px;
    background-color: #ffffff;
    background-image: url('img/landing/name.png');
    background-position: 17px 15px;
    background-repeat: no-repeat;
    padding-left: 40px;
    font-size: 15px;
    border: 1px solid #e9e9ef;
  }

  .input2 {
    flex: 1;
    height: 46px;
    font-family: 'Montserrat';
    border-radius: 5px;
    background-color: #ffffff;
    background-image: url('img/landing/email.png');
    background-position: 17px 15px;
    background-repeat: no-repeat;
    padding-left: 35px;
    margin-left: 12px;
    font-size: 15px;
    border: 1px solid #e9e9ef;
    text-indent: 10px;
  }

  .user-message {
    margin-top: 15px;
    max-width: 550px;
    text-align: end;
  }

  .user-message textarea {
    width: 542px;
    height: 180px;
    padding-top: 12px;
    font-size: 15px;
    border: 1px solid;
    border-radius: 5px;
    border-color: #e9e9ef;
    text-indent: 15px;
    overflow-y: auto;
    overflow-x: hidden;
  }

  .user-message textarea::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    /* color: pink; */
    font-family: 'Montserrat';
    margin-left: 10px;
  }

  .user-message textarea::-moz-placeholder {
    /* Firefox 19+ */
    /* color: pink; */
    font-family: 'Montserrat';
  }

  .user-message textarea:-ms-input-placeholder {
    /* IE 10+ */
    /* color: pink; */
    font-family: 'Montserrat';
  }

  .user-message textarea:-moz-placeholder {
    /* Firefox 18- */
    /* color: pink; */
    font-family: 'Montserrat';
  }

  .user-message>div {
    margin-top: 5px;
  }

  .submit-button {
    margin-top: 20px;
    text-align: right;
  }

  .copyright {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 43px;
  }

  .powered-by {
    display: flex;
    align-items: center;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #6c6c6c;
  }
  .powered-by >div {
    padding-left: 5px;
    padding-right: 5px;
  }

  .powered-by img {
    width: 30px;
  }

  .copyright-text {
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #6c6c6c;
    margin: 0 auto
  }

  .copyright-text span {
    font-weight: bold;
  }

  .exchange-marketplace-header {
    color: #f7f7f7;
    position: absolute;
    right: 32px;
    top: 70px;
  }

  .how-it-works-header {
    color: #f7f7f7;
    top: 90px;
    right: 0;
    position: absolute;
  }

  .under-line {
    width: 74px;
    height: 5px;
    background: linear-gradient(94deg, #00d2ff, #3ad5a8);
    float: right;
  }

  /* buttons */

  .btn {
    height: 46px;
    border-radius: 100px;
    border: none;
  }

  .btn:active {
    position: relative;
    border: 1px solid;
  }

  .mobile-sign-up-btn {
    width: 144px;
    background-color: #ffffff;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.8px;
    color: #6050b2;
  }

  .border-mobile-sign-up :active {
    border: 1px solid #6050b2 !important;
    border-radius: 100px;
  }

  .sign-up {
    width: 160px;
    color: #ffffff;
    background: none;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 46px;
    text-align: center;
    letter-spacing: 2.8px;
  }

  .sign-up :active {
    border: 1px solid white;
  }

  .proceed-button {
    width: 230px;
    background-image: linear-gradient(101deg, #00d2ff, #3ad5a8);
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.8px;
    color: #ffffff;
  }

  .get-started-button {
    width: 232px;
    background-color: #ffffff;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 46px;
    letter-spacing: 2.8px;
    text-align: center;
    color: #6050b2;
  }

  .border-for-get-started {
    border: 1px solid blue;
    border-radius: 100px;
  }

  .compare-button {
    width: 230px;
    background-image: linear-gradient(101deg, #ffffff, #ffffff);
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.8px;
    color: #6050b2;
  }

  .compare-button:active {
    border-color: #ff7c7c;
  }

  .comp-btn {
    margin-top: 40px;
  }

  .ready-to-start-button {
    width: 230px; 
    background-color: #ffffff;
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 46px;
    text-align: center;
    letter-spacing: 2.8px;
    color: #6050b2;
  }

  .how-it-works-button {
    width: 230px;
    margin: auto;
    background-image: linear-gradient(101deg, #6050b2, #6050b2);
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 46px;
    letter-spacing: 2.8px;
    color: #ffffff;
  }

  .contact-us-button {
    width: 230px;
    background-image: linear-gradient(101deg, #6050b2, #6050b2);
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.8px;
    color: #ffffff;
  }

  .start-saving-button-started-now {
    width: 230px;
    background-color: #ffffff;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 46px;
    letter-spacing: 2.8px;
    color: #6050b2;
  }

  .submit-message {
    &:disabled {
      opacity: 0.5;
    }
    width: 130px;
    background-image: linear-gradient(101deg, #6050b2, #6050b2);
    font-family: 'Montserrat';
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 2.8px;
    color: #ffffff;
  }

  /* end */

  @media (min-width:1200px) and (max-width:1429px) {
    .frame {
      padding-left: 100px !important;
      padding-right: 100px !important;
    }
    nav ul li:not(.signup) {
      padding-right: 34px !important;
    }
    .areas-row {
      padding-right: 0px !important;
    }
    .areas-row>div {
      padding-right: 50px !important;
    }
    .mobile-item-header {
      display: none !important;
    }
    .desktop-item-header {
      display: inline-block !important;
      width: 454px !important;
    }
  }

  @media (min-width:768px) and (max-width:1199px) {
    #header-section {
      background-position-x: -175px;
    }
    nav ul li:not(.different) {
      display: none;
    }
    .navbar-toggle {
      display: block !important;
      font-size: 24px;
    }
    .navbar-toggle .fa {
      color: #ffffff;
    }
    .signup {
      padding-right: 37px;
    }
    .frame {
      padding-left: 30px !important;
      padding-right: 30px !important;
    }
    .big-text {
      width: 345px !important;
      height: 110px !important;
      font-size: 30px !important;
      line-height: 1.23 !important;
    }
    .small-text {
      width: 345px !important;
      height: 52px !important;
      font-size: 16px !important;
      font-weight: 500 !important;
      line-height: 1.5 !important;
    }
    .calc {
      width: 344px !important;
    }
    .calc-input {
      margin: 10px 15px 10px 15px !important;
    }
    .market-rate {
      padding-right: 15px !important;
      padding-left: 15px !important;
    }
    .exchange-marketplace-row {
      flex-direction: column !important;
    }
    .item-2 {
      margin-left: 0px !important;
      margin-top: 90px;
    }
    .item-content {
      max-width: 700px !important;
    }
    .exchange-marketplace-header {
      font-size: 80px !important;
      font-weight: 800 !important;
      line-height: 1 !important;
    }
    .market-place {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.31 !important;
    }
    .best-rates-header {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.15 !important;
    }
    .cheaper-currency-header {
      font-size: 40px !important;
      font-weight: 800 !important;
      line-height: 2 !important;
    }
    .online-platform-text {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.15 !important;
    }
    table {
      max-width: 704px;
    }
    .currency-th {
      width: 0px !important;
    }
    .banks {
      width: 0px !important;
    }
    .ready-to-start-item1 {
      font-size: 28px !important;
      font-weight: 800 !important;
      line-height: 1.07 !important;
    }
    .central-bank-text {
      font-size: 28px !important;
      font-weight: 800 !important;
      line-height: 1.07 !important;
    }
    .how-it-works-header {
      font-size: 80px !important;
      font-weight: 800 !important;
      line-height: 1 !important;
    }
    .how-it-works-section1 {
      flex-direction: column !important;
    }
    .how-it-works-button-section {
      text-align: center;
    }
    .how-it-works-section2 {
      flex-wrap: wrap !important;
      align-content: space-between !important;
    }
    .how-it-works-section2>div {
      padding-right: 69px;
      padding-bottom: 30px;
    }
    .carusel-header {
      font-size: 72px !important;
      font-weight: 800 !important;
      line-height: 1 !important;
    }
    .carusel-users-section {
      padding-left: 20px !important;
      padding-right: 20px !important;
    }
    .about-customers {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.54 !important;
    }
    .questions-section-header {
      font-size: 56px !important;
      font-weight: 800 !important;
      line-height: 1.33 !important;
    }
    .questions-section-subheader {
      padding-top: 50px !important;
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.15 !important;
    }
    .questions-column1>div {
      width: 344px !important;
    }
    .questions-column2>div {
      width: 344px !important;
    }
    .questions-info {
      font-size: 14px !important;
    }
    .read-about-us {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.54 !important;
    }
    .help-section-subheader {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.15 !important;
    }
    .help-section-header {
      font-size: 80px !important;
      font-weight: 800 !important;
      line-height: 1 !important;
    }
    .start-saving-header {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.15 !important;
    }
    .contacts-section-main {
      flex-direction: column !important;
    }
    .main-section2 {
      padding-right: 32px !important;
      padding-left: 32px !important;
    }
    .areas-items {
      flex-direction: column !important;
    }
    .areas-row {
      padding-bottom: 30px;
      flex: 1 0 800px;c
      max-width: 800px;
      justify-content: space-between;
    }
    .areas-item4 {
      padding-right: 130px !important;
    }
    .team-members {
      flex-wrap: wrap !important;
    }
    .member {
      width: 50% !important;
      padding-bottom: 40px;
    }
    .our-team {
      height: 380px !important;
    }
    .contacts-header {
      font-size: 80px !important;
      top: 155px !important;
    }
  }

  @media (min-width:375px) and (max-width:767px) {
    #header-section {
      clip-path: none !important;
      background-position-x: -230px !important;
    }
    nav ul li {
      display: none;
    }
    .navbar-toggle {
      display: block !important;
      font-size: 24px;
    }
    .navbar-toggle .fa {
      color: #ffffff;
    }
    .frame {
      padding-left: 24px !important;
      padding-right: 24px !important;
    }
    .header-section-aside {
      flex-direction: column !important;
      align-items: center;
    }
    .exchange-for-business {
      max-width: 344px;
    }
    .big-text {
      width: 345px !important;
      height: 110px !important;
      font-size: 30px !important;
      line-height: 1.23 !important;
    }
    .small-text {
      width: 345px !important;
      height: 52px !important;
      font-size: 16px !important;
      font-weight: 500 !important;
      line-height: 1.5 !important;
    }
    .calc {
      width: 344px !important;
      box-shadow: 0 2px 33px 0 rgba(0, 0, 0, 0.15) !important;
      position: absolute !important;
      top: 415px !important;
    }
    .calc-input {
      margin: 10px 15px 10px 15px !important;
    }
    .market-rate {
      padding-right: 15px !important;
      padding-left: 15px !important;
    }
    #exchange-marketplace-section {
      margin-top: 225px !important;
    }
    .exchange-marketplace-row {
      flex-direction: column !important;
    }
    .item-2 {
      margin-left: 0px !important;
      margin-top: 90px;
    }
    .item-content {
      max-width: 500px !important;
    }
    .exchange-marketplace-header {
      width: 375px !important;
      font-size: 50px !important;
    }
    .market-place {
      font-size: 26px !important;
    }
    .info {
      font-size: 16px !important;
      font-weight: 500 !important;
      line-height: 1.75 !important;
    }
    .best-rates-header {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.15 !important;
    }
    .cheaper-currency-header {
      font-size: 24px !important;
      font-weight: 800 !important;
      line-height: 1.25 !important;
      text-align: center !important;
    }
    .online-platform-text {
      font-size: 20px !important;
      font-weight: 800 !important;
      line-height: 1.5 !important;
    }
    table {
      border-collapse: collapse;
    }
    table th {
      font-size: 16px !important;
      line-height: 1.88 !important;
    }
    .td-text {
      font-size: 12px !important;
      font-weight: 300 !important;
      line-height: 2.5 !important;
    }
    .banks {
      width: 130px !important;
    }
    .currency-th {
      width: fit-content !important;
    }
    .about-currency-info {
      font-size: 14px !important;
      line-height: 1.43;
    }
    .ready-to-start-main {
      flex-direction: column !important;
      align-items: center;
    }
    .ready-to-start-item1 {
      font-size: 18px !important;
      font-weight: 800 !important;
      line-height: 1.67 !important;
    }
    .ready-to-start-item2 {
      font-size: 14px !important;
      line-height: 1.43 !important;
      max-width: 300px;
    }
    .central-bank-text {
      font-size: 22px !important;
      line-height: 1.36 !important;
    }
    .learn-more-text {
      font-size: 16px !important;
      line-height: 1.75 !important;
    }
    .how-it-works-header {
      font-size: 40px !important;
      line-height: 2 !important;
    }
    .how-it-works-section1 {
      flex-direction: column !important;
    }
    .how-it-works-item1 img:not(:last-child) {
      width: 327px !important;
    }
    .gif {
      width: 266px !important;
      height: 145px !important;
      top: 12px !important;
      left: 30px !important;
    }
    .how-it-works-item2-text {
      font-size: 16px !important;
      line-height: 1.75 !important;
    }
    .how-it-works-section2 {
      flex-direction: column !important;
    }
    .how-it-works-section2>div {
      padding-bottom: 48px !important;
      padding-right: 0px !important;
    }
    .carusel {
      clip-path: ellipse(140% 77% at 50% 22%) !important;
    }
    .carusel-header {
      font-size: 34px !important;
      line-height: 2 !important;
    }
    .carusel-info-text {
      font-size: 16px !important;
      line-height: 1.56 !important;
    }
    .about-customers {
      font-size: 24px !important;
      line-height: 1.25 !important;
    }
    .areas-items {
      flex-direction: column !important
    }
    .areas-row {
      flex-direction: column;
    }
    .areas-row>div {
      padding-bottom: 40px;
    }
    .questions-section-header {
      font-size: 24px !important;
      line-height: 3.33 !important;
    }
    .questions-section-subheader {
      font-size: 24px !important;
      line-height: 1.25 !important;
      padding-top: 35px !important;
    }
    .questions-main {
      flex-direction: column !important;
    }
    .questions-info {
      font-size: 14px !important;
    }
    .read-about-us {
      font-size: 26px !important;
      line-height: 1.54 !important;
    }
    .help-section-subheader {
      font-size: 20px !important;
      line-height: 1.5 !important;
    }
    .help-section-header {
      font-size: 60px !important;
      padding-bottom: 20px !important;
    }
    .our-team {
      height: 580px !important;
      clip-path: ellipse(140% 77% at 50% 22%) !important;
    }
    .team-members {
      flex-direction: column !important;
    }
    .team-members>div {
      padding-bottom: 30px !important;
    }
    .start-saving-header {
      font-size: 22px !important;
      line-height: 1.36 !important;
    }
    .contacts-section-main {
      flex-direction: column !important;
    }
    .main-section1-header {
      font-size: 26px !important;
    }
    .main-section2 {
      padding-right: 0px !important;
    }
    .user-info {
      max-width: 550px !important;
      flex-direction: column !important;
      align-items: center;
    }
    .user-info>input {
      margin-left: 0px !important;
      margin-top: 10px !important;
    }
    .user-message {
      display: flex;
      text-align: center !important;
      flex-direction: column;
      align-items: center;
    }
    .user-message textarea {
      width: 270px !important;
      padding-left: 0px !important;
    }
    .mobile-item-header {
      display: inline-block;
    }
    .desktop-item-header {
      display: none !important;
    }
    .consistent {
      width: 215px !important;
    }
    .submit-button {
      width: 274px !important;
    }
    .hide {
      display: none !important;
    }
    .carusel-users-section {
      padding-left: 0px;
      padding-right: 0px;
    }
    .contacts-header {
      font-size: 50px !important;
      top: 215px !important;
    }
    .copyright {
      flex-direction: column !important;
      align-items: center;
    }
  }

  @media (min-width:320px) and (max-width:374px) {
    #header-section {
      clip-path: none !important;
      background-position-x: -230px !important;
    }
    nav ul li {
      display: none;
    }
    nav ul li:last-child {
      padding-right: 3px !important;
    }
    .navbar-toggle {
      display: block !important;
      font-size: 24px;
    }
    .navbar-toggle .fa {
      color: #ffffff;
    }
    .frame {
      padding-left: 5px !important;
      padding-right: 5px !important;
    }
    .header-section-aside {
      flex-direction: column !important;
      align-items: center;
    }
    .exchange-for-business {
      max-width: 320px;
    }
    .big-text {
      width: 320px !important;
      height: 110px !important;
      font-size: 30px !important;
      line-height: 1.23 !important;
    }
    .small-text {
      width: 300px !important;
      height: 52px !important;
      font-size: 16px !important;
      font-weight: 500 !important;
      line-height: 1.5 !important;
    }
    .calc {
      width: 315px !important;
      box-shadow: 0 2px 33px 0 rgba(0, 0, 0, 0.15) !important;
      position: absolute !important;
      top: 450px !important;
    }
    .calc-input {
      margin: 10px 8px 10px 8px !important;
    }
    .calc-input input {
      font-size: 15px !important;
    }
    .calc-input>span {
      margin-left: 10px !important;
    }
    .market-rate {
      padding-right: 8px !important;
      padding-left: 8px !important;
    }
    .market-rate-text {
      font-size: 14px !important;
    }
    #exchange-marketplace-section {
      margin-top: 215px !important;
    }
    .exchange-marketplace-row {
      flex-direction: column !important;
    }
    .item-2 {
      margin-left: 0px !important;
      margin-top: 90px;
    }
    .item-content {
      max-width: 318px !important;
    }
    .exchange-marketplace-header {
      width: 320px !important;
      font-size: 40px !important;
      right: 0px !important;
    }
    .market-place {
      font-size: 26px !important;
    }
    .info {
      font-size: 16px !important;
      font-weight: 500 !important;
      line-height: 1.75 !important;
    }
    .best-rates-header {
      font-size: 26px !important;
      font-weight: 800 !important;
      line-height: 1.15 !important;
    }
    .cheaper-currency-header {
      font-size: 24px !important;
      font-weight: 800 !important;
      line-height: 1.25 !important;
      text-align: center !important;
    }
    .online-platform-text {
      font-size: 20px !important;
      font-weight: 800 !important;
      line-height: 1.5 !important;
    }
    table {
      border-collapse: collapse;
    }
    table th {
      font-size: 16px !important;
      line-height: 1.88 !important;
    }
    .td-text {
      font-size: 12px !important;
      font-weight: 300 !important;
      line-height: 2.5 !important;
    }
    .banks {
      width: 130px !important;
    }
    .currency-th {
      width: fit-content !important;
    }
    .about-currency-info {
      font-size: 14px !important;
      line-height: 1.43;
    }
    .ready-to-start-main {
      flex-direction: column !important;
      align-items: center;
    }
    .ready-to-start-item1 {
      font-size: 18px !important;
      font-weight: 800 !important;
      line-height: 1.67 !important;
    }
    .ready-to-start-item2 {
      font-size: 14px !important;
      line-height: 1.43 !important;
      max-width: 250px;
    }
    .central-bank-text {
      font-size: 22px !important;
      line-height: 1.36 !important;
    }
    .learn-more-text {
      font-size: 16px !important;
      line-height: 1.75 !important;
    }
    .how-it-works-header {
      font-size: 40px !important;
      line-height: 2 !important;
    }
    .how-it-works-section1 {
      flex-direction: column !important;
    }
    .how-it-works-item1 img:not(:last-child) {
      width: 310px !important;
    }
    .gif {
      width: 248px !important;
      height: 145px !important;
      top: 12px !important;
      left: 30px !important;
    }
    .how-it-works-item2-text {
      font-size: 16px !important;
      line-height: 1.75 !important;
    }
    .how-it-works-section2 {
      flex-direction: column !important;
    }
    .how-it-works-section2>div {
      padding-bottom: 48px !important;
      padding-right: 0px !important;
    }
    .carusel {
      clip-path: ellipse(140% 77% at 50% 22%) !important;
    }
    .carusel-header {
      font-size: 34px !important;
      line-height: 2 !important;
    }
    .carusel-users-section .main {
      justify-content: space-around !important;
    }
    .carusel-info-text {
      font-size: 16px !important;
      line-height: 1.56 !important;
    }
    .about-customers {
      font-size: 24px !important;
      line-height: 1.25 !important;
    }
    .areas-items {
      flex-direction: column !important
    }
    .areas-row {
      flex-direction: column;
    }
    .areas-row>div {
      padding-bottom: 40px;
    }
    .questions-section-header {
      font-size: 24px !important;
      line-height: 3.33 !important;
    }
    .questions-section-subheader {
      font-size: 24px !important;
      line-height: 1.25 !important;
      padding-top: 35px !important;
    }
    .questions-main {
      flex-direction: column !important;
    }
    .questions-info {
      font-size: 14px !important;
    }
    .read-about-us {
      font-size: 26px !important;
      line-height: 1.54 !important;
    }
    .about-us-links img {
      max-width: 300px !important;
    }
    .help-section-subheader {
      font-size: 20px !important;
      line-height: 1.5 !important;
    }
    .help-section-header {
      font-size: 60px !important;
      padding-bottom: 20px !important;
    }
    .our-team {
      height: 580px !important;
      clip-path: ellipse(140% 77% at 50% 22%) !important;
    }
    .team-members {
      flex-direction: column !important;
    }
    .team-members>div {
      padding-bottom: 30px !important;
    }
    .start-saving-header {
      font-size: 22px !important;
      line-height: 1.36 !important;
    }
    .contacts-section-main {
      flex-direction: column !important;
    }
    .main-section1-header {
      font-size: 26px !important;
    }
    .main-section2 {
      padding-right: 0px !important;
    }
    .user-info {
      max-width: 550px !important;
      flex-direction: column !important;
      align-items: center;
    }
    .user-info>input {
      margin-left: 0px !important;
      margin-top: 10px !important;
    }
    .user-message {
      display: flex;
      text-align: center !important;
      flex-direction: column;
      align-items: center;
    }
    .user-message textarea {
      width: 270px !important;
      padding-left: 0px !important;
    }
    .mobile-item-header {
      display: inline-block;
    }
    .desktop-item-header {
      display: none !important;
    }
    .consistent {
      width: 215px !important;
    }
    .submit-button {
      width: 274px !important;
    }
    .hide {
      display: none !important;
    }
    .carusel-users-section {
      padding-left: 0px;
      padding-right: 0px;
    }
    .img-container {
      width: 90px;
      height: 120px;
      position: relative;
      display: flex;
      justify-content: center;
      align-items: flex-end;
    }
    .main-for-circle {
      width: 90px;
      height: 107px;
      position: absolute;
      margin-top: 3px;
    }
    .contacts-header {
      font-size: 42px !important;
      top: 250px !important;
    }
    .copyright {
      flex-direction: column !important;
      align-items: center;
    }
  }
`;

class Landing extends Component {
  userReviews = {
    user1: 'Fast, convenient and inexpensive. Good when one wants to save on currency exchange, compared to what banks can offer.',
    user2: 'With the savings generated by Currencii. I hired one more employee.',
    user3: 'Currencii always offers the best rates. No need to go to banks or call your broker. Just do it online... ',
    user4: 'Always good and quick communication, positive negotiation, best exchange rate and deal is always done in the same minute as agreed.',
  };

  compareSection = React.createRef();

  componentDidMount() {
    this.selectedCircle = document.getElementsByClassName('circle');
    this.selectedContainer = document.getElementsByClassName('main-for-circle');
    this.selectedUser = document.getElementsByClassName('user');
    this.selectedUserPosition = document.getElementsByClassName('user-position');
    this.selectedUserReview = document.getElementById('user-review');
    this.mobile_sidenav = document.getElementById('mobile-sidenav');
    this.selectedUserReview.innerHTML = this.userReviews[Object.keys(this.userReviews)[2]];
    this.activateClass(1);
    this.interval = setInterval(this.switchNext.bind(this), 5000);
    const circles = document.getElementsByClassName('circle');
    for (let i = 0; i < circles.length; i += 1) {
      const circle = circles[i];
      const index = i;
      circle.onclick = () => {
        this.changeActive(index);
        clearInterval(interval);
        this.interval = setInterval(this.switchNext.bind(this), 5000);
      }
    }
    this.checkWindowWidth();
    this.scrollToSection();

    window.addEventListener('click', (event) => {
      const signupBtn = document.getElementById('signup-btn')
      for (let i = 0; i < this.mobile_sidenav.childNodes.length; i += 1) {
        if (event.target === this.mobile_sidenav.childNodes[i] || event.target === signupBtn) {
          this.closeNav();
        }
      }
    });
    window.onresize = this.resize;
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  changeActive(index) {
    this.diactivateClass(this.currentIndex);
    this.activateClass(index);
  }

  activateClass(index) {
    this.selectedCircle[index].classList.add('active-img');
    this.selectedContainer[index].classList.add('active');
    this.selectedUser[index].style.display = 'inline-block';
    this.selectedUserPosition[index].style.display = 'inline-block';
    this.selectedUserReview.innerHTML = this.userReviews[Object.keys(this.userReviews)[index]];
    this.currentIndex = index;
  }

  diactivateClass(index) {
    this.selectedCircle[index] && this.selectedCircle[index].classList.remove('active-img');
    this.selectedContainer[index] && this.selectedContainer[index].classList.remove('active');
    this.selectedUser[index].style.display = 'none';
    this.selectedUserPosition[index].style.display = 'none';
  }

  switchNext() {
    let next = this.currentIndex;
    if (window.innerWidth < 768) {
      next += 1;
      if (next == 4) {
        this.changeActive(1);
      } else if (next == 3) {
        this.changeActive(0);
      } else {
        this.changeActive(next);
      }
    } else {
      next += 1;
      if (next > 3) {
        this.changeActive(0);
      } else {
        this.changeActive(next);
      }
    }
  }

  openNav() {
    document.getElementById('mobile-sidenav').style.width = '315px';
  }

  closeNav() {
    document.getElementById('mobile-sidenav').style.width = 0;
  }

  checkWindowWidth() {
    if (window.innerWidth < 768) {
      this.switchNext();
    }
  }

  resize() {
    this.screenWidth = window.innerWidth;
    if (window.innerWidth > 767) {
      this.resizeEventFired = false;
    } else if (!this.resizeEventFired) {
      this.switchNext();
      this.resizeEventFired = true;
    }
  }

  scrollToSection() {
    const nav = document.getElementById('main-nav');
    for (let i = 1; i < nav.childNodes.length - 3; i += 1) {
      const index = i;
      nav.childNodes[index].onclick = () => {
        let elem = nav.childNodes[index].childNodes[0].getAttribute('value');
        elem = elem.substring(1);
        this.smoothScroll(elem, 2000);
      };
    }

    for (let i = 2; i < this.mobile_sidenav.childNodes.length - 2; i += 1) {
      const index = i;
      this.mobile_sidenav.childNodes[index].onclick = () => {
        let elem = this.mobile_sidenav.childNodes[index].getAttribute('value');
        elem = elem.substring(1);
        this.smoothScroll(elem, 2000);
      };
    }
  }

  // eslint-disable-next-line
  smoothScroll(targetElement, duration) {
    const target = document.getElementById(targetElement);
    const targetPosition = target.getBoundingClientRect().top;
    const startPosition = window.pageYOffset;
    const distance = targetPosition - startPosition;
    let startTime = null;
    const ease = (t, b, c, d) => c * Math.sin(t / d * (Math.PI / 2)) + b;
    const animation = (currentTime) => {
      if (startTime === null) {
        startTime = currentTime;
      }
      const timeElapsed = currentTime - startTime;
      const run = ease(timeElapsed, startPosition, distance, duration);
      window.scrollTo(0, run);
      if (timeElapsed < duration) {
        requestAnimationFrame(animation);
      }
    };
    requestAnimationFrame(animation);
  }

  render() {
    return (
      <StyledLanding>
        <div id="mobile-sidenav" className="sidenav">
          <img src="img/landing/logo-black.png" />
          <a href="javascript:void(0)" className="closebtn" onClick={() => this.closeNav()}>&times;</a>
          <Link to="/exchange">Home</Link>
          <span value="#how-it-works-section">How it works</span>
          <span value="#questions-section">FAQ</span>
          <span value="#help-section">Team</span>
          <span value="#contacts-section">Contact us</span>
          <Link to="/login">Login</Link>
          <div className="mobile-sign-up">
            <div className="border-for-mobile-sign-up">
              <Link to="/signup" id="signup-btn" className="mobile-sign-up-btn btn">Sign Up</Link>
            </div>
          </div>
        </div>

        {/* Header section */}
        <section id="header-section">
          <div className="container">
            <header>
              <div className="main-for-header frame">
                <div className="logo-banner">
                  <img src="img/whiteLogo.svg" alt="logo" />
                </div>
                <nav>
                  <ul className="topnav" id="main-nav">
                    <li><Link to="/exchange">Home</Link></li>
                    <li><span value="#how-it-works-section">How it works</span></li>
                    <li><span value="#questions-section">FAQ</span></li>
                    <li><span value="#help-section">Team</span></li>
                    <li><span value="#contacts-section">Contact us</span></li>
                    <li className="different"><Link to="/login">Login</Link></li>
                    <li className="different signup">
                      <div className="for-sign-up">
                        <Link to="/signup" className="sign-up btn">
                          SIGN UP
                        </Link>
                      </div>
                    </li>
                    <li className="navbar-toggle" id="js-navbar-toggle">
                      <span className="icon" onClick={() => this.openNav()}>
                        <i className="fa fa-bars" />
                      </span>
                    </li>
                  </ul>
                </nav>
              </div>
            </header>
            <div className="header-section-aside frame">
              <div className="exchange-for-business">
                <span className="big-text">
                  <span>CURRENCY EXCHANGE</span><br />
                  <span>FOR YOUR BUSINESS</span>
                </span>
                <div className="small-text">
                  Get the best rate for you business.
                </div>
              </div>
              <div className="calc">
                <Calculator />
              </div>
            </div>
          </div>
          <img className="for-explorer" src="img/landing/for-explorer-white.png" />
        </section>

        {/* Currency exchange marketplace */}
        <section id="exchange-marketplace-section">
          <div className="container">
            <span className="shadow-text exchange-marketplace-header">WHAT WE DO</span>
            <div className="exchange-marketplace-article frame">
              <span className="market-place">
                CURRENCY EXCHANGE MARKET PLACE
                <div className="info">
                  Banks are charging up to 6 percent for such a simple and non value adding transaction as
                  currency exchange. We automate the process to make it cheaper.
                </div>
              </span>
            </div>
            <div className="exchange-marketplace-main frame">
              <div className="exchange-marketplace-row">
                <div className="item-1">
                  <span className="item-header">
                    <div>SAVE MONEY</div>
                    <div className="save-money">
                      <div className="under-line" />
                    </div>
                  </span>
                  <span className="item-content">
                      Currencii automates tasks that are usually manual in big banks thus generates savings that
                      are passed on to you.
                  </span>
                </div>
                <div className="item-2">
                  <div className="item-header">
                    <span className="desktop-item-header"> CONSISTENT & TRANSPARENT</span>
                    <span className="mobile-item-header">
                      CONSISTENT &
                      <br />
                      TRANSPARENT
                    </span>
                    <div className="consistent">
                      <div className="under-line" />
                    </div>
                  </div>
                  <div className="item-content">
                    Information about movement of your funds are available to you when you need it.
                  </div>
                </div>
              </div>
              <div className="exchange-marketplace-row">
                <div className="item-1">
                  <div className="item-header">
                    <div>NO HIDDEN FEES</div>
                    <div className="fees">
                      <div className="under-line" />
                    </div>
                  </div>
                  <div className="item-content">
                    Banks, which advertise low fees and rates, as a result charge up to 6%. On the other hand,
                    Currencii doesn't charge any fees and generates savings for its customers.
                  </div>
                </div>
                <div className="item-2">
                  <div className="item-header">
                    <div> SAFE & SECURE</div>
                    <div className="secure">
                      <div className="under-line" />
                    </div>
                  </div>
                  <div className="item-content">
                    Eliminate excessive bank and broker fees. Make more on every business transaction
                    with our
                    low-margin rates. The money you save goes straight to your bottom line
                  </div>
                </div>
              </div>
            </div>
            <div className="get-started">
              <div className="border-for-get-started">
                <Link to="/exchange" className="get-started-button btn">GET STARTED NOW</Link>
              </div>
            </div>
          </div>
        </section>

        <section id="best-rates-section">
          <div className="container aa">
            {/* <div className="rates"> */}
              <div className="best-rates">
                <div className="best-rates-header">WE ALWAYS OFFER THE BEST RATES</div>
              </div>
              <div className="best-rates">
                <div className="banks-rates-info">Currencii VS Banks</div>
              </div>
              <div className="best-rates">
                <div className="comp-btn">
                  <button
                    className="compare-button btn"
                    onClick={() => scroller.scrollTo('why-cheaper-section', {
                      duration: 800,
                      delay: 0,
                      smooth: 'ease'
                    })}
                  >
                    COMAPARE
                  </button>
                </div>
              </div>
            {/* </div> */}
          </div>
        </section>

        {/* Why Currencii is cheaper */}
        <section id="why-cheaper-section" ref={this.compareSection}>
          <Element name="why-cheaper-section" className="element">
            <div className="container">
              <div className="cheaper-currency">
                <div className="shadow-text cheaper-currency-header">WHY CURRENCII IS CHEAPER ?</div>
              </div>

              <div className="online-platform">
                <div className="online-platform-text">100% OLINE PLATFORM FREE OF HUGE COSTS</div>
              </div>

              <div className="tabel-section">
                <div className="tabel">
                  <table cellSpacing="25">
                    <tbody>
                      <tr>
                        <th className="dot-col" />
                        <th className="costs">Costs</th>
                        <th className="currency-th">Currencii</th>
                        <th className="banks">Banks</th>
                      </tr>
                      <tr>
                        <td className="td-dot"><span className="dot" /></td>
                        <td className="td-text">No Physical property</td>
                        <td><img src="img/landing/checkbox.png" /></td>
                        <td><img src="img/landing/close.png" /></td>
                      </tr>
                      <tr>
                        <td className="td-dot"><span className="dot" /></td>
                        <td className="td-text">No adds, organic growth</td>
                        <td><img src="img/landing/checkbox.png" /></td>
                        <td><img src="img/landing/close.png" /></td>
                      </tr>
                      <tr>
                        <td className="td-dot"><span className="dot" /></td>
                        <td className="td-text">Small team of professionals</td>
                        <td><img src="img/landing/checkbox.png" /></td>
                        <td><img src="img/landing/close.png" /></td>
                      </tr>
                      <tr>
                        <td className="td-dot"><span className="dot" /></td>
                        <td className="td-text">Low level of monetary reserves</td>
                        <td><img src="img/landing/checkbox.png" /></td>
                        <td><img src="img/landing/close.png" /></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="about-currency">
                <div className="about-currency-info">
                  Currencii generates savings and passes them on to you.
                </div>
              </div>
            </div>
          </Element>
        </section>

        {/* Ready to start saving */}
        <section id="start-saving-section">
          <div className="container">
            <div className="ready-to-start-main frame">
              <div className="ready-to-start-section1">
                <div className="ready-to-start-item1">
                  READY TO START SAVING ?
                </div>
                <div className="ready-to-start-item2">
                  Our customers have saved a total of over &euro; 200 million. Join the savings today!
                </div>
              </div>
              <div className="ready-to-start-section2">
                <Link to="/exchange" className="ready-to-start-button btn">GET STARTED NOW</Link>
              </div>
            </div>
          </div>
        </section>

        {/* CERTIFICATE FROM CENTRAL BANK */}
        <section id="certification-section">
          <div className="container">
            <div className="certification-logo">
              <img src="img/landing/certificate.png" />
            </div>
            <div className="central-bank">
              <div className="central-bank-text">Licensed by Central Bank</div>
            </div>
            <div className="learn-more">
              <div className="learn-more-text">Currencii is a licensed and Central Bank regulated FinTech company
                <span className="link"><a href='/'> LEARN MORE</a> </span>
              </div>
            </div>
          </div>
        </section>

        {/* how it works */}
        <section id="how-it-works-section">
          <div className="container">
            <span className="shadow-text how-it-works-header">HOW IT WORKS</span>
            <div className="how-it-works-main frame">
              <div className="how-it-works-section1">
                <div className="how-it-works-item1">
                  <img src="img/landing/mac-book.png" />
                  <img className="gif" src="img/landing/currenciigif.gif.gif" />
                </div>
                <div className="how-it-works-item2">
                <div className="how-it-works-item2-text">
                  We are a FinTech company with client first approach. Our mission is to simplify complicated
                  financial terminology and bring the currency management benefits approach. Our mission is
                  to
                  simplify complicated financial .
                </div>
                  <div className="how-it-works-button-section">
                    <Link to="/exchange" className="how-it-works-button btn">GET STARTED</Link>
                  </div>
                </div>
              </div>
              <div className="how-it-works-section2">
                <div className="how-it-works-item3">
                  <div className="oval">1</div>
                  <div className="how-it-works-section2-text">Transfer</div>
                </div>
                <div className="how-it-works-item4">
                  <div className="oval">2</div>
                  <div className="how-it-works-section2-text">Exchange</div>
                </div>
                <div className="how-it-works-item5">
                  <div className="oval">3</div>
                  <div className="how-it-works-section2-text">Save</div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* feedbacks-section */}
        <section id="feedbacks-section">
          <div className="carusel">
            <div className="container">
              <div className="carusel-img-section">
                <div className="carusel-img"><img src="img/landing/Untitled.png" /></div>
              </div>
              <div className="carusel-header-section">
                <span className="shadow-text carusel-header">WHAT THEY SPEAK</span>
              </div>
              <div className="carusel-info-section">
                <div className="carusel-info-text">
                  <span id="user-review"></span>
                </div>
              </div>
              <div className="carusel-users-section">
                <div className="main">
                  <div className="img-container">
                    <div className="main-for-circle">
                      <div className="circle-flex">
                          <div className="circle customer_1" />
                      </div>
                    </div>
                  </div>
                  <div className="img-container">
                    <div className="main-for-circle">
                      <div className="circle-flex">
                          <div className="circle customer_2" />
                      </div>
                    </div>
                  </div>
                  <div className="img-container">
                      <div className="main-for-circle">
                          <div className="circle-flex">
                              <div className="circle customer_3">

                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="img-container hide">
                    <div className="main-for-circle hide">
                      <div className="circle-flex hide">
                        <div className="circle customer_4 hide" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="carusel-users-names">
                  <div className="carusel-users-names-main">
                      <div className="carusel-selected-user-name">
                          <span className="user"> Ansis Lipenitis</span>
                          <span className="user"> Fabrice Amalaman</span>
                          <span className="user"> Aleksandr Shvaikov</span>
                          <span className="user"> Ieva Johnsson</span>
                      </div>
                      <div className="positions">
                          <span className="user-position">CEO at Motivio</span>
                          <span className="user-position">CEO at PayQin</span>
                          <span className="user-position">CEO at Orocon</span>
                          <span className="user-position">CEO/owner at Amie Export Ltd</span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <img className="for-explorer" src="img/landing/elipse-rect-gray-explorer.png" />
          </div>
          <div className="customers">
            <div className="container">
              <div className="about-customers-main frame">
                <div className="about-customers">
                  OUR CUSTOMERS ARE FROM <br />
                  VARIOUS INDUSTRIES
                </div>

                <div className="solutions">
                  Our expertise and knowledge allow us to provide you with tailored industry solutions:
                </div>
                <div className="areas-items">
                  <div className="areas-row">
                    <div className="areas-item1 areas-items-text1">
                      <div className="areas-img1 areas-img">
                      </div>
                      <div className="areas-items-text">
                        IT Companies
                      </div>
                    </div>
                    <div className="areas-item2 areas-items-text2">
                      <div className="areas-img2 areas-img">
                      </div>
                      <div className="areas-items-text">
                        Travel<br /> agencies
                      </div>
                    </div>
                  </div>
                  <div className="areas-row second">
                    <div className="areas-item3 areas-items-text3">
                      <div className="areas-img3 areas-img">
                      </div>
                      <div className="areas-items-text">
                        Trading<br />
                        companies
                      </div>
                    </div>
                    <div className="areas-item4 areas-items-text4">
                      <div className="areas-img4 areas-img">
                      </div>
                      <div className="areas-items-text">
                        Many<br />
                        more
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* Frequently asked questions */}
        <section id="questions-section">
          <div className="container">
            <div className="questions-section-subheader">FREQUENTLY ASKED QUESTIONS</div>
            <div className="shadow-text questions-section-header">STILL HAVE QUESTIONS?</div>
            <div className="questions frame">
              <div className="questions-main">
                <div className="questions-column1">

                  <div className="available-currencies">
                    <div className="questions-header">
                      Who uses Currencii?
                    </div>
                    <div className="questions-info">
                      Currencii is for small and medium sized enterprises that operate with more than one
                      currency.
                    </div>
                  </div>
                  <div className="who-was-currency">
                    <div className="questions-header">
                      Which currencies are available for exchange?
                    </div>
                    <div className="questions-info">
                      AMD, USD, EUR, RUB and GBP
                    </div>
                  </div>

                  <div className="cost-for-customer">
                    <div className="questions-header">
                      What costs will I face as a customer?
                    </div>
                    <div className="questions-info">
                      Currencii does not charge any fees, all the costs are included in the offered exchange
                      rates.
                    </div>
                  </div>
                </div>
                <div className="questions-column2">
                  <div>
                    <div className="questions-header">How safe is Currencii?</div>
                    <div className="questions-info">
                      Currencii is as safe as any other bank.
                    </div>
                  </div>
                    <div>
                      <div className="questions-header">How long does it take to exchange money via Currencii? </div>
                      <div className="questions-info">After we receive your funds, you’ll receive exchanged funds
                        within
                        24 business hours.
                      </div>
                    </div>
                    <div>
                      <div className="questions-header">How much does the registration and verification cost?</div>
                      <div className="questions-info">
                        The registration and verification processes are completely free of charge.
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div className="questins-button-section">
              <div className="questins-button">
                <button
                  className="contact-us-button btn"
                  onClick={() => scroller.scrollTo('contacts-section', {
                    duration: 800,
                    delay: 0,
                    smooth: 'ease'
                  })}
                >
                  CONTACT US
                </button>
              </div>
            </div>
          </div>
        </section>

        {/* read about us */}
        <section id="about-us-section">
          <div className="container">
            <div className="about-us-main frame">
              <div className="read-about-us">
                READ ABOUT US
              </div>
              <div className="about-us-links">
                <a target="_blank" href="https://itel.am/en/news/9738"><img src="img/landing/itel.png" /></a>
                <a target="_blank" href="http://arcticstartup.com/20-international-startups-coming-riga"><img src="img/landing/startup.jpg" /></a>
                <a target="_blank" href="https://itel.am/en/news/9906"><img src="img/landing/itel.png" /></a>
                <a target="_blank" href="https://youtu.be/zHe8nJCKdKs"><img src="img/landing/epic.png" /></a>
              </div>
            </div>
          </div>
        </section>

        {/* OUR TEAM WILL HELP YOU */}
        <section id="help-section">
          <div className="our-team">
            <div className="container">
              <div className="help-section-subheader"><span>OUR TEAM WILL HELP YOU</span></div>
              <div className="shadow-text help-section-header">MEET OUR TEAM</div>
              <div className="team-members frame">
                <div className="member">
                  <div className="team-members-img member_1"></div>
                  <div className="member-info-container">
                    <div className="team-members-info">Vahagn</div>
                    <div className="team-member-position">CEO</div>
                  </div>
                </div>
                <div className="member">
                  <div className="team-members-img member_2"></div>
                  <div className="member-info-container">
                    <div className="team-members-info">Aram</div>
                    <div className="team-member-position">CTO</div>
                  </div>
                </div>
                <div className="member">
                  <div className="team-members-img member_3"></div>
                  <div className="member-info-container">
                    <div className="team-members-info">Levon</div>
                    <div className="team-member-position">Software developer</div>
                  </div>
                </div>
                <div className="member">
                  <div className="team-members-img member_4"></div>
                  <div className="member-info-container">
                    <div className="team-members-info">Anahit</div>
                    <div className="team-member-position">support</div>
                  </div>
                </div>
              </div>
            </div>
            <img className="for-explorer" src="img/landing/for-explorer-dark-blue.png" />
          </div>
          <div className="start-saving">
            <div className="container">
              <div className="start-saving">
                <div className="start-saving-header">READY TO START SAVING ?</div>
                {/* <div className="start-saving-info">Our customers have saved a total of over €200 million. Join the
                    savings today!</div> */}
                <div className="start-saving-button">
                  <Link to="/exchange" className="start-saving-button-started-now btn">GET STARTED NOW</Link>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* CONTACTS */}
        <section id="contacts-section">
          <Element name="contacts-section">
            <div className="container">
              <div className="shadow-text contacts-header">CONTACT US</div>
              <div className="contacts-section-main">
                <div className="main-section1 frame">
                  <div className="main-section1-info">
                    Please contact us if you want to learn more about Currencii. Our support service is happy to
                    answer all your questions.
                  </div>
                  <div className="contacts-info">
                    <div className="contacts address">
                      <div className="address-subheader">ADDRESS</div>
                      <div className="address-info">Leborska 3B, 80-386 Gdansk, Poland </div>
                    </div>
                    <div className="contacts phone">
                      <div className="address-subheader">PHONE</div>
                      <div className="address-info">(+48) 572-192-120</div>
                    </div>
                    <div className="contacts email">
                      <div className="address-subheader">Email</div>
                      <div className="address-info">
                        <address className="address-info">
                          <a className="address-info" href="mailto:info@currencii.eu">info@currencii.eu</a>
                        </address>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="main-section2">
                  <div className="send-message-section">
                    <SendMessageForm />
                  </div>
                </div>
              </div>
              <div className="copyright">
                {/* <div className="powered-by">
                  <span>Powered by</span>
                  <div>
                    <img src="img/landing/alienlab_logo.svg" />
                  </div>
                  <span> Alienlab</span>
                </div> */}
                <div className="copyright-text">Copyright Currencii LLC<span> 2019.</span></div>
              </div>
            </div>
          </Element>
        </section>
      </StyledLanding>
    )
  }
}

export default Landing;