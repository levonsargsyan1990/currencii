import React from 'react';
import styled from 'styled-components';
import { Container } from 'semantic-ui-react';

import RateTable from '../../components/admin/rates/RateTable';

const StyledContainer = styled.div`
  padding: 100px 0;
`;

const Rates = () => (
  <StyledContainer>
    <Container text>
      <RateTable />
    </Container>
  </StyledContainer>
);

export default Rates;
