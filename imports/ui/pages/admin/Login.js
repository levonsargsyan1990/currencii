import React from 'react';

import { Segment } from 'semantic-ui-react';

import LoginForm from '../../components/admin/LoginForm';

const Login = () => (
  <Segment color="blue">
    <LoginForm />
  </Segment>
);

export default Login;
