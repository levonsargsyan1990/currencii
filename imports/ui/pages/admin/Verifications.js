
import React from 'react';
import styled from 'styled-components';
import { Container } from 'semantic-ui-react';

import VerificationTable from '../../components/admin/verifications/VerifiactionTable';

const StyledContainer = styled.div`
  padding: 100px 0;
`;

const Verifications = () => (
  <StyledContainer>
    <Container>
      <VerificationTable />
    </Container>
  </StyledContainer>
);

export default Verifications;
