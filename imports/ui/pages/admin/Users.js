
import React from 'react';
import styled from 'styled-components';
import { Container } from 'semantic-ui-react';

import UserTable from '../../components/admin/users/UserTable';

const StyledContainer = styled.div`
  padding: 100px 0;
`;

const Users = () => (
  <StyledContainer>
    <Container>
      <UserTable />
    </Container>
  </StyledContainer>
);

export default Users;
