// TODO add 404 page

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { withTracker } from 'meteor/react-meteor-data';
import {
  withRouter, Route, Switch, Redirect,
} from 'react-router-dom';
import { Loader, Dimmer } from 'semantic-ui-react';
import isEqual from 'lodash/isEqual';

import Layout from '../../layouts/admin/AdminLayout';
import RegistrationLayout from '../../layouts/admin/AdminRegistrationLayout';
import DashboardLayout from '../../layouts/admin/AdminDashboardLayout';

import Rates from './Rates';
import Transactions from './Transactions';
import Verifications from './Verifications';
import Users from './Users';
import Login from './Login';
import withGoogleTracker from '../../components/main/withTracker';

// Route wrapper
const AdminRoute = ({ component: Content, user, ...rest }) => {
  if (!user || !Roles.userIsInRole(user._id, ['admin'])) {
    return <Redirect to="/admin/login" />;
  }
  return (
    <Route
      {...rest}
      render={matchProps => (
        <DashboardLayout>
          <Content {...matchProps} />
        </DashboardLayout>
      )}
    />
  );
};

const RegistrationRoute = ({ component: Content, user, ...rest }) => {
  if (user) {
    if (Roles.userIsInRole(user._id, ['admin'])) {
      return <Redirect to="/admin/transactions" />;
    }
  }
  return (
    <Route
      {...rest}
      render={matchProps => (
        <RegistrationLayout>
          <Content {...matchProps} />
        </RegistrationLayout>
      )}
    />
  );
};

class Admin extends Component {
  shouldComponentUpdate({ user, location: { pathname } }) {
    const { user: oldUser, location: { pathname: oldPathname } } = this.props;
    return !(isEqual(user, oldUser) && pathname === oldPathname);
  }

  render() {
    const { isLoggingIn, user } = this.props;
    if (isLoggingIn) {
      return (
        <Dimmer active inverted>
          <Loader size="huge" />
        </Dimmer>
      );
    }
    return (
      <Layout>
        <Switch>
          <AdminRoute user={user} path="/admin/rates" component={withGoogleTracker(Rates)} />
          <AdminRoute user={user} path="/admin/transactions" component={withGoogleTracker(Transactions)} />
          <AdminRoute user={user} path="/admin/verifications" component={withGoogleTracker(Verifications)} />
          <AdminRoute user={user} path="/admin/users" component={withGoogleTracker(Users)} />
          <RegistrationRoute user={user} path="/admin/login" component={withGoogleTracker(Login)} />
        </Switch>
      </Layout>
    );
  }
}

Admin.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
  }),
  isLoggingIn: PropTypes.bool.isRequired,
  location: ReactRouterPropTypes.location.isRequired,
};

Admin.defaultProps = {
  user: null,
};

const meteorWrappedComponent = withTracker(() => ({
  user: Meteor.user(),
  isLoggingIn: Meteor.loggingIn(),
}))(Admin);

export default withRouter(meteorWrappedComponent);
