// TODO: add lazy loading.

import React from 'react';
import styled from 'styled-components';
import { Container, Divider } from 'semantic-ui-react';

import CreatedTransactions from '../../components/admin/transactions/CreatedTransactions';
import ReceivedTransactions from '../../components/admin/transactions/ReceivedTransactions';
import SentTransactions from '../../components/admin/transactions/SentTransactions';

const StyledContainer = styled.div`
  padding: 100px 0;
`;

const Transactions = () => (
  <StyledContainer>
    <Container>
      <ReceivedTransactions />
      <Divider hidden />
      <Divider hidden />
      <CreatedTransactions />
      <Divider hidden />
      <Divider hidden />
      <SentTransactions />
    </Container>
  </StyledContainer>
);

export default Transactions;
