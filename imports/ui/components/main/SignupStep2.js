// TODO: set isTouched to true if the form is filled from redux store

import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import i18n from 'meteor/universe:i18n';
import {
  Header, Divider, Form, Label, Button, Container, Dropdown,
} from 'semantic-ui-react';
import findIndex from 'lodash/findIndex';

import { countryOptions, amPostalCodeRegex, lvPostalCodeRegex } from '../../../lib/constants';
import { changeStep as changeStepAction, submitSection2 as submitSection2Action } from '../../actions/signup';

const T = i18n.createComponent();

const SignupStep2 = (props) => {
  const {
    companyName, country, city, address, postalCode, state, changeStep, submitSection2,
  } = props;
  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      initialValues={{
        companyName, country: country || 'Armenia', city, address, postalCode, state,
      }}
      validate={(values) => {
        let postalCodeRegex;
        switch (values.country) {
          case 'Latvia':
            postalCodeRegex = lvPostalCodeRegex;
            break;
          default:
            postalCodeRegex = amPostalCodeRegex;
        }
        const errors = {};
        if (!values.companyName) {
          errors.companyName = i18n.__('main.onboarding.signup2.errors.noCompany');
        } else if (values.companyName.length > 50) {
          errors.companyName = i18n.__('main.onboarding.signup2.errors.longCompany');
        }
        if (!values.country) {
          errors.country = i18n.__('main.onboarding.signup2.errors.noCountry');
        } else if (findIndex(countryOptions, { value: values.country }) === -1) {
          errors.country = i18n.__('main.onboarding.signup2.errors.unsupportedCountry');
        }
        if (!values.city) {
          errors.city = i18n.__('main.onboarding.signup2.errors.noCity');
        }
        if (!values.address) {
          errors.address = i18n.__('main.onboarding.signup2.errors.noAddress');
        }
        if (!values.postalCode) {
          errors.postalCode = i18n.__('main.onboarding.signup2.errors.noPostal');
        } else if (!postalCodeRegex.test(values.postalCode)) {
          errors.postalCode = i18n.__('main.onboarding.signup2.errors.invalidPostal');
        }
        return errors;
      }}
      isInitialValid={({ validate, initialValues }) => {
        const errors = validate(initialValues);
        if (Object.keys(errors).length > 0) {
          return false;
        }
        return true;
      }}
      onSubmit={(values) => {
        submitSection2(values);
        changeStep(3);
      }}
      render={({
        handleSubmit, handleChange, handleBlur, values,
        errors, touched, isSubmitting, setTouched,
      }) => {
        let postalCodePlaceholder;
        switch (values.country) {
          case 'Latvia':
            postalCodePlaceholder = 'e.g. LV-1058';
            break;
          default:
            postalCodePlaceholder = 'e.g. 0069';
        }
        return (
          <Form onSubmit={handleSubmit}>
            <Header as="h3">
              <T>main.onboarding.signup2.title</T>
            </Header>
            <Divider hidden />
            <Form.Field>
              <label>
                <T>main.onboarding.signup2.company</T>
              </label>
              <input
                type="text"
                name="companyName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.companyName}
              />
              {errors.companyName && touched.companyName && (
                <Label pointing basiccolor="red">
                  {errors.companyName}
                </Label>
              )}
            </Form.Field>
            <Form.Group widths="equal">
              <Form.Field>
                <label>
                  <T>main.onboarding.signup2.country</T>
                </label>
                <Dropdown
                  fluid
                  search
                  selection
                  name="country"
                  options={countryOptions}
                  onChange={(event, data) => {
                    setTouched({ country: true });
                    handleChange({ ...event, target: { name: 'country', value: data.value } });
                  }}
                  onBlur={handleBlur}
                  value={values.country}
                  onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                />
                {errors.country && touched.country && (
                  <Label pointing basic color="red">
                    {errors.country}
                  </Label>
                )}
              </Form.Field>
              <Form.Field>
                <label>
                  <T>main.onboarding.signup2.city</T>
                </label>
                <input
                  type="text"
                  name="city"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.city}
                />
                {errors.city && touched.city && (
                  <Label pointing basic color="red">
                    {errors.city}
                  </Label>
                )}
              </Form.Field>
            </Form.Group>
            <Form.Field>
              <label>
                <T>main.onboarding.signup2.address</T>
              </label>
              <input
                type="text"
                name="address"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.address}
              />
              {errors.address && touched.address && (
                <Label pointing basic color="red">
                  {errors.address}
                </Label>
              )}
            </Form.Field>
            <Form.Group widths="equal">
              <Form.Field>
                <label>
                  <T>main.onboarding.signup2.postal</T>
                </label>
                <input
                  type="text"
                  name="postalCode"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.postalCode}
                  placeholder={postalCodePlaceholder}
                />
                {errors.postalCode && touched.postalCode && (
                  <Label pointing basic color="red">
                    {errors.postalCode}
                  </Label>
                )}
              </Form.Field>
              <Form.Field>
                <label>
                  <T>main.onboarding.signup2.state</T>
                </label>
                <input
                  type="text"
                  name="state"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.state}
                />
                {errors.state && touched.state && (
                  <Label pointing basic color="red">
                    {errors.state}
                  </Label>
                )}
              </Form.Field>
            </Form.Group>
            <Container textAlign="center">
              <Button loading={isSubmitting} type="submit" color="green"><T>main.onboarding.signup2.button</T></Button>
            </Container>
          </Form>
        );
      }}
    />
  );
};

SignupStep2.propTypes = {
  companyName: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  postalCode: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  changeStep: PropTypes.func.isRequired,
  submitSection2: PropTypes.func.isRequired,
};

const matStateToProps = ({
  signup: {
    companyName, country, city, address, postalCode, state,
  },
}) => ({
  companyName, country, city, address, postalCode, state,
});

export default connect(matStateToProps, {
  changeStep: changeStepAction, submitSection2: submitSection2Action,
})(SignupStep2);
