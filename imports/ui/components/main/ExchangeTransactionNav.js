import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import i18n from 'meteor/universe:i18n';
import { Container, Grid, Header } from 'semantic-ui-react';
import styled from 'styled-components';

const T = i18n.createComponent();

const StyledContainer = styled.div`
  padding: 100px 0 50px;
  a {
    display: inline-block;
  }
`;

const ExchangeTransactionNav = ({ path, inProgress }) => {
  if (inProgress) {
    return <StyledContainer />;
  }
  return (
    <StyledContainer>
      <Container text>
        <Grid textAlign="center" columns="equal">
          <Grid.Column>
            <Link to="/exchange">
              <Header disabled={path !== 'exchange'} as="h2">
                <T>main.exchange.title</T>
              </Header>
            </Link>
          </Grid.Column>
          <Grid.Column>
            <Link to="/transactions">
              <Header disabled={path !== 'transactions'} as="h2">
                <T>main.transactions.title</T>
              </Header>
            </Link>
          </Grid.Column>
        </Grid>
      </Container>
    </StyledContainer>
  );
};

ExchangeTransactionNav.propTypes = {
  inProgress: PropTypes.bool.isRequired,
  path: PropTypes.string.isRequired,
};

const mapStateToProps = ({ exchange: { inProgress } }) => ({ inProgress });

export default connect(mapStateToProps)(ExchangeTransactionNav);
