// TODO Make company name dynamic
// TODO Logout functionality
// TODO clicks on padding of dropdown menu are not working

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import {
  Header, Container, Menu, Dropdown, Icon,
} from 'semantic-ui-react';

const T = i18n.createComponent();
const { title } = Meteor.settings.public;

const StyledHeader = styled.div`
  .pointing.menu {
    background-color: ${props => props.theme.teal};
    .header, 
    .text, 
    .dropdown.icon {
      color: #fff;
    }
  }
`;

const MainHeader = ({ user }) => (
  <StyledHeader>
    <Menu
      size="huge"
      pointing
      secondary
    >
      <Container>
        <Menu.Item>
          <Link to="/exchange">
            <Header as="h3">
              {title}
            </Header>
          </Link>
        </Menu.Item>
        <Menu.Menu position="right">
          <Dropdown text={user ? user.profile.companyName : ''} pointing className="link item">
            <Dropdown.Menu>
              <Dropdown.Item>
                <Link to="/exchange">
                  <Icon name="home" />
                  <T>main.header.home</T>
                </Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to="/profile">
                  <Icon name="user" />
                  <T>main.header.profile</T>
                </Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to="/verification">
                  <Icon name="calendar check" />
                  <T>main.header.verification</T>
                </Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to="/settings">
                  <Icon name="setting" />
                  <T>main.header.settings</T>
                </Link>
              </Dropdown.Item>
              <Dropdown.Item onClick={() => Meteor.logout()}>
                <Icon name="sign-out alternate" />
                <T>main.header.logout</T>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Menu.Menu>
      </Container>
    </Menu>
  </StyledHeader>
);

MainHeader.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
  }),
};

MainHeader.defaultProps = {
  user: null,
};

export default withTracker(() => ({ user: Meteor.user() }))(MainHeader);
