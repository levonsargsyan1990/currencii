import React from 'react';
import PropTypes from 'prop-types';
import i18n from 'meteor/universe:i18n';
import { Modal } from 'semantic-ui-react';

import VerificationUpload from './VerificationUpload';

const T = i18n.createComponent();

const VerificationModal = ({ isOpen, onClose }) => (
  <Modal
    size="small"
    closeIcon
    dimmer="blurring"
    open={isOpen}
    onClose={onClose}
    closeOnDimmerClick={false}
  >
    <Modal.Header>
      <T>main.verification.modal.title</T>
    </Modal.Header>
    <Modal.Content>
      <VerificationUpload />
    </Modal.Content>
  </Modal>
);

VerificationModal.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

VerificationModal.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default VerificationModal;
