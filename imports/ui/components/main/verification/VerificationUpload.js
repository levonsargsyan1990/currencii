import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import {
  Segment, Header, Divider, Table, Message, Icon,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { Images } from '../../../../api/images/images';
import { sendIdentityFileEmail, sendBusinessFileEmail } from '../../../../api/email/methods';
import { addVerificationImage } from '../../../../api/users/methods';

const T = i18n.createComponent();

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  padding: 0 1rem;
  input {
    display: none; 
  }
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledBasicSegment = styled.div`
  >.segment {
    margin-top: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
  }
`;

const StyledSegment = styled.div`
  >.segment {
    padding: 0;
    margin-bottom: 0px;
  }
`;

const saveFile = ({ file, userId }) => new Promise((resolve, reject) => {
  // eslint-disable-next-line no-undef
  Images.insert(new FS.File(file), (err, fileObj) => {
    if (err) {
      reject(err);
    } else {
      resolve(fileObj);
    }
  }, { userId, type: 'identity' });
});

const readFile = file => new Promise((resolve) => {
  const { name, type } = file;
  const reader = new FileReader();
  reader.onload = () => resolve({ name, fileType: type, file: reader.result });
  reader.readAsBinaryString(file);
});

class VerificationUpload extends Component {
  state = {
    identityUploading: false,
    businessUploading: false,
    error: '',
  };

  identityRef = React.createRef();

  businessRef = React.createRef();

  onIdentityUpload() {
    this.setState({ error: '' });
    this.identityRef.current.click();
  }

  onBusinessUpload() {
    this.setState({ error: '' });
    this.businessRef.current.click();
  }

  handleIdentityUpload(files) {
    const { user } = this.props;
    const [file] = files;
    if (file) {
      this.setState({ identityUploading: true });
      saveFile({ file, userId: user._id })
        .then(savedFile => addVerificationImage.callPromise({
          fileId: savedFile._id,
          verificationType: 'identity',
        }))
        .then(() => readFile(file))
        .then(data => sendIdentityFileEmail.callPromise(data))
        .catch((err) => {
          const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.verification.errors.uploadError');
          this.setState({
            error: `${i18n.__('main.verification.errors.uploadFail')} ${message}`,
            identityUploading: false,
          });
        });
    }
  }

  handleBusinessUpload(files) {
    const { user } = this.props;
    const [file] = files;
    if (file) {
      this.setState({ businessUploading: true });
      saveFile({ file, userId: user._id })
        .then(savedFile => addVerificationImage.callPromise({
          fileId: savedFile._id,
          verificationType: 'business',
        }))
        .then(() => readFile(file))
        .then(data => sendBusinessFileEmail.callPromise(data))
        .catch((err) => {
          const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.verification.errors.uploadError');
          this.setState({
            error: `${i18n.__('main.verification.errors.uploadFail')} ${message}`,
            businessUploading: false,
          });
        });
    }
  }

  renderBusiness() {
    const { business: { status } } = this.props;
    if (status === 'verified') {
      return (
        <p>
          <Icon color="green" name="check" />
          <T>main.verification.business.verified</T>
        </p>
      );
    }
    if (status === 'in-progress') {
      return (
        <p>
          <Icon color="yellow" name="time" />
          <T>main.verification.business.pending</T>
        </p>
      );
    }
    const { businessUploading } = this.state;
    return (
      <StyledBasicSegment>
        <Segment basic loading={businessUploading}>
          <StyledTable>
            <Table stackable basic="very">
              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <T>main.verification.business.notVerified</T>
                  </Table.Cell>
                  <Table.Cell textAlign="right">
                    <AnchorStyledText onClick={() => this.onBusinessUpload()}>
                      <T>main.verification.upload</T>
                      <input
                        type="file"
                        accept="image/jpeg,image/gif,image/png,application/pdf"
                        ref={this.businessRef}
                        onChange={event => this.handleBusinessUpload(event.target.files)}
                      />
                    </AnchorStyledText>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </StyledTable>

        </Segment>
      </StyledBasicSegment>
    );
  }

  renderIdentity() {
    const { identity: { status } } = this.props;
    if (status === 'verified') {
      return (
        <p>
          <Icon color="green" name="check" />
          <T>main.verification.identity.verified</T>
        </p>
      );
    }
    if (status === 'in-progress') {
      return (
        <p>
          <Icon color="yellow" name="time" />
          <T>main.verification.identity.pending</T>
        </p>
      );
    }
    const { identityUploading } = this.state;
    return (
      <StyledBasicSegment>
        <Segment basic loading={identityUploading}>
          <StyledTable>
            <Table stackable basic="very">
              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <T>main.verification.identity.notVerified</T>
                  </Table.Cell>
                  <Table.Cell textAlign="right">
                    <AnchorStyledText onClick={() => this.onIdentityUpload()}>
                      <T>main.verification.upload</T>
                      <input
                        type="file"
                        accept="image/jpeg,image/gif,image/png,application/pdf"
                        ref={this.identityRef}
                        onChange={event => this.handleIdentityUpload(event.target.files)}
                      />
                    </AnchorStyledText>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </StyledTable>
        </Segment>
      </StyledBasicSegment>
    );
  }

  render() {
    const { user } = this.props;
    const { error } = this.state;
    const { identity, business } = this.props;
    return (
      <StyledSegment>
        <Segment basic loading={!user}>
          <Segment basic>
            <Header as="h4">
              <T>main.verification.identity.title</T>
            </Header>
            {this.renderIdentity()}
          </Segment>
          <Divider />
          <Segment basic>
            <Header as="h4">
              <T>main.verification.business.title</T>
            </Header>
            {this.renderBusiness()}
          </Segment>
        </Segment>
        <StyledError>
          <Message attached="bottom" error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
        <StyledError>
          <Message info attached="bottom" hidden={(identity.status === 'not-verified' && business.status === 'not-verified') || (identity.status !== 'not-verified' && business.status !== 'not-verified')}>
            <Icon name="warning" />
            You will be able to perform currency exchange operations soon
            after you upload all the required documents.
            In some cases an online face-to-face verification will be necessary.
            You will be notified via email.
          </Message>
        </StyledError>
      </StyledSegment>
    );
  }
}

VerificationUpload.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
  }),
  business: PropTypes.shape({
    status: PropTypes.string.isRequired,
    verifiedAt: PropTypes.instanceOf(Date),
  }),
  identity: PropTypes.shape({
    status: PropTypes.string.isRequired,
    verifiedAt: PropTypes.instanceOf(Date),
  }),
};

VerificationUpload.defaultProps = {
  user: null,
  business: {
    status: 'not-verified',
    verifiedAt: null,
  },
  identity: {
    status: 'not-verified',
    verifiedAt: null,
  },
};

const mapDataToProps = () => {
  const user = Meteor.user();
  if (user) {
    const { business, identity } = user.profile.verification;
    return { user, business, identity };
  }
  return {};
};

export default withTracker(mapDataToProps)(VerificationUpload);
