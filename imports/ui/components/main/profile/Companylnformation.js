import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import {
  Segment, Header, Divider, Table,
} from 'semantic-ui-react';
import styled from 'styled-components';

import EditCompanyInformation from './EditCompanyInformation';

const T = i18n.createComponent();

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  float: right;
  padding: 0 1rem;
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

class ProfileCompanylnformation extends Component {
  state = {
    isEditModalOpen: false,
  };

  handleEditModalToggle() {
    const { isEditModalOpen } = this.state;
    this.setState({ isEditModalOpen: !isEditModalOpen });
  }

  render() {
    const {
      country, state, city, address, postalCode, phoneCode, phoneNumber, companyName,
    } = this.props;
    const { isEditModalOpen } = this.state;

    return (
      <Segment basic>
        <Header as="h4">
          <T>main.profile.company.title</T>
          <AnchorStyledText onClick={() => this.handleEditModalToggle()}>
            <T>main.profile.edit</T>
          </AnchorStyledText>
        </Header>
        <Divider />
        <Segment basic loading={!country || !city}>
          <StyledTable>
            <Table stackable basic="very">
              <Table.Body>
                <Table.Row>
                  <Table.Cell><T>main.profile.company.company</T></Table.Cell>
                  <Table.Cell>{companyName}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.company.country</T>
                  </Table.Cell>
                  <Table.Cell>{country}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.company.city</T>
                  </Table.Cell>
                  <Table.Cell>{city}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.company.address</T>
                  </Table.Cell>
                  <Table.Cell>{address}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.company.state</T>
                  </Table.Cell>
                  <Table.Cell>{state}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.company.postal</T>
                  </Table.Cell>
                  <Table.Cell>{postalCode}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.company.phone</T>
                  </Table.Cell>
                  <Table.Cell>{`(${phoneCode}) ${phoneNumber}`}</Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </StyledTable>
        </Segment>
        <EditCompanyInformation
          onClose={() => this.handleEditModalToggle()}
          isOpen={isEditModalOpen}
        />
      </Segment>
    );
  }
}

ProfileCompanylnformation.propTypes = {
  country: PropTypes.string,
  state: PropTypes.string,
  city: PropTypes.string,
  address: PropTypes.string,
  postalCode: PropTypes.string,
  phoneCode: PropTypes.string,
  phoneNumber: PropTypes.string,
  companyName: PropTypes.string,
};

ProfileCompanylnformation.defaultProps = {
  country: '',
  state: '',
  city: '',
  address: '',
  postalCode: '',
  phoneCode: '',
  phoneNumber: '',
  companyName: '',
};

const mapDataToProps = () => {
  const user = Meteor.user();
  if (user) {
    const {
      country, state, city, address,
      postalCode, phoneCode, phoneNumber,
      companyName,
    } = user.profile;
    return {
      country,
      state,
      city,
      address,
      postalCode,
      phoneCode,
      phoneNumber,
      companyName,
    };
  }
  return {};
};

export default withTracker(mapDataToProps)(ProfileCompanylnformation);
