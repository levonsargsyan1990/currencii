import React, { Component } from 'react';
import PropTypes from 'prop-types';
import i18n from 'meteor/universe:i18n';
import {
  Divider, Table,
} from 'semantic-ui-react';
import styled from 'styled-components';

import EditBankAccount from './EditBankAccount';
import DeleteBankAccount from './DeleteBankAccount';

const T = i18n.createComponent();

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  float: right;
  padding: 0 1rem;
`;

const ErrorStyledText = styled.span`
  color: ${props => props.theme.red};
  cursor: pointer;
  text-decoration: underline;
  float: right;
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

class BankAccount extends Component {
  state = {
    isEditModalOpen: false,
    isDeleteModalOpen: false,
  };

  handleEditModalToggle() {
    const { isEditModalOpen } = this.state;
    this.setState({ isEditModalOpen: !isEditModalOpen, isDeleteModalOpen: false });
  }

  handleDeleteModalToggle() {
    const { isDeleteModalOpen } = this.state;
    this.setState({ isDeleteModalOpen: !isDeleteModalOpen, isEditModalOpen: false });
  }

  render() {
    const { account, isLast } = this.props;
    const {
      _id, bankName, swift, iban, accountNumber,
    } = account;
    const { isEditModalOpen, isDeleteModalOpen } = this.state;

    return (
      <div>
        <StyledTable>
          <Table stackable basic="very">
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <T>main.profile.accounts.name</T>
                </Table.Cell>
                <Table.Cell>{bankName}</Table.Cell>
                <Table.Cell>
                  <ErrorStyledText onClick={() => this.handleDeleteModalToggle()}>
                    <T>main.profile.delete</T>
                  </ErrorStyledText>
                  <AnchorStyledText onClick={() => this.handleEditModalToggle()}>
                    <T>main.profile.edit</T>
                  </AnchorStyledText>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <T>main.profile.accounts.swift</T>
                </Table.Cell>
                <Table.Cell>{swift}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <T>main.profile.accounts.iban</T>
                </Table.Cell>
                <Table.Cell>{iban}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <T>main.profile.accounts.account</T>
                </Table.Cell>
                <Table.Cell>{accountNumber}</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </StyledTable>
        {isLast ? null : <Divider />}
        <EditBankAccount
          account={account}
          onClose={() => this.handleEditModalToggle()}
          isOpen={isEditModalOpen}
        />
        <DeleteBankAccount
          accountId={_id}
          onClose={() => this.handleDeleteModalToggle()}
          isOpen={isDeleteModalOpen}
        />
      </div>
    );
  }
}

BankAccount.propTypes = {
  account: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    bankName: PropTypes.string.isRequired,
    swift: PropTypes.string.isRequired,
    iban: PropTypes.string.isRequired,
    accountNumber: PropTypes.string.isRequired,
  }).isRequired,
  isLast: PropTypes.bool,
};

BankAccount.defaultProps = {
  isLast: false,
};

export default BankAccount;
