import React, { Component } from 'react';
import PropTypes from 'prop-types';
import i18n from 'meteor/universe:i18n';
import {
  Modal, Button, Container, Grid, Message, Icon, Divider,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { deleteBankAccount } from '../../../../api/bankAccounts/methods';

const T = i18n.createComponent();

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

class DeleteBankAccount extends Component {
  state = {
    error: '',
    deleting: false,
  };

  handleDeleteButtonClick() {
    this.setState({ deleting: true });
    const { onClose, accountId } = this.props;
    deleteBankAccount.callPromise({ accountId })
      .then(() => onClose())
      .catch((err) => {
        const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.profile.accounts.deleteModal.errors.deleteError');
        this.setState({
          error: `${i18n.__('main.profile.accounts.deleteModal.errors.deleteFail')} ${message}`,
          deleting: false,
        });
      });
  }

  render() {
    const { isOpen, onClose } = this.props;
    const { error, deleting } = this.state;
    return (
      <Modal
        size="mini"
        dimmer="blurring"
        open={isOpen}
        onClose={onClose}
      >
        <Modal.Content>
          <Container textAlign="center">
            <p>
              <T>main.profile.accounts.deleteModal.text</T>
            </p>
            <Divider hidden />
            <Grid stretched columns="equal">
              <Grid.Column textAlign="right">
                <Button
                  negative
                  onClick={() => this.handleDeleteButtonClick()}
                  loading={deleting}
                >
                  <T>main.profile.accounts.deleteModal.deleteButton</T>
                </Button>
              </Grid.Column>
              <Grid.Column textAlign="left">
                <Button onClick={onClose}>
                  <T>main.profile.accounts.deleteModal.cancelButton</T>
                </Button>
              </Grid.Column>
            </Grid>

          </Container>
        </Modal.Content>
        <StyledError>
          <Message attached="bottom" error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
      </Modal>
    );
  }
}

DeleteBankAccount.propTypes = {
  accountId: PropTypes.string.isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

DeleteBankAccount.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default DeleteBankAccount;
