// TODO handle submitting

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Formik } from 'formik';
import i18n from 'meteor/universe:i18n';
import {
  Modal, Message, Icon, Form, Label, Container, Button, Loader, Dimmer, Segment, Dropdown,
} from 'semantic-ui-react';
import styled from 'styled-components';
import findIndex from 'lodash/findIndex';

import {
  phoneNumberRegex, phoneCodesOptions, countryOptions, amPostalCodeRegex, lvPostalCodeRegex,
} from '../../../../lib/constants';
import { updateCompanyInformation } from '../../../../api/users/methods';

const T = i18n.createComponent();

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledSegment = styled.div`
  .segment {
    padding: 0 !important;
  }
`;

const AlignedField = styled.div`
  .fields {
    align-items: flex-end;
  }
`;

class EditCompanyInformation extends Component {
  state = {
    error: '',
  };

  render() {
    const {
      companyName, country, state, city, address, postalCode,
      phoneCode, phoneNumber, isOpen, onClose,
    } = this.props;
    const { error } = this.state;
    return (
      <Modal
        size="mini"
        closeIcon
        dimmer="blurring"
        open={isOpen}
        onClose={onClose}
        closeOnDimmerClick={false}
      >
        <Modal.Header>
          <T>main.profile.company.editModal.title</T>
        </Modal.Header>
        <Modal.Content>
          <StyledSegment>
            <Segment basic>
              <Dimmer inverted active={!country || !city}>
                <Loader />
              </Dimmer>
              <Formik
                initialValues={{
                  companyName, country, state, city, address, postalCode, phoneCode, phoneNumber,
                }}
                validate={(values) => {
                  let postalCodeRegex;
                  switch (values.country) {
                    case 'Latvia':
                      postalCodeRegex = lvPostalCodeRegex;
                      break;
                    default:
                      postalCodeRegex = amPostalCodeRegex;
                  }
                  let unmaskedValue = values.phoneNumber;
                  if (unmaskedValue) {
                    unmaskedValue = values.phoneNumber.replace(/[- ]/g, '');
                  }
                  const errors = {};
                  if (!values.companyName) {
                    errors.companyName = 'Please enter your company name';
                  }
                  if (!values.country) {
                    errors.country = i18n.__('main.profile.company.editModal.errors.noCountry');
                  } else if (findIndex(countryOptions, { value: values.country }) === -1) {
                    errors.country = i18n.__('main.profile.company.editModal.errors.unsupportedCountry');
                  }
                  if (!values.city) {
                    errors.city = i18n.__('main.profile.company.editModal.errors.noCity');
                  }
                  if (!values.address) {
                    errors.address = i18n.__('main.profile.company.editModal.errors.noAddress');
                  }
                  if (!values.postalCode) {
                    errors.postalCode = i18n.__('main.profile.company.editModal.errors.noPostal');
                  } else if (!postalCodeRegex.test(values.postalCode)) {
                    errors.postalCode = i18n.__('main.profile.company.editModal.errors.invalidPostal');
                  }
                  if (!values.phoneCode) {
                    errors.phoneCode = i18n.__('main.profile.company.editModal.errors.noPhoneCode');
                  } else if (findIndex(phoneCodesOptions, { value: values.phoneCode }) === -1) {
                    errors.phoneCode = i18n.__('main.profile.company.editModal.errors.unsupportedPhoneCode');
                  }
                  if (!values.phoneNumber) {
                    errors.phoneNumber = i18n.__('main.profile.company.editModal.errors.noPhoneNumber');
                  } else if (!phoneNumberRegex.test(values.phoneNumber)) {
                    errors.phoneNumber = i18n.__('main.profile.company.editModal.errors.invalidPhoneNumber');
                  }
                  if (!unmaskedValue) {
                    errors.phoneNumber = 'Please enter phone number';
                  } else if (!phoneNumberRegex.test(unmaskedValue)) {
                    errors.phoneNumber = 'Please enter valid phone number';
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                  const unmaskedValues = { ...values, phoneNumber: values.phoneNumber.replace(/[- ]/g, '') };
                  updateCompanyInformation.callPromise(unmaskedValues)
                    .then(() => onClose())
                    .catch((err) => {
                      const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.profile.company.editModal.errors.editError');
                      setSubmitting(false);
                      this.setState({
                        error: `${i18n.__('main.profile.company.editModal.errors.editFail')} ${message}`,
                      });
                    });
                }}
                render={({
                  handleSubmit, handleChange, handleBlur, values, errors,
                  touched, isValid, isSubmitting, setTouched, setValues,
                }) => {
                  let postalCodePlaceholder;
                  switch (values.country) {
                    case 'Latvia':
                      postalCodePlaceholder = 'e.g. LV-1058';
                      break;
                    default:
                      postalCodePlaceholder = 'e.g. 0069';
                  }
                  return (
                    <div>
                      <Form onSubmit={handleSubmit}>
                        <Form.Field>
                          <label>
                            <T>main.profile.company.editModal.company</T>
                          </label>
                          <input
                            type="text"
                            name="companyName"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.companyName}
                          />
                          {errors.companyName && touched.companyName && (
                            <Label pointing basic color="red">
                              {errors.companyName}
                            </Label>
                          )}
                        </Form.Field>
                        <Form.Field>
                          <label>
                            <T>main.profile.company.editModal.country</T>
                          </label>
                          <Dropdown
                            fluid
                            search
                            selection
                            name="country"
                            options={countryOptions}
                            onChange={(event, data) => {
                              setTouched({ country: true });
                              let phoneCodeValue;
                              switch (data.value) {
                                case 'Latvia':
                                  phoneCodeValue = '+371';
                                  break;
                                case 'Armenia':
                                  phoneCodeValue = '+374';
                                  break;
                                default:
                                  break;
                              }
                              setValues({ phoneCode: phoneCodeValue });
                              handleChange({ ...event, target: { name: 'country', value: data.value } });
                            }}
                            onBlur={handleBlur}
                            value={values.country}
                            onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                          />
                          {errors.country && touched.country && (
                            <Label pointing basic color="red">
                              {errors.country}
                            </Label>
                          )}
                        </Form.Field>
                        <Form.Field>
                          <label>
                            <T>main.profile.company.editModal.city</T>
                          </label>
                          <input
                            type="text"
                            name="city"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.city}
                          />
                          {errors.city && touched.city && (
                            <Label pointing basic color="red">
                              {errors.city}
                            </Label>
                          )}
                        </Form.Field>
                        <Form.Field>
                          <label>
                            <T>main.profile.company.editModal.address</T>
                          </label>
                          <input
                            type="text"
                            name="address"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.address}
                          />
                          {errors.address && touched.address && (
                            <Label pointing basic color="red">
                              {errors.address}
                            </Label>
                          )}
                        </Form.Field>
                        <Form.Group widths="equal">
                          <Form.Field>
                            <label>
                              <T>main.profile.company.editModal.postal</T>
                            </label>
                            <input
                              type="text"
                              name="postalCode"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.postalCode}
                              placeholder={postalCodePlaceholder}
                            />
                            {errors.postalCode && touched.postalCode && (
                              <Label pointing basic color="red">
                                {errors.postalCode}
                              </Label>
                            )}
                          </Form.Field>
                          <Form.Field>
                            <label>
                              <T>main.profile.company.editModal.state</T>
                            </label>
                            <input
                              type="text"
                              name="state"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.state}
                            />
                            {errors.state && touched.state && (
                              <Label pointing basic color="red">
                                {errors.state}
                              </Label>
                            )}
                          </Form.Field>
                        </Form.Group>
                        <AlignedField>
                          <Form.Group>
                            <Form.Field width={5}>
                              <label htmlFor="phoneCode">
                                <T>main.profile.company.editModal.phone</T>
                              </label>
                              <Dropdown
                                fluid
                                search
                                selection
                                id="phoneCode"
                                name="phoneCode"
                                options={phoneCodesOptions}
                                onChange={(event, data) => {
                                  setTouched({ phoneCode: true });
                                  handleChange({ ...event, target: { name: 'phoneCode', value: data.value } });
                                }}
                                onBlur={handleBlur}
                                value={values.phoneCode}
                                onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                              />
                              {errors.phoneCode && touched.phoneCode && (
                                <Label pointing basic color="red">
                                  {errors.phoneCode}
                                </Label>
                              )}
                            </Form.Field>
                            <Form.Field width={11}>
                              {errors.phoneNumber && touched.phoneNumber && (
                                <Label pointing="below" basic color="red">
                                  {errors.phoneNumber}
                                </Label>
                              )}
                              <InputMask
                                mask="99 99-99-99"
                                type="text"
                                name="phoneNumber"
                                placeholder="e.g. 77 55-55-55"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.phoneNumber}
                              />
                            </Form.Field>
                          </Form.Group>
                        </AlignedField>
                        <Container textAlign="center">
                          <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">
                            <T>main.profile.company.editModal.save</T>
                          </Button>
                        </Container>
                      </Form>
                    </div>
                  );
                }}
              />
            </Segment>
          </StyledSegment>
        </Modal.Content>
        <StyledError>
          <Message attached="bottom" error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
      </Modal>
    );
  }
}

EditCompanyInformation.propTypes = {
  country: PropTypes.string,
  state: PropTypes.string,
  city: PropTypes.string,
  address: PropTypes.string,
  postalCode: PropTypes.string,
  phoneCode: PropTypes.string,
  phoneNumber: PropTypes.string,
  companyName: PropTypes.string,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

EditCompanyInformation.defaultProps = {
  country: '',
  state: '',
  city: '',
  address: '',
  postalCode: '',
  phoneCode: '',
  phoneNumber: '',
  companyName: '',
  isOpen: false,
  onClose: () => {},
};

const mapDataToProps = () => {
  const user = Meteor.user();
  if (user) {
    const {
      country, state, city, address,
      postalCode, phoneCode, phoneNumber,
      companyName,
    } = user.profile;
    return {
      country,
      state,
      city,
      address,
      postalCode,
      phoneCode,
      phoneNumber,
      companyName,
    };
  }
  return {};
};

export default withTracker(mapDataToProps)(EditCompanyInformation);
