// TODO handle submitting

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import { Formik } from 'formik';
import i18n from 'meteor/universe:i18n';
import {
  Modal, Message, Icon, Form, Label, Container, Button, Segment,
} from 'semantic-ui-react';
import styled from 'styled-components';

import { ibanRegex, swiftRegex, bankAccountNumberRegex } from '../../../../lib/constants';
import { editBankAccount } from '../../../../api/bankAccounts/methods';

const T = i18n.createComponent();

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledSegment = styled.div`
  .segment {
    padding: 0 !important;
  }
`;

class EditBankAccount extends Component {
  state = {
    error: '',
  };

  render() {
    const {
      account: {
        _id, bankName, swift, iban, accountNumber,
      },
      isOpen,
      onClose,
    } = this.props;
    const { error } = this.state;
    return (
      <Modal
        size="mini"
        closeIcon
        dimmer="blurring"
        open={isOpen}
        onClose={onClose}
        closeOnDimmerClick={false}
      >
        <Modal.Header>
          <T>main.profile.accounts.editModal.title</T>
        </Modal.Header>
        <Modal.Content>
          <StyledSegment>
            <Segment basic>
              <Formik
                initialValues={{
                  bankName, swift, iban, accountNumber,
                }}
                validate={(values) => {
                  let unmaskedAccountNumber = values.accountNumber;
                  if (unmaskedAccountNumber) {
                    unmaskedAccountNumber = values.accountNumber.replace(/[ ]/g, '');
                  }
                  const errors = {};
                  if (!values.bankName) {
                    errors.bankName = i18n.__('main.profile.accounts.errors.noName');
                  }
                  if (!values.swift) {
                    errors.swift = i18n.__('main.profile.accounts.errors.noSwift');
                  } else if (!swiftRegex.test(values.swift)) {
                    errors.swift = i18n.__('main.profile.accounts.errors.invalidSwift');
                  }
                  if (!values.iban) {
                    errors.iban = i18n.__('main.profile.accounts.errors.noIban');
                  } else if (!ibanRegex.test(values.iban)) {
                    errors.iban = i18n.__('main.profile.accounts.errors.invalidIban');
                  }
                  if (!values.accountNumber) {
                    errors.accountNumber = i18n.__('main.profile.accounts.errors.noAccount');
                  } else if (!bankAccountNumberRegex.test(values.accountNumber)) {
                    errors.accountNumber = i18n.__('main.profile.accounts.errors.invalidAccount');
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                  const unmaskedValues = { _id, ...values, accountNumber: values.accountNumber.replace(/[- ]/g, '') };
                  editBankAccount.callPromise(unmaskedValues)
                    .then(() => onClose())
                    .catch((err) => {
                      const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.profile.accounts.editModal.errors.editError');
                      setSubmitting(false);
                      this.setState({
                        error: `${i18n.__('main.profile.accounts.editModal.errors.editFail')} ${message}`,
                      });
                    });
                }}
                render={({
                  handleSubmit, handleChange, handleBlur, values, errors,
                  touched, isValid, isSubmitting,
                }) => (
                  <div>
                    <Form onSubmit={handleSubmit}>
                      <Form.Field>
                        <label htmlFor="bankName">
                          <T>main.profile.accounts.name</T>
                        </label>
                        <input
                          type="text"
                          name="bankName"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.bankName}
                        />
                        {errors.bankName && touched.bankName && (
                          <Label pointing basic color="red">
                            {errors.bankName}
                          </Label>
                        )}
                      </Form.Field>
                      <Form.Field>
                        <label htmlFor="swift">
                          <T>main.profile.accounts.swift</T>
                        </label>
                        <input
                          type="text"
                          name="swift"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.swift}
                        />
                        {errors.swift && touched.swift && (
                          <Label pointing basic color="red">
                            {errors.swift}
                          </Label>
                        )}
                      </Form.Field>
                      <Form.Field>
                        <label htmlFor="iban">
                          <T>main.profile.accounts.iban</T>
                        </label>
                        <input
                          type="text"
                          name="iban"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.iban}
                        />
                        {errors.iban && touched.iban && (
                          <Label pointing basic color="red">
                            {errors.iban}
                          </Label>
                        )}
                      </Form.Field>
                      <Form.Field>
                        <label htmlFor="accountNumber">
                          <T>main.profile.accounts.account</T>
                        </label>
                        <InputMask
                          mask="9999  9999  9999  9999"
                          maskChar=""
                          type="text"
                          name="accountNumber"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.accountNumber}
                        />
                        {errors.accountNumber && touched.accountNumber && (
                          <Label pointing basic color="red">
                            {errors.accountNumber}
                          </Label>
                        )}
                      </Form.Field>
                      <Container textAlign="center">
                        <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">
                          <T>main.profile.accounts.addModal.button</T>
                        </Button>
                      </Container>
                    </Form>
                  </div>
                )}
              />
            </Segment>
          </StyledSegment>
        </Modal.Content>
        <StyledError>
          <Message attached="bottom" error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
      </Modal>
    );
  }
}

EditBankAccount.propTypes = {
  account: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    bankName: PropTypes.string.isRequired,
    swift: PropTypes.string.isRequired,
    iban: PropTypes.string.isRequired,
    accountNumber: PropTypes.string.isRequired,
  }).isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

EditBankAccount.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default EditBankAccount;
