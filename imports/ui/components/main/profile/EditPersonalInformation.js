// TODO handle submitting

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Formik } from 'formik';
import i18n from 'meteor/universe:i18n';
import {
  Modal, Message, Icon, Form, Label, Container, Button, Loader, Dimmer, Segment,
} from 'semantic-ui-react';
import styled from 'styled-components';

import { nameRegex } from '../../../../lib/constants';
import { updatePersonalInformation } from '../../../../api/users/methods';

const T = i18n.createComponent();

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledSegment = styled.div`
  .segment {
    padding: 0 !important;
  }
`;

class EditPersonalInformation extends Component {
  state = {
    error: '',
  };

  render() {
    const {
      firstName, lastName, position, isOpen, onClose,
    } = this.props;
    const { error } = this.state;
    return (
      <Modal
        size="mini"
        closeIcon
        dimmer="blurring"
        open={isOpen}
        onClose={onClose}
        closeOnDimmerClick={false}
      >
        <Modal.Header>
          <T>main.profile.personal.editModal.title</T>
        </Modal.Header>
        <Modal.Content>
          <StyledSegment>
            <Segment basic>
              <Dimmer inverted active={!firstName || !lastName}>
                <Loader />
              </Dimmer>
              <Formik
                initialValues={{ firstName, lastName, position }}
                validate={(values) => {
                  const errors = {};
                  if (!values.firstName) {
                    errors.firstName = i18n.__('main.profile.personal.editModal.errors.noFirstName');
                  } else if (!nameRegex.test(values.firstName)) {
                    errors.firstName = i18n.__('main.profile.personal.editModal.errors.invalidFirstName');
                  }
                  if (!values.lastName) {
                    errors.firstName = i18n.__('main.profile.personal.editModal.errors.noLastName');
                  } else if (!nameRegex.test(values.lastName)) {
                    errors.lastName = i18n.__('main.profile.personal.editModal.errors.invalidLastName');
                  }
                  if (!values.position) {
                    errors.position = i18n.__('main.profile.personal.editModal.errors.noPosition');
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                  updatePersonalInformation.callPromise(values)
                    .then(() => onClose())
                    .catch((err) => {
                      const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.profile.personal.editModal.errors.editError');
                      setSubmitting(false);
                      this.setState({
                        error: `${i18n.__('main.profile.personal.editModal.errors.editFail')} ${message}`,
                      });
                    });
                }}
                render={({
                  handleSubmit, handleChange, handleBlur, values,
                  errors, touched, isValid, isSubmitting,
                }) => (
                  <div>
                    <Form onSubmit={handleSubmit}>
                      <Form.Field>
                        <label>
                          <T>main.profile.personal.editModal.firstName</T>
                        </label>
                        <input
                          type="text"
                          name="firstName"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.firstName}
                        />
                        {errors.firstName && touched.firstName && (
                          <Label pointing basic color="red">
                            {errors.firstName}
                          </Label>
                        )}
                      </Form.Field>
                      <Form.Field>
                        <label>
                          <T>main.profile.personal.editModal.lastName</T>
                        </label>
                        <input
                          type="text"
                          name="lastName"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.lastName}
                        />
                        {errors.lastName && touched.lastName && (
                          <Label pointing basic color="red">
                            {errors.lastName}
                          </Label>
                        )}
                      </Form.Field>
                      <Form.Field>
                        <label htmlFor="position">
                          <T>main.profile.personal.editModal.position</T>
                        </label>
                        <input
                          type="text"
                          id="position"
                          name="position"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.position}
                        />
                        {errors.position && touched.position && (
                          <Label pointing basic color="red">
                            {errors.position}
                          </Label>
                        )}
                      </Form.Field>
                      <Container textAlign="center">
                        <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">
                          <T>main.profile.personal.editModal.button</T>
                        </Button>
                      </Container>
                    </Form>
                  </div>
                )}
              />
            </Segment>
          </StyledSegment>
        </Modal.Content>
        <StyledError>
          <Message attached="bottom" error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
      </Modal>
    );
  }
}

EditPersonalInformation.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  position: PropTypes.string,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

EditPersonalInformation.defaultProps = {
  firstName: '',
  lastName: '',
  position: '',
  isOpen: false,
  onClose: () => {},
};

const mapDataToProps = () => {
  const user = Meteor.user();
  if (user) {
    const { firstName, lastName, position } = user.profile;
    return { firstName, lastName, position };
  }
  return {};
};

export default withTracker(mapDataToProps)(EditPersonalInformation);
