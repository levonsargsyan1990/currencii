import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import {
  Segment, Header, Divider, Table,
} from 'semantic-ui-react';
import styled from 'styled-components';

import EditPersonalInformation from './EditPersonalInformation';

const T = i18n.createComponent();

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  float: right;
  padding: 0 1rem;
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

class ProfilePersonalInformation extends Component {
  state = {
    isEditModalOpen: false,
  };

  handleEditModalToggle() {
    const { isEditModalOpen } = this.state;
    this.setState({ isEditModalOpen: !isEditModalOpen });
  }

  render() {
    const {
      firstName, lastName, position,
    } = this.props;
    const { isEditModalOpen } = this.state;

    return (
      <Segment basic>
        <Header as="h4">
          <T>main.profile.personal.title</T>
          <AnchorStyledText onClick={() => this.handleEditModalToggle()}>
            <T>main.profile.edit</T>
          </AnchorStyledText>
        </Header>
        <Divider />
        <Segment basic loading={!firstName || !lastName}>
          <StyledTable>
            <Table stackable basic="very">
              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.personal.firstName</T>
                  </Table.Cell>
                  <Table.Cell>{firstName}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.personal.lastName</T>
                  </Table.Cell>
                  <Table.Cell>{lastName}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <T>main.profile.personal.position</T>
                  </Table.Cell>
                  <Table.Cell>{position}</Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </StyledTable>
        </Segment>
        <EditPersonalInformation
          onClose={() => this.handleEditModalToggle()}
          isOpen={isEditModalOpen}
        />
      </Segment>
    );
  }
}

ProfilePersonalInformation.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  position: PropTypes.string,
};

ProfilePersonalInformation.defaultProps = {
  firstName: '',
  lastName: '',
  position: '',
};

const mapDataToProps = () => {
  const user = Meteor.user();
  if (user) {
    const { firstName, lastName, position } = user.profile;
    return { firstName, lastName, position };
  }
  return {};
};

export default withTracker(mapDataToProps)(ProfilePersonalInformation);
