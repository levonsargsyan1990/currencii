// TODO Add loader
// TODO Remove all sample data from defaultProps

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import {
  Segment, Header, Divider,
} from 'semantic-ui-react';
import styled from 'styled-components';

import BankAccount from './BankAccount';
import AddBankAccount from './AddBankAccount';
import { BankAccounts as Accounts } from '../../../../api/bankAccounts/bankAccounts';

const T = i18n.createComponent();

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  float: right;
  padding: 0 1rem;
`;

const StyledText = styled.p`
  text-align: center;
`;

class BankAccounts extends Component {
  state = {
    isAddModalOpen: false,
  };

  handleAddModalToggle() {
    const { isAddModalOpen } = this.state;
    this.setState({ isAddModalOpen: !isAddModalOpen });
  }

  render() {
    const { accounts, loadingAccounts } = this.props;
    const { isAddModalOpen } = this.state;
    let content = null;
    if (loadingAccounts) {
      content = <Segment basic loading />;
    } else if (accounts.length === 0) {
      content = (
        <StyledText>
          <T>main.profile.accounts.noAccounts</T>
        </StyledText>
      );
    } else {
      content = accounts.map((account, index) => (
        <BankAccount
          key={account._id}
          isLast={index + 1 === accounts.length}
          account={account}
        />
      ));
    }
    return (
      <Segment basic>
        <Header as="h4">
          <T>main.profile.accounts.title</T>
          <AnchorStyledText onClick={() => this.handleAddModalToggle()}>
            <T>main.profile.add</T>
          </AnchorStyledText>
        </Header>
        <Divider />
        <Segment basic loading={!accounts}>
          {content}
        </Segment>
        <AddBankAccount
          onClose={() => this.handleAddModalToggle()}
          isOpen={isAddModalOpen}
        />
      </Segment>
    );
  }
}

BankAccounts.propTypes = {
  loadingAccounts: PropTypes.bool,
  accounts: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    bankName: PropTypes.string.isRequired,
    swift: PropTypes.string.isRequired,
    iban: PropTypes.string.isRequired,
    accountNumber: PropTypes.string.isRequired,
  })),
};

BankAccounts.defaultProps = {
  loadingAccounts: false,
  accounts: [],
};

const mapDataToProps = () => {
  const userId = Meteor.userId();
  if (userId) {
    const accountHandle = Meteor.subscribe('bankAccounts.listByUser', { userId });
    const loadingAccounts = !accountHandle.ready();
    const accounts = Accounts.find().fetch();
    return {
      loadingAccounts,
      accounts,
    };
  }
  return {};
};

export default withTracker(mapDataToProps)(BankAccounts);
