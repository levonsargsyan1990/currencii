// TODO: Remove error from redux store is password check
//       and/or send email succeed after initial failure

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import i18n from 'meteor/universe:i18n';
import {
  Container, Label, Button, Form,
} from 'semantic-ui-react';
import { Formik } from 'formik';

import { passwordRegex } from '../../../../lib/constants';
import { setEmailSent as setEmailSentAction, setError as setErrorAction } from '../../../actions/emailChange';
import { checkPassword, addEmail } from '../../../../api/users/methods';

const T = i18n.createComponent();

const EmailChangeStep2 = ({ email, setEmailSent, setError }) => (
  <Formik
    initialValues={{ password: '' }}
    validate={(values) => {
      const errors = {};
      if (!values.password) {
        errors.password = i18n.__('main.settings.emailChange2.errors.noPassword');
      } else if (!passwordRegex.test(values.password)) {
        errors.password = i18n.__('main.settings.emailChange2.errors.invalidPassword');
      }
      return errors;
    }}
    onSubmit={({ password }, { setSubmitting }) => {
      checkPassword.callPromise({ password })
        .then(() => {
          setError();
          return addEmail.callPromise({ email });
        })
        .then(() => {
          setError();
          setEmailSent();
        })
        .catch((err) => {
          setError(err.reason);
          setSubmitting(false);
        });
    }}
    render={({
      handleSubmit, handleChange, handleBlur, values, errors, touched, isValid, isSubmitting,
    }) => (
      <div>
        <Form onSubmit={handleSubmit}>
          <Form.Field>
            <p>
              Please, enter your password.
              You&apos;ll also need to verify your new email address for the change to take place
            </p>
            <label>
              <T>main.settings.emailChange2.password</T>
            </label>
            <input
              type="password"
              name="password"
              placeholder={i18n.__('main.settings.emailChange2.passwordPlaceholder')}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
            />
            {errors.password && touched.password && (
              <Label pointing basic color="red">
                {errors.password}
              </Label>
            )}
          </Form.Field>
          <Container textAlign="center">
            <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">
              <T>main.settings.emailChange2.button</T>
            </Button>
          </Container>
        </Form>
      </div>
    )}
  />
);

EmailChangeStep2.propTypes = {
  email: PropTypes.string.isRequired,
  setEmailSent: PropTypes.func.isRequired,
  setError: PropTypes.func.isRequired,
};

const mapStateToProps = ({ emailChange: { email } }) => ({ email });

export default connect(mapStateToProps, {
  setEmailSent: setEmailSentAction,
  setError: setErrorAction,
})(EmailChangeStep2);
