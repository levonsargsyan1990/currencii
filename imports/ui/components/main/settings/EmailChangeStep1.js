import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { connect } from 'react-redux';
import i18n from 'meteor/universe:i18n';
import {
  Container, Label, Button, Form, Dimmer, Loader,
} from 'semantic-ui-react';
import { Formik } from 'formik';

import { setEmail as setEmailAction } from '../../../actions/emailChange';
import { emailRegex } from '../../../../lib/constants';

const T = i18n.createComponent();

const EmailChangeStep1 = ({ setEmail, email }) => {
  if (!email) {
    return (
      <Dimmer inverted active={!email}>
        <Loader />
      </Dimmer>
    );
  }
  return (
    <Formik
      initialValues={{ email: '' }}
      validate={(values) => {
        const errors = {};
        if (!values.email) {
          errors.email = i18n.__('main.settings.emailChange1.errors.noEmail');
        } else if (!emailRegex.test(values.email)) {
          errors.email = i18n.__('main.settings.emailChange1.errors.invalidEmail');
        } else if (values.email === email) {
          errors.email = i18n.__('main.settings.emailChange1.errors.sameEmail');
          errors.emailSame = true;
        }
        return errors;
      }}
      onSubmit={(values) => {
        setEmail(values.email);
      }}
      render={({
        handleSubmit, handleChange, handleBlur, values,
        errors, touched, isValid, isSubmitting,
      }) => (
        <Form autoComplete="off" onSubmit={handleSubmit}>
          <p>
            This opereation will require you to enter your password to authorise your email change
          </p>
          <Form.Field>
            <input
              name="email"
              type="text"
              placeholder={i18n.__('main.settings.emailChange1.email')}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
            {((errors.email && touched.email) || errors.emailSame) && (
              <Label pointing basic color="red">
                {errors.email}
              </Label>
            )}
          </Form.Field>
          <Container textAlign="center">
            <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">
              <T>main.settings.emailChange1.button</T>
            </Button>
          </Container>
        </Form>
      )}
    />
  );
};

EmailChangeStep1.propTypes = {
  email: PropTypes.string.isRequired,
  setEmail: PropTypes.func.isRequired,
};

EmailChangeStep1.deafultProps = {
  email: '',
};

const mapDataToProps = () => {
  const user = Meteor.user();
  if (user) {
    return { email: user.emails[0].address };
  }
  return {};
};

const meteorWrappedComponent = withTracker(mapDataToProps)(EmailChangeStep1);

export default connect(null, {
  setEmail: setEmailAction,
})(meteorWrappedComponent);
