import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import i18n from 'meteor/universe:i18n';
import { Modal, Message, Icon } from 'semantic-ui-react';
import styled from 'styled-components';

import EmaiChangeStep1 from './EmailChangeStep1';
import EmaiChangeStep2 from './EmailChangeStep2';
import EmaiChangeStep3 from './EmailChangeStep3';

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const EmailChange = ({
  isOpen, email, emailSent, error, onClose,
}) => {
  let title = i18n.__('main.settings.emailChange1.title');
  let content = <EmaiChangeStep1 />;
  if (email) {
    title = i18n.__('main.settings.emailChange2.title');
    content = <EmaiChangeStep2 />;
  }
  if (emailSent) {
    title = i18n.__('main.settings.emailChange3.title');
    content = <EmaiChangeStep3 onClose={onClose} />;
  }
  return (
    <Modal
      size="mini"
      closeIcon
      dimmer="blurring"
      open={isOpen}
      onClose={onClose}
      closeOnDimmerClick={false}
    >
      <Modal.Header>{title}</Modal.Header>
      <Modal.Content>
        {content}
      </Modal.Content>
      <StyledError>
        <Message attached="bottom" error hidden={!error}>
          <Icon name="warning" />
          {error}
        </Message>
      </StyledError>
    </Modal>
  );
};

EmailChange.propTypes = {
  isOpen: PropTypes.bool,
  email: PropTypes.string.isRequired,
  emailSent: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
};

EmailChange.defaultProps = {
  isOpen: false,
};

const mapStateToProps = ({
  emailChange: {
    email, emailSent, error,
  },
}) => ({ email, emailSent, error });

export default connect(mapStateToProps)(EmailChange);
