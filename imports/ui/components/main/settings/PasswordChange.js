import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Accounts } from 'meteor/accounts-base';
import i18n from 'meteor/universe:i18n';
import {
  Form, Modal, Message, Icon, Container, Button, Label,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { Formik } from 'formik';

import { passwordRegex } from '../../../../lib/constants';

import { ExposablePassword } from '../../common';
import { sendPasswordChangeEmail } from '../../../../api/email/methods';

const T = i18n.createComponent();

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const resetPassword = ({ oldPassword, newPassword }) => new Promise((resolve, reject) => {
  Accounts.changePassword(oldPassword, newPassword, (err, res) => {
    if (err) {
      reject(err);
    } else {
      resolve(res);
    }
  });
});

class PasswordChange extends Component {
  state = {
    error: '',
  };

  render() {
    const { error } = this.state;
    const { isOpen, onClose } = this.props;
    return (
      <Modal
        size="mini"
        closeIcon
        dimmer="blurring"
        open={isOpen}
        onClose={onClose}
        closeOnDimmerClick={false}
      >
        <Modal.Header>
          <T>main.settings.passwordChange.title</T>
        </Modal.Header>
        <Modal.Content>
          <Formik
            initialValues={{ oldPassword: '', password: '', repassword: '' }}
            validate={(values) => {
              const errors = {};
              if (!values.oldPassword) {
                errors.oldPassword = i18n.__('main.settings.passwordChange.errors.noCurrentPassword');
              } else if (!passwordRegex.test(values.oldPassword)) {
                errors.oldPassword = i18n.__('main.settings.passwordChange.errors.invalidPassword');
              }
              if (!values.password) {
                errors.password = i18n.__('main.settings.passwordChange.errors.noPassword');
              } else if (!passwordRegex.test(values.password)) {
                errors.password = i18n.__('main.settings.passwordChange.errors.invalidPassword');
              } else if (values.oldPassword === values.password) {
                errors.password = i18n.__('main.settings.passwordChange.errors.samePassword');
              }
              if (values.password !== values.repassword) {
                errors.repassword = i18n.__('main.settings.passwordChange.errors.repassword');
              }
              return errors;
            }}
            isInitialValid={({ validate, initialValues }) => {
              const errors = validate(initialValues);
              if (Object.keys(errors).length > 0) {
                return false;
              }
              return true;
            }}
            onSubmit={({ oldPassword, password: newPassword }, { setSubmitting }) => {
              this.setState({ error: '' });
              resetPassword({ oldPassword, newPassword })
                .then(() => sendPasswordChangeEmail.callPromise())
                .then(() => onClose())
                .catch((err) => {
                  const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.settings.passwordChange.errors.changeError');
                  setSubmitting(false);
                  this.setState({
                    error: `${i18n.__('main.settings.passwordChange.errors.changeFail')}: ${message}`,
                  });
                });
            }}
            render={({
              handleSubmit, handleChange, handleBlur, values,
              errors, touched, isValid, isSubmitting,
            }) => (
              <Form onSubmit={handleSubmit}>
                <Form.Field>
                  <ExposablePassword
                    name="oldPassword"
                    placeholder="Enter your current password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.oldPassword}
                  />
                  {errors.oldPassword && touched.oldPassword && (
                    <Label pointing basic color="red">
                      {errors.oldPassword}
                    </Label>
                  )}
                </Form.Field>
                <Form.Field>
                  <ExposablePassword
                    name="password"
                    placeholder="Enter your new password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                  />
                  {errors.password && touched.password && (
                    <Label pointing basic color="red">
                      {errors.password}
                    </Label>
                  )}
                </Form.Field>
                <Form.Field>
                  <ExposablePassword
                    name="repassword"
                    placeholder="Repeat your new password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.repassword}
                  />
                  {errors.repassword && touched.repassword && (
                    <Label pointing basic color="red">
                      {errors.repassword}
                    </Label>
                  )}
                </Form.Field>
                <Container textAlign="center">
                  <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">
                    <T>main.settings.passwordChange.button</T>
                  </Button>
                </Container>
              </Form>
            )}
          />
        </Modal.Content>
        <StyledError>
          <Message attached="bottom" error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
      </Modal>
    );
  }
}

PasswordChange.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
};

PasswordChange.defaultProps = {
  isOpen: false,
};

export default PasswordChange;
