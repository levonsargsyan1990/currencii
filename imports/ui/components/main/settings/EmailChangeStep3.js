import React from 'react';
import PropTypes from 'prop-types';
import i18n from 'meteor/universe:i18n';
import {
  Container, Button,
} from 'semantic-ui-react';

const T = i18n.createComponent();

const EmailChangeStep3 = ({ onClose }) => (
  <div>
    <p>
      <T>main.settings.emailChange3.text</T>
    </p>
    <Container textAlign="center">
      <Button onClick={onClose} type="submit" color="green"><T>main.settings.emailChange3.button</T></Button>
    </Container>
  </div>
);

EmailChangeStep3.propTypes = {
  onClose: PropTypes.func.isRequired,
};

export default EmailChangeStep3;
