// TODO: set isTouched to true if the form is filled from redux store

import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import i18n from 'meteor/universe:i18n';
import {
  Header, Divider, Form, Label, Button, Container,
} from 'semantic-ui-react';

import { userWithEmailExists } from '../../../api/users/methods';
import { emailRegex, passwordRegex } from '../../../lib/constants';
import { changeStep as changeStepAction, submitSection1 as submitSection1Action } from '../../actions/signup';

const T = i18n.createComponent();

const SignupStep1 = (props) => {
  const {
    email, password, changeStep, submitSection1,
  } = props;
  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      initialValues={{ email, password, repassword: password }}
      validate={values => userWithEmailExists.callPromise({ email: values.email })
        .then((exists) => {
          const errors = {};
          if (!values.email) {
            errors.email = i18n.__('main.onboarding.signup1.errors.noEmail');
          } else if (!emailRegex.test(values.email)) {
            errors.email = i18n.__('main.onboarding.signup1.errors.invalidEmail');
          } else if (exists) {
            errors.email = 'User with this email already exists';
          }
          if (!values.password) {
            errors.password = i18n.__('main.onboarding.signup1.errors.noPassword');
          } else if (!passwordRegex.test(values.password)) {
            errors.password = i18n.__('main.onboarding.signup1.errors.invalidPassword');
          }
          if (values.password !== values.repassword) {
            errors.repassword = i18n.__('main.onboarding.signup1.errors.repassword');
          }
          if (Object.keys(errors).length) {
            throw errors;
          }
        })
      }
      isInitialValid={({ validate, initialValues }) => {
        if (!initialValues.email) {
          return false;
        }
        const errors = validate(initialValues);
        if (Object.keys(errors).length > 0) {
          return false;
        }
        return true;
      }}
      onSubmit={(values) => {
        submitSection1({ email: values.email, password: values.password });
        changeStep(2);
      }}
      render={({
        handleSubmit, handleChange, handleBlur, values, errors, touched, isSubmitting,
      }) => (
        <Form onSubmit={handleSubmit}>
          <Header as="h3">
            <T>main.onboarding.signup1.title</T>
          </Header>
          <Divider hidden />
          <Form.Field>
            <label htmlFor="email">
              <T>main.onboarding.signup1.email</T>
            </label>
            <input
              type="text"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
            {errors.email && touched.email && (
              <Label pointing basic color="red">
                {errors.email}
              </Label>
            )}
          </Form.Field>
          <Form.Field>
            <label>
              <T>main.onboarding.signup1.password</T>
            </label>
            <input
              type="password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
            />
            {errors.password && touched.password && (
              <Label pointing basic color="red">
                {errors.password}
              </Label>
            )}
          </Form.Field>
          <Form.Field>
            <label>
              <T>main.onboarding.signup1.repassword</T>
            </label>
            <input
              type="password"
              name="repassword"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.repassword}
            />
            {errors.repassword && touched.repassword && (
              <Label pointing basic color="red">
                {errors.repassword}
              </Label>
            )}
          </Form.Field>
          <Container textAlign="center">
            <Button loading={isSubmitting} type="submit" color="green"><T>main.onboarding.signup1.button</T></Button>
          </Container>
        </Form>
      )}
    />

  );
};

SignupStep1.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  changeStep: PropTypes.func.isRequired,
  submitSection1: PropTypes.func.isRequired,
};

const matStateToProps = ({ signup: { email, password } }) => ({ email, password });

export default connect(matStateToProps, {
  changeStep: changeStepAction, submitSection1: submitSection1Action,
})(SignupStep1);
