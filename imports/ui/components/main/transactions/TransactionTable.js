import React from 'react';
import PropTypes from 'prop-types';
import { Table, Loader } from 'semantic-ui-react';
import i18n from 'meteor/universe:i18n';

import PendingExchangeRow from './PendingExchangeRow';

const T = i18n.createComponent();

const TransactionTable = ({ transactions, loading }) => {
  if (loading) {
    return <Loader size="large" />;
  }
  const transactionRows = transactions.map(transaction => (
    <PendingExchangeRow key={transaction._id} transaction={transaction} />
  ));
  if (transactionRows.length === 0) {
    return <p>No transactions</p>;
  }
  return (
    <Table basic="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell><T>main.transsactions.table.ref</T></Table.HeaderCell>
          <Table.HeaderCell><T>main.transsactions.table.sell</T></Table.HeaderCell>
          <Table.HeaderCell><T>main.transsactions.table.buy</T></Table.HeaderCell>
          <Table.HeaderCell><T>main.transsactions.table.executed</T></Table.HeaderCell>
          <Table.HeaderCell><T>main.transsactions.table.saved</T></Table.HeaderCell>
          <Table.HeaderCell />
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {transactionRows}
      </Table.Body>
    </Table>
  );
};

TransactionTable.propTypes = {
  loading: PropTypes.bool,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  })),
};

TransactionTable.defaultProps = {
  loading: true,
  transactions: [],
};

export default TransactionTable;
