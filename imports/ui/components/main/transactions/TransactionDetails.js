// TODO handle submitting

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import {
  Modal, Message, Icon, Table, Container, Divider,
} from 'semantic-ui-react';
import styled from 'styled-components';
import i18n from 'meteor/universe:i18n';

import { numberWithCommas } from '../../../../lib/helpers';

const { title } = Meteor.settings.public;

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  padding: 0 1rem;
`;

const T = i18n.createComponent();

class TransactionDetails extends Component {
  state = {
    error: '',
  };

  constructor(props) {
    super(props);
    this.tableRef = React.createRef();
  }

  handlePDFDownload() {
    try {
      const { transaction: { _id } } = this.props;
      const table = this.tableRef.current;
      // eslint-disable-next-line new-cap
      const pdf = new jsPDF('p', 'pt');
      const res = pdf.autoTableHtmlToJson(table.childNodes[0]);
      pdf.autoTable(res.columns, res.data, { theme: 'plain' });
      pdf.save(`${title}_transaction(${_id}).pdf`);
    } catch (err) {
      this.setState({ error: i18n.__('main.transactions.errors.handlePDFerror') });
    }
  }

  render() {
    const {
      transaction, isOpen, onClose,
    } = this.props;
    const {
      reference, sell, buy, marketRate,
      guaranteedRate, bankAccount: { accountNumber },
    } = transaction;
    const { error } = this.state;
    const savedAmount = sell.amount * (guaranteedRate - marketRate);

    return (
      <Modal
        size="tiny"
        closeIcon
        dimmer="blurring"
        open={isOpen}
        onClose={onClose}
        closeOnDimmerClick={false}
      >
        <Modal.Header><T>main.transactions.modalHeader</T></Modal.Header>
        <Modal.Content>
          <StyledTable ref={this.tableRef}>
            <Table stackable basic="very">
              <Table.Body>
                <Table.Row>
                  <Table.Cell><T>main.transactions.sell</T></Table.Cell>
                  <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(sell.amount.toFixed(2))}`}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell><T>main.transactions.buy</T></Table.Cell>
                  <Table.Cell>{`${buy.currency.toUpperCase()} ${numberWithCommas(buy.amount.toFixed(2))}`}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell><T>main.transactions.midMarketRate</T></Table.Cell>
                  <Table.Cell>{marketRate}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell><T>main.transactions.guaranteedRate</T></Table.Cell>
                  <Table.Cell>{guaranteedRate}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell><T>main.transactions.saveUp</T></Table.Cell>
                  <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(savedAmount.toFixed(2))}`}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell><T>main.transactions.transferedTo</T></Table.Cell>
                  <Table.Cell>{accountNumber}</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell><T>main.transactions.refNumber</T></Table.Cell>
                  <Table.Cell>{`${reference}`}</Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </StyledTable>
          <Divider hidden />
          <Container textAlign="center">
            <AnchorStyledText onClick={() => this.handlePDFDownload()}>
              <T>main.transactions.downloadPDF</T>
            </AnchorStyledText>
          </Container>
        </Modal.Content>
        <StyledError>
          <Message attached="bottom" error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
      </Modal>
    );
  }
}

TransactionDetails.propTypes = {
  transaction: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  }).isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

TransactionDetails.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default TransactionDetails;
