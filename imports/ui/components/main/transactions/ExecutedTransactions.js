import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Container, Header, Segment,
} from 'semantic-ui-react';
import i18n from 'meteor/universe:i18n';

import TransactionTable from './TransactionTable';
import { Transactions } from '../../../../api/transactions/transactions';

const T = i18n.createComponent();

const StyledSegment = styled.div`
  &>.segment {
    max-height: 250px;
    overflow: auto;
  }
`;

const ResponsiveTable = styled.div`
  @media only screen and (max-width: ${props => props.theme.breakpoints.tablet}) {
    .table {
      thead {
        display: none !important;
      }
      tr {
        border-bottom: none !important;
        &:last-child {
          box-shadow: none !important;
        }
      }
      td {
        &:before {
          display: inline-block;
          font-weight:700;
          width: 100px;
        }
        &:first-child {
          font-weight: normal !important;
        }
        &:nth-of-type(1):before { content: "Reference #"; }
        &:nth-of-type(2):before { content: "Sell"; }
        &:nth-of-type(3):before { content: "Buy"; }
        &:nth-of-type(4):before { content: "Executed"; }
        &:nth-of-type(5):before { content: "Saved"; }
      }
      span {
        float: none;
        padding: 0;
      }
    }
  }
`;

const ExecutedTransactions = ({ transactions, transactionsLoading }) => (
  <Container>
    <Header as="h3"><T>main.transactions.headerExecuted</T></Header>
    <StyledSegment>
      <Segment>
        <ResponsiveTable>
          <TransactionTable
            transactions={transactions}
            loading={transactionsLoading}
          />
        </ResponsiveTable>
      </Segment>
    </StyledSegment>
  </Container>
);

ExecutedTransactions.propTypes = {
  transactionsLoading: PropTypes.bool,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  })),
};

ExecutedTransactions.defaultProps = {
  transactions: [],
  transactionsLoading: true,
};

const mapDataToProps = () => {
  const userId = Meteor.userId();
  if (userId) {
    const transactionHandle = Meteor.subscribe('transactions.executedByUser', { userId });
    const transactionsLoading = !transactionHandle.ready();
    const transactions = Transactions.find({
      status: { $in: ['completed', 'sent'] },
    }).fetch();
    return { transactionsLoading, transactions };
  }
  return {};
};

export default withTracker(mapDataToProps)(ExecutedTransactions);
