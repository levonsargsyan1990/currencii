import React, { Component } from 'react';
import PropTypes from 'prop-types';
import qs from 'query-string';
import moment from 'moment';
import { Table } from 'semantic-ui-react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';

import { numberWithCommas } from '../../../../lib/helpers';
import TransactionDetails from './TransactionDetails';


const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  float: right;
  padding: 0 1rem;
`;

class PendingExchangeRow extends Component {
  state = {
    isDetailModalOpen: false,
  };

  handleDetailModalToggle() {
    const { isDetailModalOpen } = this.state;
    this.setState({ isDetailModalOpen: !isDetailModalOpen });
  }

  render() {
    const { transaction, location } = this.props;
    const { isDetailModalOpen } = this.state;
    const {
      _id, reference, sell, buy, marketRate, guaranteedRate, receivedAt,
    } = transaction;
    const savedAmount = sell.amount * (guaranteedRate - marketRate);
    const { highlight } = qs.parse(location.search);
    return (
      <Table.Row key={_id} positive={highlight === transaction._id}>
        <Table.Cell>
          {`${reference}`}
        </Table.Cell>
        <Table.Cell>
          {`${sell.currency.toUpperCase()} ${numberWithCommas(sell.amount.toFixed(2))}`}
        </Table.Cell>
        <Table.Cell>
          {`${buy.currency.toUpperCase()} ${numberWithCommas(buy.amount.toFixed(2))}`}
        </Table.Cell>
        <Table.Cell>{receivedAt ? moment(receivedAt).format('DD/MM/YYYY') : ''}</Table.Cell>
        <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(savedAmount.toFixed(2))}`}</Table.Cell>
        <Table.Cell>
          <AnchorStyledText onClick={() => this.handleDetailModalToggle()}>
            Details
          </AnchorStyledText>
        </Table.Cell>
        <TransactionDetails
          transaction={transaction}
          onClose={() => this.handleDetailModalToggle()}
          isOpen={isDetailModalOpen}
        />
      </Table.Row>
    );
  }
}

PendingExchangeRow.propTypes = {
  transaction: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  }).isRequired,
  location: ReactRouterPropTypes.location.isRequired,
};

export default withRouter(PendingExchangeRow);
