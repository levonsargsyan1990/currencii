// TODO: set isTouched to true if the form is filled from redux store

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import { Formik } from 'formik';
import { Meteor } from 'meteor/meteor';
import { connect } from 'react-redux';
import i18n from 'meteor/universe:i18n';
import {
  Header, Divider, Form, Label, Button,
  Container, Dropdown, Message, Icon,
} from 'semantic-ui-react';
import findIndex from 'lodash/findIndex';
import omit from 'lodash/omit';
import styled from 'styled-components';

import { nameRegex, phoneNumberRegex, phoneCodesOptions } from '../../../lib/constants';
import { submitSection3 as submitSection3Action } from '../../actions/signup';
import { addUser } from '../../../api/users/methods';

const T = i18n.createComponent();

const AlignedField = styled.div`
  .fields {
    align-items: flex-end;
  }
`;

const StyledError = styled.div`
  margin-top: 15px;
`;

class SignupStep3 extends Component {
  state = {
    error: '',
  };

  render() {
    const {
      signup, firstName, country, lastName, position, phoneCode, phoneNumber, submitSection3,
    } = this.props;
    const { error } = this.state;
    let initialPhoneCode = '';
    switch (country) {
      case 'Latvia':
        initialPhoneCode = '+371';
        break;
      default:
        initialPhoneCode = '+374';
    }
    return (
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{
          firstName, lastName, position, phoneCode: phoneCode || initialPhoneCode, phoneNumber,
        }}
        validate={(values) => {
          let unmaskedValue = values.phoneNumber;
          if (unmaskedValue) {
            unmaskedValue = values.phoneNumber.replace(/[- ]/g, '');
          }
          const errors = {};
          if (!values.firstName) {
            errors.firstName = i18n.__('main.onboarding.signup3.errors.noFirstName');
          } else if (!nameRegex.test(values.firstName)) {
            errors.firstName = i18n.__('main.onboarding.signup3.errors.invalidFirstName');
          } else if (values.firstName.length > 25) {
            errors.firstName = i18n.__('main.onboarding.signup3.errors.longFirstName');
          }
          if (!values.lastName) {
            errors.lastName = i18n.__('main.onboarding.signup3.errors.noLastName');
          } else if (!nameRegex.test(values.lastName)) {
            errors.lastName = i18n.__('main.onboarding.signup3.errors.invalidLastName');
          } else if (values.lastName.length > 25) {
            errors.lastName = i18n.__('main.onboarding.signup3.errors.longLastName');
          }
          if (!values.position) {
            errors.position = i18n.__('main.onboarding.signup3.errors.noPosition');
          }
          if (!values.phoneCode) {
            errors.phoneCode = i18n.__('main.onboarding.signup3.errors.noPhoneCode');
          } else if (findIndex(phoneCodesOptions, { value: values.phoneCode }) === -1) {
            errors.phoneCode = i18n.__('main.onboarding.signup3.errors.unsupportedPhoneCode');
          }
          if (!values.phoneNumber) {
            errors.phoneNumber = i18n.__('main.onboarding.signup3.errors.noPhoneNumber');
          } else if (!phoneNumberRegex.test(unmaskedValue)) {
            errors.phoneNumber = i18n.__('main.onboarding.signup3.errors.invalidPhoneNumber');
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={(values, { setSubmitting }) => {
          const unmaskedValues = { ...values, phoneNumber: values.phoneNumber.replace(/[- ]/g, '') };
          this.setState({ error: '' });
          submitSection3(unmaskedValues);
          const { email, password, ...profile } = omit({ ...signup, ...unmaskedValues }, 'step');
          addUser.callPromise({ email, password, profile })
            .then(id => Meteor.loginWithPassword({ id }, password))
            .catch((err) => {
              const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.onboarding.signup3.errors.signupError');
              setSubmitting(false);
              this.setState({
                error: `${i18n.__('main.onboarding.signup3.errors.signupFail')} ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values,
          errors, touched, isSubmitting, setTouched,
        }) => (
          <div>
            <Form onSubmit={handleSubmit}>
              <Header as="h3">
                <T>main.onboarding.signup3.title</T>
              </Header>
              <Divider hidden />
              <Form.Group widths="equal">
                <Form.Field>
                  <label>
                    <T>main.onboarding.signup3.firstName</T>
                  </label>
                  <input
                    type="text"
                    name="firstName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstName}
                  />
                  {errors.firstName && touched.firstName && (
                    <Label pointing basic color="red">
                      {errors.firstName}
                    </Label>
                  )}
                </Form.Field>
                <Form.Field>
                  <label>
                    <T>main.onboarding.signup3.lastName</T>
                  </label>
                  <input
                    type="text"
                    name="lastName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                  />
                  {errors.lastName && touched.lastName && (
                    <Label pointing basic color="red">
                      {errors.lastName}
                    </Label>
                  )}
                </Form.Field>
              </Form.Group>
              <Form.Field>
                <label htmlFor="position">
                  <T>main.onboarding.signup3.position</T>
                </label>
                <input
                  type="text"
                  id="position"
                  name="position"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.position}
                />
                {errors.position && touched.position && (
                  <Label pointing basic color="red">
                    {errors.position}
                  </Label>
                )}
              </Form.Field>
              <AlignedField>
                <Form.Group>
                  <Form.Field width={5}>
                    <label htmlFor="phoneCode">
                      <T>main.onboarding.signup3.phone</T>
                    </label>
                    <Dropdown
                      fluid
                      search
                      selection
                      id="phoneCode"
                      name="phoneCode"
                      options={phoneCodesOptions}
                      onChange={(event, data) => {
                        setTouched({ phoneCode: true });
                        handleChange({ ...event, target: { name: 'phoneCode', value: data.value } });
                      }}
                      onBlur={handleBlur}
                      value={values.phoneCode}
                      onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                    />
                    {errors.phoneCode && touched.phoneCode && (
                      <Label pointing basic color="red">
                        {errors.phoneCode}
                      </Label>
                    )}
                  </Form.Field>
                  <Form.Field width={11}>
                    {errors.phoneNumber && touched.phoneNumber && (
                      <Label pointing="below" basic color="red">
                        {errors.phoneNumber}
                      </Label>
                    )}
                    <InputMask
                      mask="99 99-99-99"
                      type="text"
                      name="phoneNumber"
                      placeholder="e.g. 77 55-55-55"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.phoneNumber}
                    />
                  </Form.Field>
                </Form.Group>
              </AlignedField>
              <Container textAlign="center">
                <Button loading={isSubmitting} type="submit" color="green"><T>main.onboarding.signup3.button</T></Button>
              </Container>
            </Form>
            <Container>
              <StyledError>
                <Message error hidden={!error}>
                  <Icon name="warning" />
                  {error}
                </Message>
              </StyledError>
            </Container>
          </div>
        )}
      />
    );
  }
}

SignupStep3.propTypes = {
  signup: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
    phoneCode: PropTypes.string.isRequired,
    phoneNumber: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    companyName: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    postalCode: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired,
  }).isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  phoneCode: PropTypes.string.isRequired,
  phoneNumber: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  submitSection3: PropTypes.func.isRequired,
};

const matStateToProps = ({ signup }) => {
  const {
    firstName, lastName, position, phoneCode, phoneNumber, country,
  } = signup;
  return {
    firstName, lastName, position, phoneCode, phoneNumber, country, signup,
  };
};

export default connect(matStateToProps, {
  submitSection3: submitSection3Action,
})(SignupStep3);
