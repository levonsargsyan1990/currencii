import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { changeStep as changeStepAction } from '../../actions/signup';

const StyledPoint = styled.span`
  display: block;
  margin: 10px auto 10px 20px;
  width: 10px;
  height: 10px;
  cursor: pointer;
  border-radius: 50%;
  border: 1px solid ${props => (props.isSelected ? props.theme.brand.green : 'rgba(34, 36, 38, 0.15)')};
  background-color: ${props => (props.isSelected ? props.theme.brand.green : 'transparent')};
`;

const SignupPoint = ({ currentStep, step, changeStep }) => {
  const handleClick = () => {
    if (currentStep > step) {
      changeStep(step);
    }
  };
  return (
    <StyledPoint
      isSelected={currentStep === step}
      onClick={handleClick}
    />
  );
};

SignupPoint.propTypes = {
  step: PropTypes.number.isRequired,
  currentStep: PropTypes.number.isRequired,
  changeStep: PropTypes.func.isRequired,
};

const mapStateToProp = ({ signup: { step } }) => ({ currentStep: step });

export default connect(mapStateToProp, { changeStep: changeStepAction })(SignupPoint);
