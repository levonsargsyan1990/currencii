import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import moment from 'moment';
import styled, { keyframes } from 'styled-components';
import { Popup } from 'semantic-ui-react';

import { Rates } from '../../../../api/rates/rates';

const blink = keyframes`
  0%   {opacity: 1;}
  40%  {opacity: 0;}
  80% {opacity: 1;}
`;

const StyledPoint = styled.span`
  display: inline-block;
  cursor: pointer;
  margin-left: 5px;
  position: relative;
  vertical-align: top;
  animation: ${blink} 1.75s ease-in-out infinite;
  display: inline-block;
  width: 5px;
  height: 5px;
  border-radius: 50%;
  background-color: ${props => props.theme.green};
  .label {
    position: absolute !important;
    text-align: left;
    top: 50%;
    transform: translate(0, -50%);
    z-index: 1;
    width: 150px;
    left: 100%;
    transition: opacity 0s;
    transition-delay: 0.5s;
    opacity: 0;
    pointer-events: none;
  }
  &:hover {
    .label {
      pointer-events: auto;
      opacity: 1;
    }
  }
`;

const GuaranteedRateInfo = ({ rateLoading, updatedAt }) => {
  if (rateLoading || !updatedAt) {
    return null;
  }
  const duration = moment.duration(moment().diff(moment(updatedAt)));
  let text = '';

  if (duration.asSeconds() < 60) {
    const seconds = parseInt(duration.asSeconds(), 10);
    text = `${seconds} second${seconds === 1 ? '' : 's'}`;
  } else if (duration.asMinutes() < 60) {
    const minutes = parseInt(duration.asMinutes(), 10);
    text = `${minutes} minute${minutes === 1 ? '' : 's'}`;
  } else {
    const hours = parseInt(duration.asHours(), 10);
    text = `${hours} hour${hours === 1 ? '' : 's'}`;
  }

  return (
    <Popup
      trigger={<StyledPoint />}
      position="right center"
    >
      {`Updated ${text} ago`}
    </Popup>
  );
};

GuaranteedRateInfo.propTypes = {
  rateLoading: PropTypes.bool.isRequired,
  updatedAt: PropTypes.instanceOf(Date),
};

GuaranteedRateInfo.defaultProps = {
  updatedAt: null,
};

const mapDataToProps = ({ buyCurrency, sellCurrency }) => {
  const rateHandle = Meteor.subscribe('rates.getRateByCurrencies', { buyCurrency, sellCurrency });
  const rateLoading = !rateHandle.ready();
  const rate = Rates.findOne();
  return { updatedAt: rate ? rate.marketRate.updatedAt : null, rateLoading };
};

export default withTracker(mapDataToProps)(GuaranteedRateInfo);
