// TODO: Handle the case when the account has been edited or deleted during the transaction
// TODO: Set learn more URL
// TODO: Add updated X mins ago

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import {
  Container, Segment, Grid, Popup, Button, Divider, Header, Message, Icon,
} from 'semantic-ui-react';
import i18n from 'meteor/universe:i18n';

import { numberWithCommas } from '../../../../lib/helpers';
import { addTransaction } from '../../../../api/transactions/methods';
import { changeStep as changeStepAction, setTransactionId as setTransactionIdAction } from '../../../actions/exchange';

const T = i18n.createComponent();

const StyledError = styled.div`
  margin-top: 15px;
`;

const LabeledText = styled.p`
  cursor: pointer;
  text-decoration: underline ${props => props.theme.anchorBlue} dotted;
`;

class ExchangeStep3 extends Component {
  state = {
    error: '',
  }

  handleConfirm() {
    this.setState({ error: '' });
    const {
      sellCurrency, sellAmount, buyCurrency, buyAmount,
      bankAccount, marketRate, guaranteedRate, savedAmount,
      changeStep, setTransactionId, reference, transactionId,
    } = this.props;

    if (transactionId) {
      changeStep(4);
    } else {
      const transaction = {
        reference,
        sell: {
          amount: sellAmount,
          currency: sellCurrency,
        },
        buy: {
          amount: buyAmount,
          currency: buyCurrency,
        },
        savedAmount,
        marketRate,
        guaranteedRate,
        bankAccount,
      };
      addTransaction.callPromise(transaction)
        .then(id => setTransactionId({ transactionId: id }))
        .then(changeStep.bind(this, 4))
        .catch((err) => {
          const message = (err.reason && err.reason.toLowerCase()) || i18n.__('main.exchange.stepThree.tryLater');
          this.setState({
            error: `${i18n.__('main.exchange.stepThree.fail')} ${message}`,
          });
        });
    }
  }

  render() {
    const {
      sellCurrency,
      sellAmount,
      buyCurrency,
      buyAmount,
      bankAccount,
      marketRate,
      guaranteedRate,
      savedAmount,
      changeStep,
      reference,
    } = this.props;
    const { error } = this.state;
    return (
      <Segment color="teal">
        <Container textAlign="center">
          <Divider hidden />
          <Grid centered columns={2}>
            <Grid.Row colmuns={2}>
              <Grid.Column textAlign="right">
                <p><T>main.exchange.stepThree.sell</T></p>
                <p><T>main.exchange.stepThree.buy</T></p>
                <p><T>main.exchange.stepThree.curRate</T></p>
                <Popup
                  trigger={<LabeledText><T>main.exchange.stepThree.guaranteedRate</T></LabeledText>}
                  position="right center"
                  on="click"
                >
                  <>
                    <Header as="h4">
                      <T>main.exchange.tepThree.guaranteedRateHeader</T>
                    </Header>
                    <p><T>main.exchange.stepThree.guaranteedRateText</T></p>
                    <Link to="/exchange"><T>main.exchange.stepThree.learnMore</T></Link>
                  </>
                </Popup>
                <p><T>main.exchange.stepThree.labelSave</T></p>
                <p><T>main.exchange.stepThree.transferredTo</T></p>
                <p><T>main.exchange.stepThree.refNumber</T></p>
              </Grid.Column>
              <Grid.Column>
                <p>{`${sellCurrency.toUpperCase()} ${numberWithCommas(sellAmount.toFixed(2))}`}</p>
                <p>{`${buyCurrency.toUpperCase()} ${numberWithCommas(buyAmount.toFixed(2))}`}</p>
                <p>{marketRate}</p>
                <p>{guaranteedRate}</p>
                <p>{`${numberWithCommas(savedAmount.toFixed(2))} ${sellCurrency.toUpperCase()}`}</p>
                <p>{bankAccount.accountNumber}</p>
                <p>{reference}</p>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <Divider hidden />
          <Container textAlign="center">
            <Button
              type="button"
              basic
              content="Back"
              icon="left arrow"
              labelPosition="left"
              onClick={() => changeStep(2)}
            />
            <Button
              type="button"
              color="green"
              onClick={() => this.handleConfirm()}
            >
              <T>main.exchange.tepThree.button</T>
            </Button>
          </Container>
          <Divider hidden />
        </Container>
        <StyledError>
          <Message error hidden={!error}>
            <Icon name="warning" />
            {error}
          </Message>
        </StyledError>
      </Segment>
    );
  }
}

ExchangeStep3.propTypes = {
  bankAccount: PropTypes.shape({
    accountNumber: PropTypes.string.isRequired,
  }).isRequired,
  buyAmount: PropTypes.number.isRequired,
  buyCurrency: PropTypes.string.isRequired,
  sellAmount: PropTypes.number.isRequired,
  sellCurrency: PropTypes.string.isRequired,
  marketRate: PropTypes.number.isRequired,
  guaranteedRate: PropTypes.number.isRequired,
  savedAmount: PropTypes.number.isRequired,
  reference: PropTypes.string.isRequired,
  transactionId: PropTypes.string.isRequired,
  setTransactionId: PropTypes.func.isRequired,
  changeStep: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  exchange: {
    sellAmount,
    sellCurrency,
    buyAmount,
    buyCurrency,
    bankAccount,
    marketRate,
    guaranteedRate,
    savedAmount,
    reference,
    transactionId,
  },
}) => ({
  sellAmount,
  sellCurrency,
  buyAmount,
  buyCurrency,
  bankAccount,
  marketRate,
  guaranteedRate,
  savedAmount,
  transactionId,
  reference,
});

export default connect(mapStateToProps, {
  changeStep: changeStepAction,
  setTransactionId: setTransactionIdAction,
})(ExchangeStep3);
