// TODO: Set learn more URL
// TODO: Investigate USD to AMD saved amount bug
/* eslint-disable no-restricted-globals */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';
import qs from 'query-string';
import NumberFormat from 'react-number-format';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Link, withRouter } from 'react-router-dom';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import findIndex from 'lodash/findIndex';
import find from 'lodash/find';
import styled from 'styled-components';
import {
  Container, Segment, Grid, Form, Label, Dropdown,
  Button, Divider, Header, Message, Icon, Popup,
} from 'semantic-ui-react';

import i18n from 'meteor/universe:i18n';
import { numberRegex, currencyOptions } from '../../../../lib/constants';
import {
  changeStep as changeStepAction,
  submitSection1 as submitSection1Action,
} from '../../../actions/exchange';
import VerificationModal from '../verification/VerificationModal';
import GuaranteedRateInfo from './GuaranteedRateInfo';
import { Rates } from '../../../../api/rates/rates';
import { numberWithCommas } from '../../../../lib/helpers';

const T = i18n.createComponent();

const ResponsiveSegment = styled.div`
  @media only screen and (max-width: ${props => props.theme.breakpoints.tablet}) {
    max-width: 400px;
    margin: auto;
    .segment {
      .calculator-grid {
        .row {
          display: block;
          .column {
            width: 100%;
            margin-bottom: 15px;
          }
        }
      }
      .result-grid {
        .row {
          .column {
            width: 33%;
            &.right {
              width: 67%;
            }
          }
        }
      }
      .divider:last-of-type {
        display: none;
      }
      .center.aligned.container {
        button {
          display: block;
          width: 100%;
        }
      }
    }
  }
`;

const LabeledText = styled.p`
  cursor: pointer;
  text-decoration: underline ${props => props.theme.anchorBlue} dotted;
`;

const StyledError = styled.div`
  margin-top: 15px;
`;

const StyledForm = styled.div`
  /* hide up/down arrows ("spinners") on input fields marked type="number" */
  input {
    -moz-appearance: textfield;
  }

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  .menu.transition {
    max-height: 200px;
    overflow-y: auto;
  }
`;

class ExchangeStep1 extends Component {
  state = {
    error: '',
    isLoading: true,
    isVerifyModalOpen: false,
  };

  defaultValues = {
    marketRate: '',
    guaranteedRate: '',
    savedAmount: '',
  }

  componentDidMount() {
    const {
      sellCurrency,
      buyCurrency,
      sellAmount,
      buyAmount,
      marketRate,
      guaranteedRate,
      savedAmount,
      location: { search },
    } = this.props;

    const values = {
      sellCurrency,
      buyCurrency,
      sellAmount,
      buyAmount,
      marketRate,
      guaranteedRate,
      savedAmount,
    };

    if (search) {
      const {
        sa, sc, ba, bc,
      } = qs.parse(search);
      if (!buyAmount && !sellAmount) {
        values.sellAmount = sa;
        values.sellCurrency = sc;
        values.buyAmount = ba;
        values.buyCurrency = bc;
      }
    }
    this.values = values;
  }

  componentDidUpdate(prevProps) {
    const {
      sellCurrency,
      buyCurrency,
      marketRate,
      guaranteedRate,
    } = this.values;
    const { ratesLoading } = this.props;
    if (!ratesLoading && prevProps.ratesLoading) {
      if (!marketRate || !guaranteedRate) {
        this.updateMidAndGuaranteedRates({ sellCurrency, buyCurrency });
      }
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ isLoading: false });
    }
  }

  getMidAndGuaranteedRates({ sellCurrency, buyCurrency }) {
    const { rates } = this.props;
    const rate = find(rates, ({ sell, buy }) => ((sell.currency === sellCurrency)
      && (buy.currency === buyCurrency)));
    if (!rate) {
      throw new Meteor.Error(
        'rate-error',
        `We are not converting ${sellCurrency.toUpperCase()} to ${buyCurrency.toUpperCase()} yet`,
      );
    }
    const {
      guaranteedRate: {
        rate: guaranteedRate,
      },
      marketRate: {
        rate: marketRate,
      },
    } = rate;
    return { marketRate, guaranteedRate };
  }

  updateValues(changedValue, values) {
    const currentState = { ...values, ...changedValue };
    const {
      sellCurrency,
      buyCurrency,
      sellAmount,
      buyAmount,
    } = currentState;
    const {
      sellAmount: oldSellAmount,
      buyAmount: oldBuyAmount,
      sellCurrency: oldSellCurrency,
      buyCurrency: oldBuyCurrency,
    } = values;

    // if sell/buy currencies were changed,
    // get mid market and guaranteed rates are queried from database
    if (sellCurrency !== oldSellCurrency || buyCurrency !== oldBuyCurrency) {
      this.setState({ error: '' });
      const { error } = this.updateMidAndGuaranteedRates({ sellCurrency, buyCurrency });
      if (error) {
        return;
      }
    }
    const { marketRate, guaranteedRate } = this.values;
    const newState = { ...currentState, marketRate, guaranteedRate };

    // if the sell amount was changed
    if (sellAmount !== oldSellAmount) {
      if (!sellAmount) {
        newState.buyAmount = '';
      } else {
        newState.sellAmount = parseFloat(sellAmount, 10);
        newState.buyAmount = parseFloat((sellAmount * newState.guaranteedRate).toFixed(2), 10);
      }
    }

    // if the buy amount was changed
    if (buyAmount !== oldBuyAmount) {
      if (!buyAmount) {
        newState.sellAmount = '';
      } else {
        newState.buyAmount = parseFloat(buyAmount, 10);
        newState.sellAmount = parseFloat((buyAmount / newState.guaranteedRate).toFixed(2), 10);
      }
    }

    newState.savedAmount = parseFloat(((newState.sellAmount / newState.marketRate)
      - (newState.sellAmount / newState.guaranteedRate)).toFixed(2), 10);
    this.values = newState;
  }

  updateMidAndGuaranteedRates({ sellCurrency, buyCurrency }) {
    try {
      this.values = {
        ...this.values,
        ...this.getMidAndGuaranteedRates({ sellCurrency, buyCurrency }),
      };
      return { error: '', values: this.values };
    } catch (err) {
      const message = (err.error && err.error) === 'rate-error' ? err.reason : 'Failed to calculate. please try again later';
      this.values = {
        ...this.values, ...this.defaultValues, sellCurrency, buyCurrency,
      };
      this.setState({ error: message });
      return { error: message, values: null };
    }
  }

  handleVerifyModalToggle() {
    const { isVerifyModalOpen } = this.state;
    this.setState({ isVerifyModalOpen: !isVerifyModalOpen });
  }

  render() {
    const { isLoading, isVerifyModalOpen, error } = this.state;
    const {
      isVerified,
      changeStep,
      submitSection1,
      ratesLoading,
    } = this.props;

    return (
      <ResponsiveSegment>
        <Segment color="teal" loading={isLoading || ratesLoading}>
          {!isLoading ? (
            <Formik
              initialValues={this.values}
              validate={(values) => {
                const errors = {};
                if (!values.sellAmount) {
                  errors.sellAmountHidden = 'Please enter the amount of money you want to sell';
                } else if (values.sellAmount <= 0) {
                  errors.sellAmount = 'The amount should be more then zero';
                } else if (!numberRegex.test(values.sellAmount)) {
                  errors.sellAmount = 'The amount can only be a number';
                }
                if (!values.sellCurrency) {
                  errors.sellCurrency = 'Please select the currency';
                } else if (values.buyAmount <= 0) {
                  errors.buyAmountHidden = 'The amount should be more then zero';
                } else if (findIndex(currencyOptions, { value: values.sellCurrency }) === -1) {
                  errors.sellCurrency = 'We only provide service in currencies from the dropdown list';
                }
                if (!values.buyAmount) {
                  errors.buyAmountHidden = 'Please enter the amount of money you want to buy';
                } else if (!numberRegex.test(values.buyAmount)) {
                  errors.buyAmount = 'The amount can only be a number';
                }
                if (!values.buyCurrency) {
                  errors.buyCurrency = 'Please select the currency';
                } else if (findIndex(currencyOptions, { value: values.buyCurrency }) === -1) {
                  errors.buyCurrency = 'We only provide service in currencies from the dropdown list';
                }
                return errors;
              }}
              isInitialValid={({ validate, initialValues }) => {
                const errors = validate(initialValues);
                if (Object.keys(errors).length > 0) {
                  return false;
                }
                return true;
              }}
              onSubmit={(values, { setSubmitting }) => {
                if (isVerified) {
                  submitSection1(this.values);
                  changeStep(2);
                } else {
                  setSubmitting(false);
                  this.handleVerifyModalToggle();
                }
              }}
              render={({
                handleSubmit, handleChange, handleBlur, values, errors,
                touched, isValid, isSubmitting, setTouched, setValues,
              }) => (
                <StyledForm>
                  <Form
                    onSubmit={handleSubmit}
                    autoComplete="off"
                  >
                    <Grid className="calculator-grid" columns={2}>
                      <Grid.Row>
                        <Grid.Column>
                          <Form.Field>
                            <label>
                              You sell
                            </label>
                            <div className="ui right labeled input">
                              <NumberFormat
                                name="sellAmount"
                                thousandSeparator
                                onKeyPress={e => e.key === 'Enter' && e.preventDefault()}
                                isAllowed={({ value }) => {
                                  if (parseFloat(value, 10) === 0) {
                                    return false;
                                  }
                                  return true;
                                }}
                                onChange={(event) => {
                                  handleChange(event);
                                  const value = event.target.value.replace(/,/g, '');
                                  this.updateValues({ sellAmount: value }, values);
                                  setValues(this.values);
                                }}
                                onBlur={handleBlur}
                                value={values.sellAmount}
                              />
                              <Dropdown
                                className="label"
                                options={currencyOptions}
                                onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                                onChange={(event, data) => {
                                  setTouched({ sellCurrency: true });
                                  handleChange({ ...event, target: { name: 'sellCurrency', value: data.value } });
                                  this.updateValues({ sellCurrency: data.value }, values);
                                  setValues(this.values);
                                }}
                                onBlur={(event) => {
                                  handleBlur({ ...event, target: { name: 'sellCurrency' } });
                                }}
                                name="sellCurrency"
                                value={values.sellCurrency}
                              />
                            </div>
                            {((errors.sellCurrency && touched.sellCurrency)
                              || (errors.sellAmount && touched.sellAmount))
                              && (
                                <Label pointing basic color="red">
                                  {errors.sellAmount || errors.sellCurrency}
                                </Label>
                              )
                            }
                          </Form.Field>
                        </Grid.Column>
                        <Grid.Column>
                          <Form.Field>
                            <label>
                              You buy
                            </label>
                            <div className="ui right labeled input">
                              <NumberFormat
                                name="buyAmount"
                                thousandSeparator
                                onKeyPress={e => e.key === 'Enter' && e.preventDefault()}
                                isAllowed={({ value }) => {
                                  if (parseFloat(value, 10) === 0) {
                                    return false;
                                  }
                                  return true;
                                }}
                                onChange={(event) => {
                                  handleChange(event);
                                  const value = event.target.value.replace(/,/g, '');
                                  this.updateValues({ buyAmount: value }, values);
                                  setValues(this.values);
                                }}
                                onBlur={handleBlur}
                                value={values.buyAmount}
                              />
                              <Dropdown
                                className="label"
                                options={currencyOptions}
                                onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                                onChange={(event, data) => {
                                  setTouched({ buyCurrency: true });
                                  handleChange({ ...event, target: { name: 'buyCurrency', value: data.value } });
                                  this.updateValues({ buyCurrency: data.value }, values);
                                  setValues(this.values);
                                }}
                                onBlur={(event) => {
                                  handleBlur({ ...event, target: { name: 'buyCurrency' } });
                                }}
                                name="buyCurrency"
                                value={values.buyCurrency}
                              />
                            </div>
                            {((errors.buyCurrency && touched.buyCurrency)
                              || (errors.buyAmount && touched.buyAmount))
                              && (
                                <Label pointing basic color="red">
                                  {errors.buyAmount || errors.buyCurrency}
                                </Label>
                              )
                            }
                          </Form.Field>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                    <Grid className="result-grid" columns={2}>
                      <Grid.Row>
                        <Grid.Column textAlign="right">
                          <p>Current mid market rate</p>
                          <Popup
                            trigger={<LabeledText>Your guaranteed rate (24 hrs)</LabeledText>}
                            position="right center"
                            on="click"
                          >
                            <>
                              <Header as="h4">
                                Guaranteed rate
                              </Header>
                              <p>
                                You&apos;ll get this rate as long as we
                                receive your money within the next 24 hours.
                              </p>
                              <Link to="/exchange">Learn more</Link>
                            </>
                          </Popup>
                          <p>You could save up to</p>
                        </Grid.Column>
                        <Grid.Column>
                          <p>{values.marketRate}</p>
                          <p>
                            {values.guaranteedRate}
                            {error ? null : (
                              <GuaranteedRateInfo
                                buyCurrency={values.buyCurrency}
                                sellCurrency={values.sellCurrency}
                              />
                            )}
                          </p>
                          {/* eslint-disable-line no-restricted-globals */}
                          <p>{typeof values.savedAmount === 'number' && !isNaN(values.savedAmount) ? `${numberWithCommas(values.savedAmount)} ${values.sellCurrency.toUpperCase()}` : ''}</p>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                    <Divider hidden />
                    <Container textAlign="center">
                      <Button disabled={!isValid || !!error} loading={isSubmitting} type="submit" color="green">Next</Button>
                    </Container>
                    <Divider hidden />
                    <VerificationModal
                      onClose={() => this.handleVerifyModalToggle()}
                      isOpen={isVerifyModalOpen}
                    />
                  </Form>
                </StyledForm>
              )}
            />
          ) : <Divider hidden />}
          <StyledError>
            <Message error hidden={!error}>
              <Icon name="warning" />
              {error}
            </Message>
          </StyledError>
        </Segment>
      </ResponsiveSegment>
    );
  }
}

ExchangeStep1.propTypes = {
  isVerified: PropTypes.bool,
  sellAmount: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  sellCurrency: PropTypes.string.isRequired,
  buyAmount: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  buyCurrency: PropTypes.string.isRequired,
  changeStep: PropTypes.func.isRequired,
  submitSection1: PropTypes.func.isRequired,
  ratesLoading: PropTypes.bool.isRequired,
  marketRate: PropTypes.number.isRequired,
  guaranteedRate: PropTypes.number.isRequired,
  savedAmount: PropTypes.number.isRequired,
  rates: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    sell: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    buy: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    guaranteedRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
    marketRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
    active: PropTypes.bool.isRequired,
  })),
  location: ReactRouterPropTypes.location.isRequired,
};

ExchangeStep1.defaultProps = {
  isVerified: false,
  rates: null,
};

const mapStateToProps = ({
  exchange: {
    buyAmount, sellAmount, buyCurrency, sellCurrency, marketRate, guaranteedRate, savedAmount,
  },
}) => ({
  buyAmount,
  buyCurrency,
  sellAmount,
  sellCurrency,
  marketRate,
  guaranteedRate,
  savedAmount,
});

const mapDataToProps = () => {
  const user = Meteor.user();
  const { profile: { verification: { identity, business } } } = user;
  const isVerified = identity.status === 'verified' && business.status === 'verified';
  const ratesHandle = Meteor.subscribe('rates.listActive');
  const ratesLoading = !ratesHandle.ready();
  const rates = Rates.find().fetch();
  return { isVerified, ratesLoading, rates };
};

const meteorWrappedComponent = withTracker(mapDataToProps)(ExchangeStep1);

const connectedComponent = connect(mapStateToProps, {
  changeStep: changeStepAction,
  submitSection1: submitSection1Action,
})(meteorWrappedComponent);

export default withRouter(connectedComponent);
