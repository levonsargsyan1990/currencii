// TODO: Handle the case when the account has been edited or deleted during the transaction

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import findIndex from 'lodash/findIndex';
import find from 'lodash/find';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import styled from 'styled-components';
import {
  Container, Segment, Grid, Form, Label, Dropdown, Button, Divider, Header, Loader,
} from 'semantic-ui-react';
import i18n from 'meteor/universe:i18n';

import { numberWithCommas } from '../../../../lib/helpers';
import { BankAccounts as Accounts } from '../../../../api/bankAccounts/bankAccounts';
import AddBankAccount from '../profile/AddBankAccount';

import {
  changeStep as changeStepAction,
  submitSection2 as submitSection2Action,
} from '../../../actions/exchange';

const T = i18n.createComponent();

const ResponsiveSegment = styled.div`
  @media only screen and (max-width: ${props => props.theme.breakpoints.tablet}) {
    max-width: 400px;
    margin: auto;
    .segment {
      .account-grid {
        .column {
          width: 100% !important;
        }
      }
    }
  }
`;

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
`;

class ExchangeStep2 extends Component {
  state = {
    isAddBankModalOpen: false,
  };

  handleAddBankModalToggle() {
    const { isAddBankModalOpen } = this.state;
    this.setState({ isAddBankModalOpen: !isAddBankModalOpen });
  }

  render() {
    const {
      buyCurrency,
      buyAmount,
      bankAccount,
      loadingAccounts,
      accounts,
      changeStep,
      submitSection2,
    } = this.props;

    if (loadingAccounts) {
      return (
        <Segment color="teal">
          <Loader />
        </Segment>
      );
    }

    const { isAddBankModalOpen } = this.state;

    let bankAccountOptions = [];
    if (!loadingAccounts && accounts.length > 0) {
      bankAccountOptions = accounts.map(account => (
        {
          key: account._id, value: account._id, text: `${account.accountNumber}, ${account.bankName}`,
        }
      ));
    } else {
      bankAccountOptions = [(
        <Dropdown.Item
          key="new account"
          value=""
          content={(
            <Grid comlumns={2}>
              <Grid.Row>
                <Grid.Column width={12}>
                  <T>main.exchange.exchangeStep2.noBankAccount</T>
                </Grid.Column>
                <Grid.Column textAlign="right" width={4}>
                  <AnchorStyledText
                    onClick={(event) => {
                      event.stopPropagation();
                      this.handleAddBankModalToggle(event);
                    }}
                  >
                    <T>main.exchange.exchangeStep2.add</T>
                  </AnchorStyledText>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          )}
        />
      )];
    }

    return (
      <ResponsiveSegment>
        <Segment color="teal" textAlign="center">
          <Divider hidden />
          <Header as="h1">
            {`${numberWithCommas(buyAmount.toFixed(2))} ${buyCurrency.toUpperCase()}`}
            <Header.Subheader>
              <T>main.exchange.exchangeStep2.transferedTo</T>
            </Header.Subheader>
          </Header>
          <Divider hidden />
          <Formik
            enableReinitialize
            initialValues={{ bankAccount: bankAccount._id || (accounts[0] ? accounts[0]._id : '') }}
            validate={(values) => {
              const errors = {};
              if (!values.bankAccount) {
                errors.bankAccount = i18n.__('main.exchange.exchangeStep2.selectAccount');
              } else if (findIndex(bankAccountOptions, { value: values.bankAccount }) === -1) {
                errors.bankAccount = i18n.__('main.exchange.exchangeStep2.onlyOneAccont');
              }
              return errors;
            }}
            isInitialValid={({ validate, initialValues }) => {
              const errors = validate(initialValues);
              if (Object.keys(errors).length > 0) {
                return false;
              }
              return true;
            }}
            onSubmit={(values) => {
              const selectedBankAccount = find(accounts, { _id: values.bankAccount });
              const { createdAt, userId, ...account } = selectedBankAccount;
              submitSection2({ ...values, bankAccount: account });
              changeStep(3);
            }}
            render={({
              handleSubmit, handleChange, handleBlur, values, errors,
              touched, isValid, isSubmitting, setTouched,
            }) => (
              <Form onSubmit={handleSubmit}>
                <Grid className="account-grid" centered columns={2}>
                  <Grid.Column>
                    <Form.Field>
                      <Dropdown
                        loading={loadingAccounts}
                        fluid
                        search
                        selection
                        name="bankAccount"
                        options={bankAccountOptions}
                        onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                        onChange={async (event, data) => {
                          setTouched({ bankAccount: true });
                          handleChange({ ...event, target: { name: 'bankAccount', value: data.value } });
                        }}
                        onBlur={(event) => {
                          handleBlur({ ...event, target: { name: 'bankAccount' } });
                        }}
                        value={values.bankAccount}
                      />
                      {errors.bankAccount && touched.bankAccount && (
                        <Label pointing basic color="red">
                          {errors.bankAccount}
                        </Label>
                      )}
                    </Form.Field>
                  </Grid.Column>
                </Grid>
                <Divider hidden />
                <Container textAlign="center">
                  <Button
                    type="button"
                    basic
                    content="Back"
                    icon="left arrow"
                    labelPosition="left"
                    onClick={() => changeStep(1)}
                  />
                  <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">
                    <T>main.exchange.exchangeStep2.button</T>
                  </Button>
                </Container>
                <Divider hidden />
              </Form>
            )}
          />
          <AddBankAccount
            onClose={() => this.handleAddBankModalToggle()}
            isOpen={isAddBankModalOpen}
          />
        </Segment>
      </ResponsiveSegment>
    );
  }
}

ExchangeStep2.propTypes = {
  loadingAccounts: PropTypes.bool.isRequired,
  accounts: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    bankName: PropTypes.string.isRequired,
    swift: PropTypes.string.isRequired,
    iban: PropTypes.string.isRequired,
    accountNumber: PropTypes.string.isRequired,
  })),
  bankAccount: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    bankName: PropTypes.string.isRequired,
    swift: PropTypes.string.isRequired,
    iban: PropTypes.string.isRequired,
    accountNumber: PropTypes.string.isRequired,
  }).isRequired,
  buyAmount: PropTypes.number.isRequired,
  buyCurrency: PropTypes.string.isRequired,
  changeStep: PropTypes.func.isRequired,
  submitSection2: PropTypes.func.isRequired,
};

ExchangeStep2.defaultProps = {
  accounts: [],
};

const mapDataToProps = () => {
  const userId = Meteor.userId();
  if (userId) {
    const accountHandle = Meteor.subscribe('bankAccounts.listByUser', { userId });
    const loadingAccounts = !accountHandle.ready();
    const accounts = Accounts.find().fetch();
    return {
      loadingAccounts,
      accounts,
    };
  }
  return {};
};

const meteorWrappedComponent = withTracker(mapDataToProps)(ExchangeStep2);

const mapStateToProps = ({
  exchange: {
    buyAmount,
    buyCurrency,
    bankAccount,
  },
}) => ({
  bankAccount,
  buyCurrency,
  buyAmount,
});

export default connect(mapStateToProps, {
  changeStep: changeStepAction,
  submitSection2: submitSection2Action,
})(meteorWrappedComponent);
