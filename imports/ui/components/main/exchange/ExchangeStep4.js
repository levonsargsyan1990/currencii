// TODO: Handle the case when the account has been edited or deleted during the transaction
// TODO: Set 2 learn more URL
// TODO: Add updated X mins ago
// TODO: navigate to transactions

import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Meteor } from 'meteor/meteor';
import styled from 'styled-components';
import {
  Container, Segment, Popup, Button, Divider, Header,
} from 'semantic-ui-react';
import i18n from 'meteor/universe:i18n';

import { numberWithCommas } from '../../../../lib/helpers';
import {
  reset as resetAction,
  changeStep as changeStepAction,
} from '../../../actions/exchange';

const T = i18n.createComponent();

const { bankAccount } = Meteor.settings.public;

const LabeledText = styled.div`
  margin: calc(2rem - .14285714em) 0 1rem;
  display: inline-block;
  .header {
    cursor: pointer;
    line-height: 1;
    border-bottom: 2px dotted ${props => props.theme.anchorBlue};
  }
`;

const ImportantText = styled.div`
  display: inline-block;
  .header {
    cursor: pointer;
    text-decoration: underline;
  }
`;

const ExchangeStep4 = ({
  sellCurrency,
  sellAmount,
  reference,
  transactionId,
  changeStep,
  history,
  reset,
}) => (
  <Segment color="teal">
    <Container textAlign="center">
      <Divider hidden />
      <p>
        {`Please transfer ${sellCurrency.toUpperCase()} ${numberWithCommas(sellAmount.toFixed(2))}`}
        <br />
        <T>main.exchange.stepFour.following</T>
      </p>
      <Header>
        {bankAccount}
      </Header>
      <p><T>main.exchange.stepFour.refNumber</T></p>
      <Popup
        trigger={(
          <LabeledText>
            <Header>{reference}</Header>
          </LabeledText>
        )}
        position="right center"
        on="click"
      >
        <>
          <p><T>main.exchange.stepFour.includeRefNumber</T></p>
          <Link to="/exchange"><T>main.exchange.stepFour.learnMore</T></Link>
        </>
      </Popup>
      <Divider hidden />
      <Popup
        trigger={(
          <ImportantText>
            <Header color="red"><T>main.exchange.stepFour.important</T></Header>
          </ImportantText>
        )}
        position="right center"
        on="click"
      >
        <>
          <p><T>main.exchange.stepFour.warning</T></p>
          <p><T>main.exchange.stepFour.notification</T></p>
          <Link to="/exchange"><T>main.exchange.stepFour.learnMore</T></Link>
        </>
      </Popup>
      <Divider hidden />
      <Container textAlign="center">
        <Button
          type="button"
          basic
          content="Back"
          icon="left arrow"
          labelPosition="left"
          onClick={() => changeStep(3)}
        />
        <Button
          type="button"
          color="green"
          onClick={() => {
            history.push(`/transactions?highlight=${transactionId}`);
            reset();
          }}
        >
          <T>main.exchange.stepFour.button</T>
        </Button>
      </Container>
      <Divider hidden />
    </Container>
  </Segment>
);

ExchangeStep4.propTypes = {
  sellAmount: PropTypes.number.isRequired,
  sellCurrency: PropTypes.string.isRequired,
  reference: PropTypes.string.isRequired,
  transactionId: PropTypes.string.isRequired,
  changeStep: PropTypes.func.isRequired,
  history: ReactRouterPropTypes.history.isRequired,
  reset: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  exchange: {
    sellAmount,
    sellCurrency,
    reference,
    transactionId,
  },
}) => ({
  sellAmount,
  sellCurrency,
  reference,
  transactionId,
});

const connectedComponent = connect(mapStateToProps, {
  changeStep: changeStepAction,
  reset: resetAction,
})(ExchangeStep4);

export default withRouter(connectedComponent);
