import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Container, Grid, Progress, Header,
} from 'semantic-ui-react';
import { connect } from 'react-redux';
import i18n from 'meteor/universe:i18n';

const T = i18n.createComponent();

const StyledGrid = styled.div`
  .grid {
    width: 133.333%;
    margin-left: -16.666%;
    margin-bottom: 0px;
  }
`;

const ExchangeProgressBar = ({ step, inProgress }) => {
  const percent = Math.round((step - 1) * 33.333);
  if (!inProgress) {
    return null;
  }
  return (
    <Container>
      <StyledGrid>
        <Grid textAlign="center">
          <Grid.Row columns={4}>
            <Grid.Column>
              <Header as="h4">
                <T>main.exchange.amount</T>
              </Header>
            </Grid.Column>
            <Grid.Column>
              <Header as="h4">
                <T>main.exchange.account</T>
              </Header>
            </Grid.Column>
            <Grid.Column>
              <Header as="h4">
                <T>main.exchange.review</T>
              </Header>
            </Grid.Column>
            <Grid.Column>
              <Header as="h4">
                <T>main.exchange.pay</T>
              </Header>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </StyledGrid>
      <Progress percent={percent} size="tiny" active />
    </Container>
  );
};

ExchangeProgressBar.propTypes = {
  inProgress: PropTypes.bool.isRequired,
  step: PropTypes.number.isRequired,
};

const mapStateToProps = ({ exchange: { step, inProgress } }) => ({ step, inProgress });

export default connect(mapStateToProps)(ExchangeProgressBar);
