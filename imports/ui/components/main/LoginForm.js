import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Formik } from 'formik';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import i18n from 'meteor/universe:i18n';
import {
  Header, Divider, Form, Label, Button, Container, Message, Icon,
} from 'semantic-ui-react';
import { allowClientLogin } from '../../../api/users/methods';

import { emailRegex, passwordRegex } from '../../../lib/constants';

const StyledLink = styled.span`
  margin: 0 10px;
  display: inline-block;
`;

const T = i18n.createComponent();

const StyledError = styled.div`
  margin-top: 15px;
`;

class LoginForm extends Component {
  state = {
    error: '',
  };

  // eslint-disable-next-line class-methods-use-this
  login({ email, password }) {
    return new Promise((resolve, reject) => {
      Meteor.loginWithPassword(email, password, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  render() {
    const { error } = this.state;
    return (
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{ email: '', password: '' }}
        validate={(values) => {
          const errors = {};
          if (!values.email) {
            errors.email = i18n.__('main.onboarding.login.errors.noEmail');
          } else if (!emailRegex.test(values.email)) {
            errors.email = i18n.__('main.onboarding.login.errors.invalidEmail');
          }
          if (!values.password) {
            errors.password = i18n.__('main.onboarding.login.errors.noPassword');
          } else if (!passwordRegex.test(values.password)) {
            errors.password = i18n.__('main.onboarding.login.errors.invalidPassword');
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={({ email, password }, { setSubmitting }) => {
          allowClientLogin.callPromise({ email })
            .then(this.login.bind(this, { email, password }))
            .catch((err) => {
              const message = err.reason.toLowerCase() || i18n.__('main.onboarding.login.errors.loginError');
              setSubmitting(false);
              this.setState({
                error: `${i18n.__('main.onboarding.login.errors.loginFail')}: ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values, errors, touched, isSubmitting,
        }) => (
          <div>
            <Form onSubmit={handleSubmit}>
              <Header as="h3">
                <T>main.onboarding.login.title</T>
              </Header>
              <Divider hidden />
              <Form.Field>
                <label>
                  <T>main.onboarding.login.email</T>
                </label>
                <input
                  type="text"
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                />
                {errors.email && touched.email && (
                  <Label pointing basic color="red">
                    {errors.email}
                  </Label>
                )}
              </Form.Field>
              <Form.Field>
                <label>
                  <T>main.onboarding.login.password</T>
                </label>
                <input
                  type="password"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                {errors.password && touched.password && (
                  <Label pointing basic color="red">
                    {errors.password}
                  </Label>
                )}
              </Form.Field>
              <Container textAlign="right">
                <StyledLink>
                  <Link to="/forgot-password">Forgot password</Link>
                </StyledLink>
              </Container>
              <Divider hidden />
              <Container textAlign="center">
                <Button loading={isSubmitting} type="submit" color="green"><T>main.onboarding.login.button</T></Button>
              </Container>
            </Form>
            <Container>
              <StyledError>
                <Message error hidden={!error}>
                  <Icon name="warning" />
                  {error}
                </Message>
              </StyledError>
            </Container>
          </div>
        )}
      />
    );
  }
}

export default LoginForm;
