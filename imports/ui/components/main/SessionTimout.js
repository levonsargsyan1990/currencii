import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import {
  Modal, Container, Button, Divider,
} from 'semantic-ui-react';

const TIMEOUT = 15; // Timeout in seconds

class SessionTimout extends Component {
  state = {
    timer: TIMEOUT,
  };

  componentDidUpdate(prevProps) {
    const { isOpen } = this.props;
    if (!prevProps.isOpen && isOpen) {
      this.startTimer();
    } else if (prevProps.isOpen && !isOpen) {
      this.stopTimer();
    }
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  startTimer() {
    this.timer = setInterval(() => {
      const { timer } = this.state;
      if (timer === 1) {
        Meteor.logout();
      }
      this.setState({ timer: timer - 1 });
    }, 1000);
  }

  stopTimer() {
    clearInterval(this.timer);
    this.setState({ timer: TIMEOUT });
  }

  render() {
    const { isOpen, onClose } = this.props;
    const { timer } = this.state;
    return (
      <Modal
        size="mini"
        dimmer="blurring"
        open={isOpen}
        onClose={onClose}
        closeOnDimmerClick={false}
      >
        <Modal.Header>Session Timeout</Modal.Header>
        <Modal.Content>
          <p>
            Your session timed out doe to inactivity.
            Please choose to stay signed in or to signed out,
            otherwise, you will be logged off automatically
          </p>
          <Divider hidden />
          <Container textAlign="center">
            <Button onClick={() => Meteor.logout()}>
              Sign out
            </Button>
            <Button color="green" onClick={onClose}>
              {`Stay signed in (${timer})`}
            </Button>
          </Container>
        </Modal.Content>
      </Modal>
    );
  }
}

SessionTimout.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

SessionTimout.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default SessionTimout;
