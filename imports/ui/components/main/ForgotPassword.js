import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base';
import { Formik } from 'formik';
import styled from 'styled-components';
import {
  Header, Divider, Form, Label, Button, Container, Message, Icon,
} from 'semantic-ui-react';

import { emailRegex } from '../../../lib/constants';

const StyledMessage = styled.div`
  margin-top: 15px;
`;

class ForgotPasswordForm extends Component {
  state = {
    error: '',
    sent: false,
  };

  // eslint-disable-next-line class-methods-use-this
  sendEmail({ email }) {
    return new Promise((resolve, reject) => {
      Accounts.forgotPassword({ email }, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  render() {
    const { error, sent } = this.state;
    return (
      <Formik
        initialValues={{ email: '' }}
        validate={(values) => {
          const errors = {};
          if (!values.email) {
            errors.email = 'Please enter your email';
          } else if (!emailRegex.test(values.email)) {
            errors.email = 'Please enter a valid email';
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={({ email }, { setSubmitting }) => {
          this.setState({ error: '' });
          this.sendEmail({ email })
            .then(() => {
              setSubmitting(false);
              this.setState({ sent: true });
            })
            .catch((err) => {
              const message = err.reason.toLowerCase() || 'please try again later';
              setSubmitting(false);
              this.setState({
                error: `Failed to send email: ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values, errors, touched, isValid, isSubmitting,
        }) => (
          <div>
            <Form onSubmit={handleSubmit}>
              <Header as="h3">
                Forgot Password
              </Header>
              <Divider hidden />
              <Form.Field>
                <label>
                  Your email address
                </label>
                <input
                  type="text"
                  name="email"
                  onChange={(e) => {
                    this.setState({ sent: false });
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  value={values.email}
                />
                {errors.email && touched.email && (
                  <Label pointing basic color="red">
                    {errors.email}
                  </Label>
                )}
              </Form.Field>
              <Container textAlign="center">
                <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">{sent ? 'Resend' : 'Continue'}</Button>
              </Container>
            </Form>
            <Container>
              <StyledMessage>
                <Message error hidden={!error}>
                  <Icon name="warning" />
                  {error}
                </Message>
              </StyledMessage>
              <StyledMessage>
                <Message success hidden={!sent}>
                  <Icon name="check" />
                  An email has been sent to you address. Follow the link
                  in the email to reset your password
                </Message>
              </StyledMessage>
            </Container>
          </div>
        )}
      />
    );
  }
}


export default ForgotPasswordForm;
