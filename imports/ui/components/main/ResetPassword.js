import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import ReactRouterPropTypes from 'react-router-prop-types';
import { Accounts } from 'meteor/accounts-base';
import { Formik } from 'formik';
import styled from 'styled-components';
import {
  Header, Divider, Form, Label, Button, Container, Message, Icon,
} from 'semantic-ui-react';

import { passwordRegex } from '../../../lib/constants';
import { sendPasswordChangeEmail } from '../../../api/email/methods';

const StyledMessage = styled.div`
  margin-top: 15px;
`;

class ResetPasswordForm extends Component {
  state = {
    error: '',
    changed: false,
  };

  resetPassword({ password }) {
    const { match: { params: { token } } } = this.props;
    return new Promise((resolve, reject) => {
      Accounts.resetPassword(token, password, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  render() {
    const { error, changed } = this.state;
    return (
      <Formik
        initialValues={{ password: '', repassword: '' }}
        validate={(values) => {
          const errors = {};
          if (!values.password) {
            errors.password = 'Please enter a password';
          } else if (!passwordRegex.test(values.password)) {
            errors.password = 'Must contain minimum eight characters, at least one uppercase letter, one lowercase letter and one number';
          }
          if (values.password !== values.repassword) {
            errors.repassword = 'Must match with password';
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={({ password }, { setSubmitting }) => {
          this.setState({ error: '' });
          this.resetPassword({ password })
            .then(() => sendPasswordChangeEmail.callPromise())
            .then(() => Meteor.logout())
            .then(() => setSubmitting(false))
            .then(() => this.setState({ changed: true }))
            .catch((err) => {
              const message = err.reason.toLowerCase() || 'please try again later';
              setSubmitting(false);
              this.setState({
                error: `Failed to reset password: ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values, errors, touched, isValid, isSubmitting,
        }) => (
          <div>
            <Form onSubmit={handleSubmit}>
              <Header as="h3">
                Reset Password
              </Header>
              <Divider hidden />
              <Form.Field>
                <label>
                  New Password
                </label>
                <input
                  type="password"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                {errors.password && touched.password && (
                  <Label pointing basic color="red">
                    {errors.password}
                  </Label>
                )}
              </Form.Field>
              <Form.Field>
                <label>
                  Repeat Password
                </label>
                <input
                  type="password"
                  name="repassword"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.repassword}
                />
                {errors.repassword && touched.repassword && (
                  <Label pointing basic color="red">
                    {errors.repassword}
                  </Label>
                )}
              </Form.Field>
              {changed ? null : (
                <Container textAlign="center">
                  <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">Change Password</Button>
                </Container>
              )}
            </Form>
            <Container>
              <StyledMessage>
                <Message error hidden={!error}>
                  <Icon name="warning" />
                  {error}
                </Message>
              </StyledMessage>
              <StyledMessage>
                <Message color="green" hidden={!changed}>
                  <Icon name="check" />
                  Your password was successfully changed, please
                  <Link to="/login"> login </Link>
                  now
                </Message>
              </StyledMessage>
            </Container>
          </div>
        )}
      />
    );
  }
}

ResetPasswordForm.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
};

export default withRouter(ResetPasswordForm);
