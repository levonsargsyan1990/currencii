import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import { Formik } from 'formik';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import findIndex from 'lodash/findIndex';
import find from 'lodash/find';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import styled from 'styled-components';
import { Dropdown, Flag } from 'semantic-ui-react';

import { Rates } from '../../../api/rates/rates';
import { numberRegex, currencyOptions } from '../../../lib/constants';
import { numberWithCommas } from '../../../lib/helpers';

const StyledDropdown = styled.div`
  display: inline-block;
  position: relative;
  top: 13px;
  width: 89px;
  height: 40px;
  border-radius: 4px;
  background-color: #f2eded;
  cursor: pointer;
  .ui.fluid.dropdown.label {
    display: flex;
    height: 100%;
    align-items: center;
  }
`;

class Calculator extends Component {
  state = {
    dropdown: [],
    dropdownContent: [],
    dropdownInitialized: false,
    error: '',
    isLoading: true,
    isVerifyModalOpen: false,
  };

  defaultValues = {
    marketRate: '',
    guaranteedRate: '',
    savedAmount: '',
  }

  values = {
    sellAmount: '',
    sellCurrency: 'usd',
    buyAmount: '',
    buyCurrency: 'eur',
    marketRate: 0,
    guaranteedRate: 0,
    savedAmount: 0,
  }

  componentDidMount() {
    const dropdown = document.getElementsByClassName('currency-dropdown');
    const dropdownContent = document.getElementsByClassName('currency-dropdown-content');
    this.setState({ dropdown, dropdownContent });
  }

  componentDidUpdate(prevProps) {
    const {
      sellCurrency,
      buyCurrency,
      marketRate,
      guaranteedRate,
    } = this.values;
    const { ratesLoading } = this.props;
    if (!ratesLoading && prevProps.ratesLoading) {
      if (!marketRate || !guaranteedRate) {
        this.updateMidAndGuaranteedRates({ sellCurrency, buyCurrency });
      }
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ isLoading: false });
    }
    const { dropdownInitialized, dropdown, dropdownContent } = this.state;
    if (!dropdownInitialized && dropdown.length > 0 && dropdownContent > 0) {
      this.setState({ dropdownInitialized: true });
      this.openDropdown();
    }
  }

  getMidAndGuaranteedRates({ sellCurrency, buyCurrency }) {
    const { rates } = this.props;
    const rate = find(rates, ({ sell, buy }) => ((sell.currency === sellCurrency)
      && (buy.currency === buyCurrency)));
    if (!rate) {
      throw new Meteor.Error(
        'rate-error',
        `We are not converting ${sellCurrency.toUpperCase()} to ${buyCurrency.toUpperCase()} yet`,
      );
    }
    const {
      guaranteedRate: {
        rate: guaranteedRate,
      },
      marketRate: {
        rate: marketRate,
      },
    } = rate;
    return { marketRate, guaranteedRate };
  }

  updateValues(changedValue, values) {
    const currentState = { ...values, ...changedValue };
    const {
      sellCurrency,
      buyCurrency,
      sellAmount,
      buyAmount,
    } = currentState;
    const {
      sellAmount: oldSellAmount,
      buyAmount: oldBuyAmount,
      sellCurrency: oldSellCurrency,
      buyCurrency: oldBuyCurrency,
    } = values;

    // if sell/buy currencies were changed,
    // get mid market and guaranteed rates are queried from database
    if (sellCurrency !== oldSellCurrency || buyCurrency !== oldBuyCurrency) {
      this.setState({ error: '' });
      const { error } = this.updateMidAndGuaranteedRates({ sellCurrency, buyCurrency });
      if (error) {
        return;
      }
    }
    const { marketRate, guaranteedRate } = this.values;
    const newState = { ...currentState, marketRate, guaranteedRate };

    // if the sell amount was changed
    if (sellAmount !== oldSellAmount) {
      if (!sellAmount) {
        newState.buyAmount = '';
      } else {
        newState.sellAmount = parseFloat(sellAmount, 10);
        newState.buyAmount = parseFloat((sellAmount * newState.guaranteedRate).toFixed(2), 10);
      }
    }

    // if the buy amount was changed
    if (buyAmount !== oldBuyAmount) {
      if (!buyAmount) {
        newState.sellAmount = '';
      } else {
        newState.buyAmount = parseFloat(buyAmount, 10);
        newState.sellAmount = parseFloat((buyAmount / newState.guaranteedRate).toFixed(2), 10);
      }
    }

    newState.savedAmount = parseFloat(((newState.sellAmount / newState.marketRate)
      - (newState.sellAmount / newState.guaranteedRate)).toFixed(2), 10);
    this.values = newState;
  }

  updateMidAndGuaranteedRates({ sellCurrency, buyCurrency }) {
    try {
      this.values = {
        ...this.values,
        ...this.getMidAndGuaranteedRates({ sellCurrency, buyCurrency }),
      };
      return { error: '', values: this.values };
    } catch (err) {
      const message = (err.error && err.error) === 'rate-error' ? err.reason : 'Failed to calculate. please try again later';
      this.values = {
        ...this.values, ...this.defaultValues, sellCurrency, buyCurrency,
      };
      this.setState({ error: message });
      return { error: message, values: null };
    }
  }

  handleVerifyModalToggle() {
    const { isVerifyModalOpen } = this.state;
    this.setState({ isVerifyModalOpen: !isVerifyModalOpen });
  }

  openDropdown() {
    const { dropdown, dropdownContent } = this.state;
    for (let i = 0; i < dropdown.length; i += 1) {
      const index = i;
      dropdown[index].addEventListener('click', (event) => {
        const dropContent = document.getElementsByClassName('currency-dropdown-content')[index];
        const shouldOpen = !dropContent.classList.contains('open-currency-dropdown-content');
        event.preventDefault();
        this.closeDropdown();

        if (shouldOpen) {
          dropdown[index].childNodes[1].childNodes[3].src = 'img/chevron-down-icon.png';
          dropContent.classList.add('open-currency-dropdown-content');
        }
        event.stopPropagation();
      });
    }
  }

  setSellCurrency(index) {
    const { dropdown } = this.state;
    dropdown[0].childNodes[1].childNodes[3].removeAttribute('value');
    dropdown[0].childNodes[1].childNodes[3].setAttribute('value', this.dropdown_content[0].childNodes[index].childNodes[3].getAttribute('value'));
    dropdown[0].childNodes[1].childNodes[1].src = dropdown_content[0].childNodes[index].childNodes[1].src;
    dropdown[0].childNodes[1].childNodes[3].textContent = dropdown_content[0].childNodes[index].childNodes[3].textContent;
  }

  setBuyCurrency(index) {
    const { dropdown } = this.state;
    dropdown[0].childNodes[1].childNodes[3].removeAttribute('value');
    dropdown[0].childNodes[1].childNodes[3].setAttribute('value', dropdown_content[0].childNodes[index].childNodes[3].getAttribute('value'));
    dropdown[0].childNodes[1].childNodes[1].src = dropdown_content[0].childNodes[index].childNodes[1].src;
    dropdown[0].childNodes[1].childNodes[3].textContent = dropdown_content[0].childNodes[index].childNodes[3].textContent;
  }

  closeDropdown() {
    const { dropdown } = this.state;
    const currenctDropdown = document.getElementsByClassName('currency-dropdown-content');
    for (let i = 0; i < currenctDropdown.length; i += 1) {
      const opened = currenctDropdown[i].classList.contains('open-currency-dropdown-content');
      if (opened) {
        dropdown[i].childNodes[1].childNodes[3].src = 'img/dropdown.png';
        currenctDropdown[i].classList.remove('open-currency-dropdown-content')
      }
    }
  }

  render () {
    const { isLoading, error } = this.state;
    const {
      ratesLoading,
      history,
    } = this.props;

    return !isLoading && !ratesLoading ? (
      <Formik
        initialValues={this.values}
        validate={(values) => {
          const errors = {};
          if (!values.sellAmount) {
            errors.sellAmountHidden = 'Please enter the amount of money you want to sell';
          } else if (values.sellAmount <= 0) {
            errors.sellAmount = 'The amount should be more then zero';
          } else if (!numberRegex.test(values.sellAmount)) {
            errors.sellAmount = 'The amount can only be a number';
          }
          if (!values.sellCurrency) {
            errors.sellCurrency = 'Please select the currency';
          } else if (values.buyAmount <= 0) {
            errors.buyAmountHidden = 'The amount should be more then zero';
          } else if (findIndex(currencyOptions, { value: values.sellCurrency }) === -1) {
            errors.sellCurrency = 'We only provide service in currencies from the dropdown list';
          }
          if (!values.buyAmount) {
            errors.buyAmountHidden = 'Please enter the amount of money you want to buy';
          } else if (!numberRegex.test(values.buyAmount)) {
            errors.buyAmount = 'The amount can only be a number';
          }
          if (!values.buyCurrency) {
            errors.buyCurrency = 'Please select the currency';
          } else if (findIndex(currencyOptions, { value: values.buyCurrency }) === -1) {
            errors.buyCurrency = 'We only provide service in currencies from the dropdown list';
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={(values, { setSubmitting }) => {
          history.push(`/exchange?sa=${values.sellAmount}&ba=${values.buyAmount}&sc=usd&bc=eur`);
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values, errors,
          touched, isValid, isSubmitting, setTouched, setValues,
        }) => (
          <form
            className="calc-main"
            onSubmit={handleSubmit}
            autoComplete="off"
          >
            <div className="calc-inputs">
              <div className="calc-input">
                <div>
                  <label>You sell</label>
                  <NumberFormat
                    name="sellAmount"
                    thousandSeparator
                    onKeyPress={e => e.key === 'Enter' && e.preventDefault()}
                    isAllowed={({ value }) => {
                      if (parseFloat(value, 10) === 0) {
                        return false;
                      }
                      return true;
                    }}
                    onChange={(event) => {
                      handleChange(event);
                      const value = event.target.value.replace(/,/g, '');
                      this.updateValues({ sellAmount: value }, values);
                      setValues(this.values);
                    }}
                    onBlur={handleBlur}
                    value={values.sellAmount}
                  />
                </div>
                <StyledDropdown>
                  <Dropdown
                    scrolling
                    trigger={(
                      <>
                        <Flag name={find(currencyOptions, { value: values.sellCurrency }).flag} />
                        <div className="text">{values.sellCurrency.toUpperCase()}</div>
                      </>
                    )}
                    fluid
                    className="label"
                    options={currencyOptions}
                    onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                    onChange={(event, data) => {
                      setTouched({ sellCurrency: true });
                      handleChange({ ...event, target: { name: 'sellCurrency', value: data.value } });
                      this.updateValues({ sellCurrency: data.value }, values);
                      setValues(this.values);
                    }}
                    onBlur={(event) => {
                      handleBlur({ ...event, target: { name: 'sellCurrency' } });
                    }}
                    name="sellCurrency"
                    value={values.sellCurrency}
                  />
                </StyledDropdown>
              </div>
              <div className="calc-input">
                <div>
                  <label>You buy</label>
                  <NumberFormat
                    name="buyAmount"
                    thousandSeparator
                    onKeyPress={e => e.key === 'Enter' && e.preventDefault()}
                    isAllowed={({ value }) => {
                      if (parseFloat(value, 10) === 0) {
                        return false;
                      }
                      return true;
                    }}
                    onChange={(event) => {
                      handleChange(event);
                      const value = event.target.value.replace(/,/g, '');
                      this.updateValues({ buyAmount: value }, values);
                      setValues(this.values);
                    }}
                    onBlur={handleBlur}
                    value={values.buyAmount}
                  />
                </div>
                <StyledDropdown>
                  <Dropdown
                    scrolling
                    trigger={(
                      <>
                        <Flag name={find(currencyOptions, { value: values.buyCurrency }).flag} />
                        <div className="text">{values.buyCurrency.toUpperCase()}</div>
                      </>
                    )}
                    fluid
                    className="label"
                    options={currencyOptions}
                    onFocus={e => e.target.setAttribute('autocomplete', 'nope')}
                    onChange={(event, data) => {
                      setTouched({ buyCurrency: true });
                      handleChange({ ...event, target: { name: 'buyCurrency', value: data.value } });
                      this.updateValues({ buyCurrency: data.value }, values);
                      setValues(this.values);
                    }}
                    onBlur={(event) => {
                      handleBlur({ ...event, target: { name: 'buyCurrency' } });
                    }}
                    name="buyCurrency"
                    value={values.buyCurrency}
                  />
                </StyledDropdown>
              </div>
            </div>

            <div className="curent-rate">
              <div className="market-rate">
                <div className="market-rate-text">Market rate:</div>
                <div className="market-rate-chip">{values.marketRate}</div>
              </div>
              <div className="market-rate">
                <div className="market-rate-text">Your guaranteed rate (24 hours)</div>
                <div className="market-rate-chip">{values.guaranteedRate}</div>
              </div>
            </div>
            <div className="save-up">
              <img src="img/landing/hey.png" />
              <div className="save-up-text">You save up to<span> {typeof values.savedAmount === 'number' && !isNaN(values.savedAmount) ? ` ${values.sellCurrency.toUpperCase()} ${numberWithCommas(values.savedAmount)}` : ''}</span></div>
            </div>
            <div className="proceed">
              <button type="submit" className="proceed-button btn">PROCEED</button>
            </div>
          </form>
        )}
      />
    ) : null;
  }
}

Calculator.propTypes = {
  ratesLoading: PropTypes.bool.isRequired,
  rates: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    sell: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    buy: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    guaranteedRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
    marketRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
    active: PropTypes.bool.isRequired,
  })),
};

Calculator.defaultProps = {
  rates: null,
};

const mapDataToProps = () => {
  const ratesHandle = Meteor.subscribe('rates.listActive');
  const ratesLoading = !ratesHandle.ready();
  const rates = Rates.find().fetch();
  return { ratesLoading, rates };
};

export default withRouter(withTracker(mapDataToProps)(Calculator));
