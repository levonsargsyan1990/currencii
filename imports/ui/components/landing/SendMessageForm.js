import React from 'react';
import { Formik } from 'formik';


import { sendContactEmail } from '../../../api/email/methods';
import { nameRegex, emailRegex } from '../../../lib/constants';

const SendMessageForm = () => {
  return (
    <Formik
      initialValues={{ name: '', email: '', message: '' }}
      validate={(values) => {
        const errors = {};
        if (!values.name) {
          errors.firstName = 'Please enter your name';
        } else if (!nameRegex.test(values.name)) {
          errors.name = 'Please enter a valid name';
        }
        if (!values.email) {
          errors.firstName = 'Please enter your email';
        } else if (!emailRegex.test(values.email)) {
          errors.email = 'Please enter a valid email';
        }
        if (!values.message) {
          errors.message = 'Please enter your message';
        }
        return errors;
      }}
      isInitialValid={({ validate, initialValues }) => {
        const errors = validate(initialValues);
        if (Object.keys(errors).length > 0) {
          return false;
        }
        return true;
      }}
      onSubmit={(values, { setSubmitting, setValues }) => {
        sendContactEmail.callPromise(values)
          .then(() => {
            setValues({ email: '', name: '', message: '' });
            setSubmitting(false);
          })
          .catch(() => {
            setSubmitting(false);
          });
      }}
      render={({
        handleSubmit, handleChange, handleBlur, values,
        isValid, isSubmitting,
      }) => (
        <form onSubmit={handleSubmit}>
          <div className="user-info">
            <input
              className="input1"
              placeholder="Name"
              type="text"
              name="name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.name}
            />
            <input
              className="input2"
              placeholder="Email"
              type="text"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
          </div>
          <div className="user-message">
            <textarea
              placeholder="Your Message"
              type="text"
              name="message"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.message}
            />
            <div className="submit-button">
              <button disabled={!isValid || isSubmitting} type="submit" className="submit-message btn">SUBMIT</button>
            </div>
          </div>
        </form>
      )}
    />
  );
};

export default SendMessageForm;
