import React from 'react';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Header, Container, Menu,
} from 'semantic-ui-react';

const StyledHeader = styled.div`
  .pointing.menu {
    z-index: 110;
    background-color: ${props => props.theme.teal};
    .header, 
    .text, 
    .dropdown.icon {
      color: #fff;
    }
  }
`;

const AdminHeader = () => (
  <StyledHeader>
    <Menu
      fixed="top"
      size="huge"
      pointing
      secondary
    >
      <Container>
        <Menu.Item>
          <Link to="/">
            <Header as="h3">
              Currencii
            </Header>
          </Link>
        </Menu.Item>
      </Container>
    </Menu>
  </StyledHeader>
);

export default withTracker(() => ({ user: Meteor.user() }))(AdminHeader);
