import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Table, Grid } from 'semantic-ui-react';

import UserImage from './UserImage';

const StyledGrid = styled.div`
  .column:nth-child(even) {
    box-shadow: none !important;
  }
`;

const UserDetail = ({
  user: {
    _id,
    profile: {
      verification: {
        business, identity,
      },
    },
  },
}) => {
  const isIdentityInProgress = (identity.status === 'verified') && identity.fileId;
  const isBusinessInProgress = (business.status === 'verified') && business.fileId;
  return (
    <Table.Row key={`${_id}-detail`}>
      <Table.Cell colSpan={9}>
        <StyledGrid>
          <Grid verticalAlign="middle" columns={4} divided>
            <Grid.Row>
              {isIdentityInProgress
                ? (
                  <UserImage
                    fileId={identity.fileId}
                    type="identity"
                    verifiedAt={identity.verifiedAt}
                  />
                ) : null}
              {isBusinessInProgress
                ? (
                  <UserImage
                    fileId={business.fileId}
                    type="business"
                    verifiedAt={business.verifiedAt}
                  />) : null}
            </Grid.Row>
          </Grid>
        </StyledGrid>
      </Table.Cell>
    </Table.Row>
  );
};

UserDetail.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    profile: PropTypes.shape({
      verification: PropTypes.shape({
        business: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
        identity: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
      }),
    }),
  }).isRequired,
};

export default UserDetail;
