import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Grid, Image, Loader, Modal, Message, Icon,
} from 'semantic-ui-react';

import { capitalize } from '../../../../lib/helpers';

import { Images } from '../../../../api/images/images';

const StyledImage = styled.div`
  img {
    cursor: pointer;
    width: 100%;
  }
`;

const StyledError = styled.div`
  margin-top: 15px;
  text-align: left;
`;

class UserImage extends Component {
  state= {
    error: '',
    isModalOpen: false,
  }

  handleModalToggle() {
    const { isModalOpen } = this.state;
    this.setState({ isModalOpen: !isModalOpen });
  }

  render() {
    const {
      isModalOpen, error,
    } = this.state;
    const {
      type, image, loading, verifiedAt,
    } = this.props;
    if (loading) {
      return (
        <Grid.Column>
          <Loader />
        </Grid.Column>
      );
    }

    return (
      <>
        <Grid.Column>
          <StyledImage>
            <Image
              src={image.url()}
              alt={`${capitalize(type)} verification`}
              onClick={() => this.handleModalToggle()}
            />
          </StyledImage>
        </Grid.Column>
        <Grid.Column verticalAlign="middle">
          <p>
            {`${capitalize(type)} verification`}
          </p>
          <p>
            {`Verified at: ${moment(verifiedAt).format('DD/MM/YYYY, hh:mm')}`}
          </p>
          <StyledError>
            <Message error hidden={!error}>
              <Icon name="warning" />
              {error}
            </Message>
          </StyledError>
        </Grid.Column>

        <Modal
          size="large"
          dimmer="blurring"
          open={isModalOpen}
          closeIcon
          closeOnDimmerClick={false}
          onClose={() => this.handleModalToggle()}
        >
          <Modal.Header>
            {`${capitalize(type)} verification (${moment(verifiedAt).format('DD/MM/YYYY, hh:mm')})`}
          </Modal.Header>
          <Modal.Content>
            <StyledImage>
              <Image
                src={image.url()}
                alt={`${capitalize(type)} verification`}
              />
            </StyledImage>
          </Modal.Content>
        </Modal>
      </>
    );
  }
}

UserImage.propTypes = {
  image: PropTypes.shape({}),
  loading: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  verifiedAt: PropTypes.instanceOf(Date).isRequired,
};

UserImage.defaultProps = {
  image: null,
};

const mapDataToProps = ({ fileId }) => {
  const imagesHandle = Meteor.subscribe('images.one', { fileId });
  const imagesLoading = !imagesHandle.ready();
  const image = Images.findOne({ _id: fileId });
  return { loading: imagesLoading, image };
};

export default withTracker(mapDataToProps)(UserImage);
