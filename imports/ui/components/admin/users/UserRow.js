import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, Icon } from 'semantic-ui-react';

import UserDetail from './UserDetail';

class UserRow extends Component {
  state = {
    isOpen: false,
  };

  handleRowClick() {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  }

  render () {
    const { isOpen } = this.state;
    const { user } = this.props;
    const {
      _id,
      emails,
      profile: {
        firstName,
        lastName,
        companyName,
        position,
        address,
        city,
        state,
        country,
        phoneCode,
        phoneNumber,
        verification,
      },
    } = user;
    const isIdentityVerified = verification.identity.status === 'verified';
    const isBusinessVerified = verification.business.status === 'verified';
    let identityVerifiedIcon = <Icon name="delete" color="red" />;
    if (isIdentityVerified) {
      identityVerifiedIcon = <Icon name="check" color="green" />;
    }
    let businessVerifiedIcon = <Icon name="delete" color="red" />;
    if (isBusinessVerified) {
      businessVerifiedIcon = <Icon name="check" color="green" />;
    }
    return (
      <>
        <Table.Row
          style={{ cursor: (isIdentityVerified || isBusinessVerified) ? 'pointer' : 'default' }}
          key={_id}
          onClick={() => {
            if (isIdentityVerified || isBusinessVerified) {
              this.handleRowClick();
            }
          }}
        >
          <Table.Cell>{`${_id}`}</Table.Cell>
          <Table.Cell>{`${firstName} ${lastName}`}</Table.Cell>
          <Table.Cell>{companyName}</Table.Cell>
          <Table.Cell>{position}</Table.Cell>
          <Table.Cell>
            {`${address}, ${city}, ${state ? `${state}, ` : ''}, ${country}`}
          </Table.Cell>
          <Table.Cell>{`(${phoneCode}) ${phoneNumber}`}</Table.Cell>
          <Table.Cell>{emails[0].address}</Table.Cell>
          <Table.Cell textAlign="center">
            {identityVerifiedIcon}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {businessVerifiedIcon}
          </Table.Cell>
        </Table.Row>
        {isOpen ? <UserDetail user={user} /> : null}
      </>
    );
  }
}

UserRow.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    emails: PropTypes.arrayOf(PropTypes.shape({
      address: PropTypes.string.isRequired,
    })),
    profile: PropTypes.shape({
      companyName: PropTypes.string.isRequired,
      country: PropTypes.string.isRequired,
      city: PropTypes.string.isRequired,
      state: PropTypes.string,
      address: PropTypes.string.isRequired,
      postalCode: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      position: PropTypes.string.isRequired,
      phoneCode: PropTypes.string.isRequired,
      phoneNumber: PropTypes.string.isRequired,
      verification: PropTypes.shape({
        business: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
        identity: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
      }),
    }),
  }).isRequired,
};

export default UserRow;
