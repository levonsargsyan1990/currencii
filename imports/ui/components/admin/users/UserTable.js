import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Roles } from 'meteor/alanning:roles';
import { Table, Loader, Segment } from 'semantic-ui-react';

import UserRow from './UserRow';

const UserTable = ({ users, loading }) => {
  if (loading) {
    return <Loader size="large" />;
  }
  const userRows = users.map(user => (
    <UserRow key={user._id} user={user} />
  ));
  if (users.length === 0) {
    return <p>No users found</p>;
  }
  return (
    <Segment>
      <Table basic="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Company</Table.HeaderCell>
            <Table.HeaderCell>Position</Table.HeaderCell>
            <Table.HeaderCell>Address</Table.HeaderCell>
            <Table.HeaderCell>Phone</Table.HeaderCell>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">Identity Verified</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">Business Verified</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {userRows}
        </Table.Body>
      </Table>
    </Segment>
  );
};

UserTable.propTypes = {
  loading: PropTypes.bool,
  users: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    emails: PropTypes.arrayOf(PropTypes.shape({
      address: PropTypes.string.isRequired,
    })),
    profile: PropTypes.shape({
      companyName: PropTypes.string.isRequired,
      country: PropTypes.string.isRequired,
      city: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
      postalCode: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      position: PropTypes.string.isRequired,
      phoneCode: PropTypes.string.isRequired,
      phoneNumber: PropTypes.string.isRequired,
      verification: PropTypes.shape({
        business: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
        identity: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
      }),
    }),
  })),
};

UserTable.defaultProps = {
  loading: true,
  users: [],
};

const mapDataToProps = () => {
  const userId = Meteor.userId();
  if (userId) {
    const userHandle = Meteor.subscribe('users.list');
    const usersLoading = !userHandle.ready();
    const users = Roles.getUsersInRole('client', Roles.GLOBAL_GROUP).fetch();
    return { loading: usersLoading, users };
  }
  return {};
};

export default withTracker(mapDataToProps)(UserTable);
