import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Formik } from 'formik';
import {
  Modal, Message, Form, Icon, Table,
  Container, Button, Label, Divider,
} from 'semantic-ui-react';
import { DateTimeInput } from 'semantic-ui-calendar-react';
import styled from 'styled-components';

import { getActiveRate } from '../../../../api/rates/methods';
import { receiveTransaction } from '../../../../api/transactions/methods';
import { sendTransactionReceivedEmail } from '../../../../api/email/methods';
import { numberWithCommas } from '../../../../lib/helpers';

const GUARANTEED_HOURS = 24;

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

const StyledInput = styled.div`
  .input {
    display: flex;
  }
`;

const BoldText = styled.p`
  font-weight: 700;
`;

class ReceiveTransactionModal extends Component {
  constructor(props) {
    super(props);
    const { transaction: { guaranteedRate, buy: { amount } } } = props;
    this.state = {
      rate: guaranteedRate,
      currentRate: guaranteedRate,
      buyAmount: amount,
      error: '',
    };
  }

  componentDidMount() {
    const { transaction: { sell, buy } } = this.props;
    getActiveRate
      .callPromise({ sell: sell.currency, buy: buy.currency })
      .then(({ guaranteedRate: { rate } }) => this.setState({ currentRate: rate }))
      .catch(() => (
        this.setState({
          error: 'Failed to get current rate: try refreshing the page',
        })
      ));
  }

  handleDateTimeChange(timeDate) {
    const {
      transaction: {
        createdAt, guaranteedRate, sell, buy,
      },
    } = this.props;
    const { currentRate } = this.state;
    const createdMoment = moment(createdAt);
    const receivedMoment = moment(timeDate);
    const hours = moment
      .duration(receivedMoment.diff(createdMoment))
      .asHours();
    if (hours > GUARANTEED_HOURS) {
      const buyAmount = parseFloat((sell.amount * currentRate).toFixed(2));
      this.setState({
        rate: currentRate,
        buyAmount,
      });
    } else {
      this.setState({
        rate: guaranteedRate,
        buyAmount: buy.amount,
      });
    }
  }

  render() {
    const {
      transaction, isOpen, onClose,
    } = this.props;
    const {
      sell, buy, marketRate, userId,
      guaranteedRate, createdAt,
    } = transaction;
    const { error, rate, buyAmount } = this.state;
    const savedAmount = sell.amount * (guaranteedRate - marketRate);

    return (
      <Formik
        initialValues={{ date: '' }}
        validateOnBlur={false}
        validate={(values) => {
          const errors = {};
          if (!values.date) {
            errors.date = 'Please enter the date when money has been received';
          } else if (!moment(values.date).isValid()) {
            errors.date = 'Please enter a valid date or select one';
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={({ date }, { setSubmitting }) => {
          const { transaction: { _id } } = this.props;
          const receivedAt = moment(date).toDate();
          receiveTransaction.callPromise({
            transactionId: _id, rate, receivedAt, buyAmount,
          })
            .then(() => sendTransactionReceivedEmail.callPromise({ transactionId: _id, userId }))
            .catch((err) => {
              const message = err.reason.toLowerCase() || 'please try again later';
              setSubmitting(false);
              this.setState({
                error: `Failed to update transaction state: ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values,
          errors, touched, isValid, isSubmitting,
        }) => (
          <Modal
            size="tiny"
            closeIcon
            dimmer="blurring"
            open={isOpen}
            onClose={onClose}
            closeOnDimmerClick={false}
          >
            <Modal.Header>Receive</Modal.Header>
            <Modal.Content>
              <Form onSubmit={handleSubmit} autoComplete="off">
                <Form.Field>
                  <label>
                    Receving Time
                  </label>
                  <StyledInput>
                    <DateTimeInput
                      name="date"
                      closable
                      placeholder="Select the date and time when payment has been received"
                      iconPosition="left"
                      dateTimeFormat="YYYY-MM-DD HH:MM"
                      popupPosition="bottom center"
                      maxDate={new Date()}
                      minDate={createdAt}
                      value={values.date}
                      onBlur={handleBlur}
                      onChange={(event, target) => {
                        this.handleDateTimeChange(target.value);
                        handleChange({ ...event, target, type: 'change' });
                      }}
                    />
                    {errors.date && touched.date && (
                      <Label pointing basic color="red">
                        {errors.date}
                      </Label>
                    )}
                  </StyledInput>
                </Form.Field>
                <Divider hidden />
                <StyledTable>
                  <Table stackable basic="very">
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>
                          <BoldText>Rate</BoldText>
                        </Table.Cell>
                        <Table.Cell>
                          <BoldText>{rate}</BoldText>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Sell</Table.Cell>
                        <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(sell.amount.toFixed(2))}`}</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Buy</Table.Cell>
                        <Table.Cell>{`${buy.currency.toUpperCase()} ${numberWithCommas(buyAmount.toFixed(2))}`}</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Mid market rate</Table.Cell>
                        <Table.Cell>{marketRate}</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Guaranteed rate (24 hours)</Table.Cell>
                        <Table.Cell>{guaranteedRate}</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Saved up to</Table.Cell>
                        <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(savedAmount.toFixed(2))}`}</Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </StyledTable>
                <Divider hidden />
                <Container textAlign="center">
                  <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">Submit</Button>
                </Container>
              </Form>
            </Modal.Content>
            <StyledError>
              <Message attached="bottom" error hidden={!error}>
                <Icon name="warning" />
                {error}
              </Message>
            </StyledError>
          </Modal>
        )}
      />
    );
  }
}

ReceiveTransactionModal.propTypes = {
  transaction: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    createdAt: PropTypes.instanceOf(Date),
  }).isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

ReceiveTransactionModal.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default ReceiveTransactionModal;
