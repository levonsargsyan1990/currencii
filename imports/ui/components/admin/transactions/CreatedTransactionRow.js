import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import moment from 'moment';
import { Table, Loader, Button } from 'semantic-ui-react';

import { numberWithCommas } from '../../../../lib/helpers';
import ReceiveTransactionModal from './ReceiveTransactionModal';

class CreatedTransactionRow extends Component {
  state = {
    isReceiveModalOpen: false,
  };

  handleReceiveModalToggle() {
    const { isReceiveModalOpen } = this.state;
    this.setState({ isReceiveModalOpen: !isReceiveModalOpen });
  }

  render() {
    const {
      transaction, user, userLoading,
    } = this.props;
    const { isReceiveModalOpen } = this.state;
    const {
      _id, reference, sell, buy, marketRate, guaranteedRate, createdAt,
    } = transaction;
    if (userLoading) {
      return (
        <Table.Row key={_id}>
          <Table.Cell colSpan={7}>
            <Loader />
          </Table.Cell>
        </Table.Row>
      );
    }
    const savedAmount = sell.amount * (guaranteedRate - marketRate);
    const { firstName, lastName } = user.profile;
    const name = `${firstName} ${lastName}`;
    return (
      <Table.Row key={_id}>
        <Table.Cell>
          {`${reference}`}
        </Table.Cell>
        <Table.Cell>
          {`${name}`}
        </Table.Cell>
        <Table.Cell>
          {`${sell.currency.toUpperCase()} ${numberWithCommas(sell.amount.toFixed(2))}`}
        </Table.Cell>
        <Table.Cell>
          {`${buy.currency.toUpperCase()} ${numberWithCommas(buy.amount.toFixed(2))}`}
        </Table.Cell>
        <Table.Cell>{moment(createdAt).format('DD/MM/YYYY, hh:mm')}</Table.Cell>
        <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(savedAmount.toFixed(2))}`}</Table.Cell>
        <Table.Cell collapsing>
          <Button
            size="mini"
            basic
            primary
            onClick={() => this.handleReceiveModalToggle()}
          >
            Receive
          </Button>
          <ReceiveTransactionModal
            transaction={transaction}
            onClose={() => this.handleReceiveModalToggle()}
            isOpen={isReceiveModalOpen}
          />
        </Table.Cell>
      </Table.Row>
    );
  }
}

CreatedTransactionRow.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    profile: PropTypes.shape({
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
    }).isRequired,
  }),
  userLoading: PropTypes.bool.isRequired,
  transaction: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    createdAt: PropTypes.instanceOf(Date),
  }).isRequired,
};

CreatedTransactionRow.defaultProps = {
  user: null,
};

const mapDataToProps = ({ transaction: { userId } }) => {
  if (userId) {
    const userHandle = Meteor.subscribe('users.findById', { userId });
    const userLoading = !userHandle.ready();
    const user = Meteor.users.findOne(userId);
    return { userLoading, user };
  }
  return {};
};

export default withTracker(mapDataToProps)(CreatedTransactionRow);
