import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Formik } from 'formik';
import {
  Modal, Message, Form, Icon, Table,
  Container, Button, Label, Divider,
} from 'semantic-ui-react';
import { DateTimeInput } from 'semantic-ui-calendar-react';
import styled from 'styled-components';

import { sendTransaction } from '../../../../api/transactions/methods';
import { sendTransactionSentEmail } from '../../../../api/email/methods';
import { numberWithCommas } from '../../../../lib/helpers';

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

const StyledInput = styled.div`
  .input {
    display: flex;
  }
`;

const BoldText = styled.p`
  font-weight: 700;
`;

class SendTransactionModal extends Component {
  state = {
    error: '',
  };

  render() {
    const {
      transaction, isOpen, onClose,
    } = this.props;
    const {
      sell, buy, createdAt, receivedAt, userId, rate,
    } = transaction;
    const { error } = this.state;

    return (
      <Formik
        initialValues={{ date: '' }}
        validateOnBlur={false}
        validate={(values) => {
          const errors = {};
          if (!values.date) {
            errors.date = 'Please enter the date when money has been received';
          } else if (!moment(values.date).isValid()) {
            errors.date = 'Please enter a valid date or select one';
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={({ date }, { setSubmitting }) => {
          const { transaction: { _id } } = this.props;
          const sentAt = moment(date).toDate();
          sendTransaction.callPromise({
            transactionId: _id, sentAt,
          })
            .then(() => sendTransactionSentEmail.callPromise({ transactionId: _id, userId }))
            .catch((err) => {
              const message = err.reason.toLowerCase() || 'please try again later';
              setSubmitting(false);
              this.setState({
                error: `Failed to login: ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values,
          errors, touched, isValid, isSubmitting,
        }) => (
          <Modal
            size="tiny"
            closeIcon
            dimmer="blurring"
            open={isOpen}
            onClose={onClose}
            closeOnDimmerClick={false}
          >
            <Modal.Header>Send</Modal.Header>
            <Modal.Content>
              <Form onSubmit={handleSubmit} autoComplete="off">
                <Form.Field>
                  <label>
                    Sending Time
                  </label>
                  <StyledInput>
                    <DateTimeInput
                      name="date"
                      closable
                      placeholder="Select the date and time when payment has been sent to user"
                      iconPosition="left"
                      dateTimeFormat="YYYY-MM-DD HH:MM"
                      popupPosition="bottom center"
                      maxDate={new Date()}
                      minDate={receivedAt}
                      value={values.date}
                      onBlur={handleBlur}
                      onChange={(event, target) => handleChange({ ...event, target, type: 'change' })}
                    />
                    {errors.date && touched.date && (
                      <Label pointing basic color="red">
                        {errors.date}
                      </Label>
                    )}
                  </StyledInput>
                </Form.Field>
                <Divider hidden />
                <StyledTable>
                  <Table stackable basic="very">
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>
                          <BoldText>Rate</BoldText>
                        </Table.Cell>
                        <Table.Cell>
                          <BoldText>{rate}</BoldText>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Sell</Table.Cell>
                        <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(sell.amount.toFixed(2))}`}</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Buy</Table.Cell>
                        <Table.Cell>{`${buy.currency.toUpperCase()} ${numberWithCommas(buy.amount.toFixed(2))}`}</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Created At</Table.Cell>
                        <Table.Cell>{`${moment(createdAt).format('DD/MM/YYYY, hh:mm')}`}</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Received At</Table.Cell>
                        <Table.Cell>{`${moment(receivedAt).format('DD/MM/YYYY, hh:mm')}`}</Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </StyledTable>
                <Divider hidden />
                <Container textAlign="center">
                  <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">Submit</Button>
                </Container>
              </Form>
            </Modal.Content>
            <StyledError>
              <Message attached="bottom" error hidden={!error}>
                <Icon name="warning" />
                {error}
              </Message>
            </StyledError>
          </Modal>
        )}
      />
    );
  }
}

SendTransactionModal.propTypes = {
  transaction: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    createdAt: PropTypes.instanceOf(Date),
  }).isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

SendTransactionModal.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default SendTransactionModal;
