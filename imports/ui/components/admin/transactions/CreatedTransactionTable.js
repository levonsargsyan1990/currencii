import React from 'react';
import PropTypes from 'prop-types';
import { Table, Loader } from 'semantic-ui-react';

import CreatedTransactionRow from './CreatedTransactionRow';

const CreatedTransactionTable = ({ transactions, loading }) => {
  if (loading) {
    return <Loader size="large" />;
  }
  const transactionRows = transactions.map(transaction => (
    <CreatedTransactionRow key={transaction._id} transaction={transaction} />
  ));
  if (transactionRows.length === 0) {
    return <p>No new transactions</p>;
  }
  return (
    <Table basic="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Reference #</Table.HeaderCell>
          <Table.HeaderCell>User</Table.HeaderCell>
          <Table.HeaderCell>Sell</Table.HeaderCell>
          <Table.HeaderCell>Buy</Table.HeaderCell>
          <Table.HeaderCell>Created At</Table.HeaderCell>
          <Table.HeaderCell>Saved</Table.HeaderCell>
          <Table.HeaderCell />
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {transactionRows}
      </Table.Body>
    </Table>
  );
};

CreatedTransactionTable.propTypes = {
  loading: PropTypes.bool,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  })),
};

CreatedTransactionTable.defaultProps = {
  loading: true,
  transactions: [],
};

export default CreatedTransactionTable;
