import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Container, Header, Segment,
} from 'semantic-ui-react';

import CreatedTransactionTable from './CreatedTransactionTable';
import { Transactions } from '../../../../api/transactions/transactions';

const StyledSegment = styled.div`
  &>.segment {
    max-height: 250px;
    overflow: auto;
  }
`;

const ExecutedTransactions = ({ transactions, transactionsLoading }) => (
  <Container>
    <Header as="h3">
      Created Transactions
      <Header.Subheader>
        This transactions have been created by client. They should now make the payment.
        As soon as the payment is received, the transaction can be marked as received.
      </Header.Subheader>
    </Header>
    <StyledSegment>
      <Segment>
        <CreatedTransactionTable
          transactions={transactions}
          loading={transactionsLoading}
        />
      </Segment>
    </StyledSegment>
  </Container>
);

ExecutedTransactions.propTypes = {
  transactionsLoading: PropTypes.bool,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  })),
};

ExecutedTransactions.defaultProps = {
  transactions: [],
  transactionsLoading: true,
};

const mapDataToProps = () => {
  const userId = Meteor.userId();
  if (userId) {
    const transactionHandle = Meteor.subscribe('transactions.all.created', { userId });
    const transactionsLoading = !transactionHandle.ready();
    const transactions = Transactions.find({
      status: 'created',
    }).fetch();
    return { transactionsLoading, transactions };
  }
  return {};
};

export default withTracker(mapDataToProps)(ExecutedTransactions);
