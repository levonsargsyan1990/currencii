import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import moment from 'moment';
import { Table, Loader } from 'semantic-ui-react';

import { numberWithCommas } from '../../../../lib/helpers';

const SentTransactionRow = ({ transaction, user, userLoading }) => {
  const {
    _id, reference, sell, buy, marketRate, guaranteedRate, createdAt, receivedAt, sentAt,
  } = transaction;
  if (userLoading) {
    return (
      <Table.Row key={_id}>
        <Table.Cell colSpan={7}>
          <Loader />
        </Table.Cell>
      </Table.Row>
    );
  }
  const savedAmount = sell.amount * (guaranteedRate - marketRate);
  const { firstName, lastName } = user.profile;
  const name = `${firstName} ${lastName}`;
  return (
    <Table.Row key={_id}>
      <Table.Cell>
        {`${reference}`}
      </Table.Cell>
      <Table.Cell>
        {`${name}`}
      </Table.Cell>
      <Table.Cell>
        {`${sell.currency.toUpperCase()} ${numberWithCommas(sell.amount.toFixed(2))}`}
      </Table.Cell>
      <Table.Cell>
        {`${buy.currency.toUpperCase()} ${numberWithCommas(buy.amount.toFixed(2))}`}
      </Table.Cell>
      <Table.Cell>{createdAt ? moment(createdAt).format('DD/MM/YYYY') : ''}</Table.Cell>
      <Table.Cell>{receivedAt ? moment(receivedAt).format('DD/MM/YYYY') : ''}</Table.Cell>
      <Table.Cell>{sentAt ? moment(sentAt).format('DD/MM/YYYY') : ''}</Table.Cell>
      <Table.Cell>{`${sell.currency.toUpperCase()} ${numberWithCommas(savedAmount.toFixed(2))}`}</Table.Cell>
    </Table.Row>
  );
};

SentTransactionRow.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    profile: PropTypes.shape({
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
    }).isRequired,
  }),
  userLoading: PropTypes.bool.isRequired,
  transaction: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  }).isRequired,
};

SentTransactionRow.defaultProps = {
  user: null,
};

const mapDataToProps = ({ transaction: { userId } }) => {
  if (userId) {
    const userHandle = Meteor.subscribe('users.findById', { userId });
    const userLoading = !userHandle.ready();
    const user = Meteor.users.findOne(userId);
    return { userLoading, user };
  }
  return {};
};

export default withTracker(mapDataToProps)(SentTransactionRow);
