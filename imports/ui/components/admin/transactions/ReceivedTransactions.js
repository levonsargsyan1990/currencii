import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Container, Header, Segment,
} from 'semantic-ui-react';

import ReceivedTransactionTable from './ReceivedTransactionTable';
import { Transactions } from '../../../../api/transactions/transactions';

const StyledSegment = styled.div`
  &>.segment {
    max-height: 250px;
    overflow: auto;
  }
`;

const ReceivedTransactions = ({ transactions, transactionsLoading }) => (
  <Container>
    <Header as="h3">
      Received Transactions
      <Header.Subheader>
        Client has sent the money for these transactions
        and is waiting for the exchanged amount to be sent.
      </Header.Subheader>
    </Header>
    <StyledSegment>
      <Segment>
        <ReceivedTransactionTable
          transactions={transactions}
          loading={transactionsLoading}
        />
      </Segment>
    </StyledSegment>
  </Container>
);

ReceivedTransactions.propTypes = {
  transactionsLoading: PropTypes.bool,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  })),
};

ReceivedTransactions.defaultProps = {
  transactions: [],
  transactionsLoading: true,
};

const mapDataToProps = () => {
  const userId = Meteor.userId();
  if (userId) {
    const transactionHandle = Meteor.subscribe('transactions.all.received', { userId });
    const transactionsLoading = !transactionHandle.ready();
    const transactions = Transactions.find({
      status: 'received',
    }).fetch();
    return { transactionsLoading, transactions };
  }
  return {};
};

export default withTracker(mapDataToProps)(ReceivedTransactions);
