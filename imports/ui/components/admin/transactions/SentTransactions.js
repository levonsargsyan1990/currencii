import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Container, Header, Segment,
} from 'semantic-ui-react';

import SentTransactionTable from './SentTransactionTable';
import { Transactions } from '../../../../api/transactions/transactions';

const StyledSegment = styled.div`
  &>.segment {
    max-height: 250px;
    overflow: auto;
  }
`;

const SentTransactions = ({ transactions, transactionsLoading }) => (
  <Container>
    <Header as="h3">
      Executed Transactions
      <Header.Subheader>
        You have sent the exchanged money to client.
        As soon as they receive it, transaction will be completed.
      </Header.Subheader>
    </Header>
    <StyledSegment>
      <Segment>
        <SentTransactionTable
          transactions={transactions}
          loading={transactionsLoading}
        />
      </Segment>
    </StyledSegment>
  </Container>
);

SentTransactions.propTypes = {
  transactionsLoading: PropTypes.bool,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    reference: PropTypes.string.isRequired,
    sell: PropTypes.shape({}),
    buy: PropTypes.shape({}),
    marketRate: PropTypes.number.isRequired,
    guaranteedRate: PropTypes.number.isRequired,
    bankAccount: PropTypes.shape({
      accountNumber: PropTypes.string.isRequired,
    }).isRequired,
    receivedAt: PropTypes.instanceOf(Date),
  })),
};

SentTransactions.defaultProps = {
  transactions: [],
  transactionsLoading: true,
};

const mapDataToProps = () => {
  const userId = Meteor.userId();
  if (userId) {
    const transactionHandle = Meteor.subscribe('transactions.all.sent', { userId });
    const transactionsLoading = !transactionHandle.ready();
    const transactions = Transactions.find({
      status: 'sent',
    }).fetch();
    return { transactionsLoading, transactions };
  }
  return {};
};

export default withTracker(mapDataToProps)(SentTransactions);
