import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Table, Icon } from 'semantic-ui-react';

import { enableRate, disableRate } from '../../../../api/rates/methods';

import UpdateRateModal from './UpdateRateModal';

const LargeText = styled.span`
  text-transform: uppercase;
  font-weight: 600;
`;

const SmallText = styled.p`
  white-space: nowrap;
  font-size: 10px;
`;

const AnchorStyledText = styled.span`
  color: ${props => props.theme.anchorBlue};
  cursor: pointer;
  text-decoration: underline;
  padding: 0 1rem;
`;

const ErrorStyledText = styled.span`
  color: ${props => props.theme.red};
  cursor: pointer;
  text-decoration: underline;
  float: right;
`;

const SuccessStyledText = styled.span`
  color: ${props => props.theme.green};
  cursor: pointer;
  text-decoration: underline;
  float: right;
`;

class RateRow extends Component {
  state = {
    isUpdateModalOpen: false,
  };

  handleUpdateModalToggle() {
    const { isUpdateModalOpen } = this.state;
    this.setState({ isUpdateModalOpen: !isUpdateModalOpen });
  }

  render () {
    const { isUpdateModalOpen } = this.state;
    const { rate } = this.props;
    const {
      _id, buy, sell, guaranteedRate, marketRate, active,
    } = rate;
    return (
      <>
        <Table.Row key={_id}>
          <Table.Cell disabled={!active}>
            <LargeText>{buy.currency}</LargeText>
            <SmallText>{`(${buy.fullName})`}</SmallText>
          </Table.Cell>
          <Table.Cell disabled={!active}><Icon name="exchange" /></Table.Cell>
          <Table.Cell disabled={!active}>
            <LargeText>{sell.currency}</LargeText>
            <SmallText>{`(${sell.fullName})`}</SmallText>
          </Table.Cell>
          <Table.Cell disabled={!active}>{guaranteedRate.rate}</Table.Cell>
          <Table.Cell disabled={!active}>{marketRate.rate}</Table.Cell>
          <Table.Cell collapsing>
            <AnchorStyledText onClick={() => this.handleUpdateModalToggle()}>
              Edit
            </AnchorStyledText>
            <UpdateRateModal
              rate={rate}
              onClose={() => this.handleUpdateModalToggle()}
              isOpen={isUpdateModalOpen}
            />
          </Table.Cell>
          <Table.Cell collapsing>
            {active ? (
              <ErrorStyledText onClick={() => disableRate.callPromise({ rateId: _id })}>
                Disable
              </ErrorStyledText>
            ) : (
              <SuccessStyledText onClick={() => enableRate.callPromise({ rateId: _id })}>
                Enable
              </SuccessStyledText>
            )}
          </Table.Cell>
        </Table.Row>
      </>
    );
  }
}

RateRow.propTypes = {
  rate: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    sell: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    buy: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    guaranteedRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
    marketRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
  }).isRequired,
};

export default RateRow;
