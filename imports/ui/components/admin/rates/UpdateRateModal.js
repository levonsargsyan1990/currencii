import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Formik } from 'formik';
import {
  Form, Label, Button, Table,
  Divider, Container, Modal,
  Message, Icon,
} from 'semantic-ui-react';

import { updateRate } from '../../../../api/rates/methods';

const StyledError = styled.div`
  .message {
    margin: 0 !important;
  }
`;

const StyledTable = styled.div`
  .ui.table tr td {
    border-top: 0px !important;
  }
`;

class UpdateRateModal extends Component {
  state = {
    error: '',
  };

  render() {
    const {
      rate, isOpen, onClose,
    } = this.props;
    const {
      _id, sell, buy, marketRate, guaranteedRate,
    } = rate;
    const { error } = this.state;

    return (
      <Formik
        initialValues={{
          marketRate: marketRate.rate,
          guaranteedRate: guaranteedRate.rate,
        }}
        validateOnBlur={false}
        validate={(values) => {
          const errors = {};
          if (!values.marketRate) {
            errors.marketRate = 'Please enter the market rate';
          }
          if (!values.guaranteedRate) {
            errors.guaranteedRate = 'Please enter the guaranteed rate';
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={(values, { setSubmitting }) => {
          const params = { rateId: _id };
          if (values.marketRate !== marketRate.rate) {
            params.marketRate = values.marketRate;
          }
          if (values.guaranteedRate !== guaranteedRate.rate) {
            params.guaranteedRate = values.guaranteedRate;
          }
          updateRate.callPromise(params)
            .then(setSubmitting.bind(null, false))
            .then(onClose)
            .catch((err) => {
              const message = err.reason.toLowerCase() || 'please try again later';
              setSubmitting(false);
              this.setState({
                error: `Failed to update the rate: ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values,
          errors, touched, isValid, isSubmitting,
        }) => (
          <Modal
            size="tiny"
            closeIcon
            dimmer="blurring"
            open={isOpen}
            onClose={onClose}
            closeOnDimmerClick={false}
          >
            <Modal.Header>Update Rate</Modal.Header>
            <Modal.Content>
              <Form onSubmit={handleSubmit} autoComplete="off">
                <StyledTable>
                  <Table stackable basic="very">
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell textAlign="right">{buy.currency.toUpperCase()}</Table.Cell>
                        <Table.Cell collapsing><Icon name="exchange" /></Table.Cell>
                        <Table.Cell>{sell.currency.toUpperCase()}</Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </StyledTable>
                <Form.Field>
                  <label>
                    Guaranteed Rate
                  </label>
                  <input
                    type="number"
                    name="guaranteedRate"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.guaranteedRate}
                  />
                  {errors.guaranteedRate && touched.guaranteedRate && (
                    <Label pointing basic color="red">
                      {errors.guaranteedRate}
                    </Label>
                  )}
                </Form.Field>
                <Form.Field>
                  <label>
                    Market Rate
                  </label>
                  <input
                    type="number"
                    name="marketRate"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.marketRate}
                  />
                  {errors.marketRate && touched.marketRate && (
                    <Label pointing basic color="red">
                      {errors.marketRate}
                    </Label>
                  )}
                </Form.Field>
                <Divider hidden />
                <Container textAlign="center">
                  <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">Save</Button>
                  <Button disabled={isSubmitting} type="button" onClick={onClose}>Cancel</Button>
                </Container>
              </Form>
            </Modal.Content>
            <StyledError>
              <Message attached="bottom" error hidden={!error}>
                <Icon name="warning" />
                {error}
              </Message>
            </StyledError>
          </Modal>
        )}
      />
    );
  }
}

UpdateRateModal.propTypes = {
  rate: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    sell: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    buy: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    marketRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }).isRequired,
    guaranteedRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }).isRequired,
    active: PropTypes.bool.isRequired,
  }).isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

UpdateRateModal.defaultProps = {
  isOpen: false,
  onClose: () => {},
};

export default UpdateRateModal;
