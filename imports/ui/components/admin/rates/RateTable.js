import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Table, Loader, Segment } from 'semantic-ui-react';

import { Rates } from '../../../../api/rates/rates';

import RateRow from './RateRow';

const RateTable = ({ rates, loading }) => {
  if (loading) {
    return <Loader size="large" />;
  }
  const rateRows = rates.map(rate => (
    <RateRow key={rate._id} rate={rate} />
  ));
  if (rates.length === 0) {
    return <p>No rates</p>;
  }
  return (
    <Segment>
      <Table textAlign="center" basic="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell collapsing>Buy</Table.HeaderCell>
            <Table.HeaderCell collapsing />
            <Table.HeaderCell collapsing>Sell</Table.HeaderCell>
            <Table.HeaderCell>Guaranteed Rate</Table.HeaderCell>
            <Table.HeaderCell>Market Rate</Table.HeaderCell>
            <Table.HeaderCell collapsing />
            <Table.HeaderCell collapsing />
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {rateRows}
        </Table.Body>
      </Table>
    </Segment>
  );
};

RateTable.propTypes = {
  loading: PropTypes.bool,
  rates: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string.isRequired,
    sell: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    buy: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    guaranteedRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
    marketRate: PropTypes.shape({
      rate: PropTypes.number.isRequired,
      updatedAt: PropTypes.instanceOf(Date).isRequired,
    }),
    active: PropTypes.bool.isRequired,
  })),
};

RateTable.defaultProps = {
  loading: true,
  rates: [],
};

const mapDataToProps = () => {
  const ratesHandle = Meteor.subscribe('rates.list');
  const ratesLoading = !ratesHandle.ready();
  const rates = Rates.find().fetch();
  return { loading: ratesLoading, rates };
};

export default withTracker(mapDataToProps)(RateTable);
