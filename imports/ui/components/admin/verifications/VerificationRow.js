import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, Button } from 'semantic-ui-react';

import VerificationDetail from './VerificationDetail';

class VerificationRow extends Component {
  state= {
    isOpen: false,
  }

  handleRowClick() {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  }

  render() {
    const { isOpen } = this.state;
    const { user } = this.props;
    const {
      _id,
      profile: {
        firstName,
        lastName,
        companyName,
        position,
      },
    } = user;

    return (
      <>
        <Table.Row key={_id}>
          <Table.Cell>{`${firstName} ${lastName}`}</Table.Cell>
          <Table.Cell>{companyName}</Table.Cell>
          <Table.Cell>{position}</Table.Cell>
          <Table.Cell collapsing>
            <Button
              size="mini"
              basic
              primary
              onClick={() => this.handleRowClick()}
            >
              {isOpen ? 'Hide' : 'Show'}
            </Button>
          </Table.Cell>
        </Table.Row>
        {isOpen ? <VerificationDetail user={user} /> : null}
      </>
    );
  }
}

VerificationRow.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    emails: PropTypes.arrayOf(PropTypes.shape({
      address: PropTypes.string.isRequired,
    })),
    profile: PropTypes.shape({
      companyName: PropTypes.string.isRequired,
      country: PropTypes.string.isRequired,
      city: PropTypes.string.isRequired,
      state: PropTypes.string,
      address: PropTypes.string.isRequired,
      postalCode: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      position: PropTypes.string.isRequired,
      phoneCode: PropTypes.string.isRequired,
      phoneNumber: PropTypes.string.isRequired,
    }),
  }).isRequired,
};

export default VerificationRow;
