import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Grid, Image, Button, Loader, Modal, Message, Icon,
} from 'semantic-ui-react';

import { approveVerification, declineVerification } from '../../../../api/users/methods';
import { sendApprovalEmail, sendDeclineEmail } from '../../../../api/email/methods';
import { capitalize } from '../../../../lib/helpers';

import { Images } from '../../../../api/images/images';

const StyledImage = styled.div`
  img {
    cursor: pointer;
    width: 100%;
  }
`;

const StyledError = styled.div`
  margin-top: 15px;
  text-align: left;
`;

class VerificationImage extends Component {
  state= {
    error: '',
    approveLoading: false,
    declineLoading: false,
    isModalOpen: false,
  }

  handleModalToggle() {
    const { isModalOpen } = this.state;
    this.setState({ isModalOpen: !isModalOpen });
  }

  handleApprove () {
    this.setState({ approveLoading: true });
    const { userId, fileId, type } = this.props;
    approveVerification.callPromise({ userId, fileId, verificationType: type })
      .then(() => sendApprovalEmail.callPromise({ userId, verificationType: type }))
      .catch((err) => {
        const message = (err.reason && err.reason.toLowerCase()) || 'please try again later';
        this.setState({
          approveLoading: false,
          error: `Failed to approve: ${message}`,
        });
      });
  }

  handleDecline () {
    this.setState({ declineLoading: true });
    const { userId, fileId, type } = this.props;
    declineVerification.callPromise({ userId, fileId, verificationType: type })
      .then(() => sendDeclineEmail.callPromise({ userId, verificationType: type }))
      .catch((err) => {
        const message = (err.reason && err.reason.toLowerCase()) || 'please try again later';
        this.setState({
          declineLoading: false,
          error: `Failed to decline: ${message}`,
        });
      });
  }

  render() {
    const {
      isModalOpen, error, approveLoading, declineLoading,
    } = this.state;
    const {
      type, image, loading,
    } = this.props;
    if (loading) {
      return (
        <Grid.Column>
          <Loader />
        </Grid.Column>
      );
    }

    return (
      <>
        <Grid.Column>
          <StyledImage>
            <Image
              src={image.url()}
              alt={`${capitalize(type)} verfication`}
              onClick={() => this.handleModalToggle()}
            />
          </StyledImage>
        </Grid.Column>
        <Grid.Column verticalAlign="middle" textAlign="center">
          <p>
            {`${capitalize(type)} verfication`}
          </p>
          <div className="ui two buttons">
            <Button
              basic
              loading={approveLoading}
              color="green"
              onClick={() => this.handleApprove()}
            >
              Approve
            </Button>
            <Button
              basic
              loading={declineLoading}
              color="red"
              onClick={() => this.handleDecline()}
            >
              Decline
            </Button>
          </div>
          <StyledError>
            <Message error hidden={!error}>
              <Icon name="warning" />
              {error}
            </Message>
          </StyledError>
        </Grid.Column>

        <Modal
          size="large"
          dimmer="blurring"
          open={isModalOpen}
          onClose={() => this.handleModalToggle()}
        >
          <Modal.Header>
            {`${capitalize(type)} verfication`}
          </Modal.Header>
          <Modal.Content>
            <StyledImage>
              <Image
                src={image.url()}
                alt={`${capitalize(type)} verfication`}
              />
            </StyledImage>
          </Modal.Content>
        </Modal>
      </>
    );
  }
}

VerificationImage.propTypes = {
  image: PropTypes.shape({}),
  loading: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired,
  fileId: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

VerificationImage.defaultProps = {
  image: null,
};

const mapDataToProps = ({ fileId }) => {
  const imagesHandle = Meteor.subscribe('images.one', { fileId });
  const imagesLoading = !imagesHandle.ready();
  const image = Images.findOne({ _id: fileId });
  return { loading: imagesLoading, image };
};

export default withTracker(mapDataToProps)(VerificationImage);
