import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Table, Grid } from 'semantic-ui-react';

import VerificationImage from './VerificationImage';

const StyledGrid = styled.div`
  .column:nth-child(even) {
    box-shadow: none !important;
  }
`;

const VerificationDetail = ({
  user: {
    _id,
    profile: {
      verification: {
        business, identity,
      },
    },
  },
}) => {
  const isIdentityInProgress = (identity.status === 'in-progress') && identity.fileId;
  const isBusinessInProgress = (business.status === 'in-progress') && business.fileId;
  return (
    <Table.Row key={`${_id}-detail`}>
      <Table.Cell colSpan={4}>
        <StyledGrid>
          <Grid verticalAlign="middle" columns={4} divided>
            <Grid.Row>
              {isIdentityInProgress ? <VerificationImage fileId={identity.fileId} userId={_id} type="identity" /> : null}
              {isBusinessInProgress ? <VerificationImage fileId={business.fileId} userId={_id} type="business" /> : null}
            </Grid.Row>
          </Grid>
        </StyledGrid>
      </Table.Cell>
    </Table.Row>
  );
};

VerificationDetail.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    profile: PropTypes.shape({
      verification: PropTypes.shape({
        business: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
        identity: PropTypes.shape({
          status: PropTypes.string.isRequired,
        }),
      }),
    }),
  }).isRequired,
};

export default VerificationDetail;
