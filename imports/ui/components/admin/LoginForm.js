import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Formik } from 'formik';
import styled from 'styled-components';
import {
  Header, Divider, Form, Label, Button, Container, Message, Icon,
} from 'semantic-ui-react';
import { allowAdminLogin } from '../../../api/users/methods';

import { emailRegex, passwordRegex } from '../../../lib/constants';

const StyledError = styled.div`
  margin-top: 15px;
`;

class LoginForm extends Component {
  state = {
    error: '',
  };

  // eslint-disable-next-line class-methods-use-this
  login({ email, password }) {
    return new Promise((resolve, reject) => {
      Meteor.loginWithPassword(email, password, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  render() {
    const { error } = this.state;
    return (
      <Formik
        initialValues={{ email: '', password: '' }}
        validate={(values) => {
          const errors = {};
          if (!values.email) {
            errors.email = 'Please enter email';
          } else if (!emailRegex.test(values.email)) {
            errors.email = 'Please enter a email';
          }
          if (!values.password) {
            errors.password = 'Please enter a password';
          } else if (!passwordRegex.test(values.password)) {
            errors.password = 'Must contain minimum eight characters, at least one uppercase letter, one lowercase letter and one number';
          }
          return errors;
        }}
        isInitialValid={({ validate, initialValues }) => {
          const errors = validate(initialValues);
          if (Object.keys(errors).length > 0) {
            return false;
          }
          return true;
        }}
        onSubmit={({ email, password }, { setSubmitting }) => {
          allowAdminLogin.callPromise({ email })
            .then(this.login.bind(this, { email, password }))
            .catch((err) => {
              const message = err.reason.toLowerCase() || 'please try again later';
              setSubmitting(false);
              this.setState({
                error: `Failed to login: ${message}`,
              });
            });
        }}
        render={({
          handleSubmit, handleChange, handleBlur, values, errors, touched, isValid, isSubmitting,
        }) => (
          <div>
            <Form onSubmit={handleSubmit}>
              <Header as="h3">
                Admin Login
              </Header>
              <Divider hidden />
              <Form.Field error={errors.email && touched.email}>
                <label>
                  Email
                </label>
                <input
                  type="text"
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                />
                {errors.email && touched.email && (
                  <Label pointing basic color="red">
                    {errors.email}
                  </Label>
                )}
              </Form.Field>
              <Form.Field>
                <label>
                  Password
                </label>
                <input
                  type="password"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                {errors.password && touched.password && (
                  <Label pointing basic color="red">
                    {errors.password}
                  </Label>
                )}
              </Form.Field>
              <Container textAlign="center">
                <Button disabled={!isValid} loading={isSubmitting} type="submit" color="green">Login</Button>
              </Container>
            </Form>
            <Container>
              <StyledError>
                <Message error hidden={!error}>
                  <Icon name="warning" />
                  {error}
                </Message>
              </StyledError>
            </Container>
          </div>
        )}
      />
    );
  }
}


export default LoginForm;
