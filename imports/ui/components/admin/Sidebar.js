import React from 'react';
import styled from 'styled-components';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import {
  Sidebar as SemanticSidebar, Menu, Icon,
} from 'semantic-ui-react';

const StyledSidebar = styled.div`
  .sidebar {
    padding-top: 52px;
  }
`;

const Sidebar = ({ history }) => {
  const logout = () => Meteor.logout();

  return (
    <StyledSidebar>
      <SemanticSidebar
        fixed="left"
        as={Menu}
        animation="overlay"
        icon="labeled"
        vertical
        visible
        width="thin"
      >
        <Menu.Item as="a" onClick={() => history.push('/admin/transactions')}>
          <Icon name="exchange" />
          Transactions
        </Menu.Item>
        <Menu.Item as="a" onClick={() => history.push('/admin/rates')}>
          <Icon name="money bill alternate outline" />
          Rates
        </Menu.Item>
        <Menu.Item as="a" onClick={() => history.push('/admin/verifications')}>
          <Icon name="calendar check outline" />
          Verifications
        </Menu.Item>
        <Menu.Item as="a" onClick={() => history.push('/admin/users')}>
          <Icon name="users" />
          Users
        </Menu.Item>
        <Menu.Item as="a" onClick={logout}>
          <Icon name="log out" />
          Logout
        </Menu.Item>
      </SemanticSidebar>
    </StyledSidebar>
  );
};

Sidebar.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
};

export default withRouter(Sidebar);
