import React, { Component } from 'react';
import { Input, Icon } from 'semantic-ui-react';

export class ExposablePassword extends Component {
  state = {
    isVisible: false,
  }

  toggleVisibility() {
    const { isVisible } = this.state;
    this.setState({ isVisible: !isVisible });
  }

  render() {
    const { isVisible } = this.state;
    return (
      <Input
        {...this.props}
        icon={<Icon name="eye" onClick={() => this.toggleVisibility()} circular link />}
        type={isVisible ? 'text' : 'password'}
      />
    );
  }
}
