// TODO improve bank account number regex
// TODO Remove hardcoded currency options

export const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
export const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/;
export const amPostalCodeRegex = /^[0-9]{4}$/;
export const lvPostalCodeRegex = /^(LV-)[0-9]{4}$/;
export const nameRegex = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;
export const phoneNumberRegex = /^[0-9]*$/;
export const ibanRegex = /^([A-Z]{2}[ \-]?[0-9]{2})(?=(?:[ \-]?[A-Z0-9]){9,30}$)((?:[ \-]?[A-Z0-9]{3,5}){2,7})([ \-]?[A-Z0-9]{1,3})?$/;
export const swiftRegex = /^[A-Z]{6}[A-Z0-9]{2}([A-Z0-9]{3})?$/;
export const bankAccountNumberRegex = /^[0-9]*$/;
export const numberRegex = /^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$/;

// List of country options supported
export const countryOptions = [
  {
    key: 'am', value: 'Armenia', text: 'Armenia',
  },
  {
    key: 'lv', value: 'Latvia', text: 'Latvia',
  },
];

// List of phone code options supported
export const phoneCodesOptions = [
  {
    key: 'am', value: '+374', text: '(+374)',
  },
  {
    key: 'lv', value: '+371', text: '(+371)',
  },
];

// List of currency options supported
export const currencyOptions = [
  {
    key: 'usd',
    flag: 'us',
    value: 'usd',
    text: 'USD',
  },
  {
    key: 'eur',
    flag: 'eu',
    value: 'eur',
    text: 'EUR',
  },
  {
    key: 'gbp',
    flag: 'gb',
    value: 'gbp',
    text: 'GBP',
  },
  {
    key: 'sek',
    flag: 'se',
    value: 'sek',
    text: 'SEK',
  },
  {
    key: 'aed',
    flag: 'uae',
    value: 'aed',
    text: 'AED',
  },
  {
    key: 'aud',
    flag: 'au',
    value: 'aud',
    text: 'AUD',
  },
  {
    key: 'bgn',
    flag: 'bg',
    value: 'bgn',
    text: 'BGN',
  },
  {
    key: 'cad',
    flag: 'ca',
    value: 'cad',
    text: 'CAD',
  },
  {
    key: 'chf',
    flag: 'ch',
    value: 'chf',
    text: 'CHF',
  },
  {
    key: 'cny',
    flag: 'cn',
    value: 'cny',
    text: 'CNY',
  },
  {
    key: 'czk',
    flag: 'cz',
    value: 'czk',
    text: 'CZK',
  },
  {
    key: 'dkk',
    flag: 'dk',
    value: 'dkk',
    text: 'DKK',
  },
  {
    key: 'hkd',
    flag: 'hk',
    value: 'hkd',
    text: 'HKD',
  },
  {
    key: 'hrk',
    flag: 'hr',
    value: 'hrk',
    text: 'HRK',
  },
  {
    key: 'huf',
    flag: 'hu',
    value: 'huf',
    text: 'HUF',
  },
  {
    key: 'ils',
    flag: 'il',
    value: 'ils',
    text: 'ILS',
  },
  {
    key: 'inr',
    flag: 'in',
    value: 'inr',
    text: 'INR',
  },
  {
    key: 'jpy',
    flag: 'jp',
    value: 'jpy',
    text: 'JPY',
  },
  {
    key: 'mxn',
    flag: 'mx',
    value: 'mxn',
    text: 'MXN',
  },
  {
    key: 'nok',
    flag: 'no',
    value: 'nok',
    text: 'NOK',
  },
  {
    key: 'nzd',
    flag: 'nz',
    value: 'nzd',
    text: 'NZD',
  },
  {
    key: 'pln',
    flag: 'pl',
    value: 'pln',
    text: 'PLN',
  },
  {
    key: 'ron',
    flag: 'ro',
    value: 'ron',
    text: 'RON',
  },
  {
    key: 'rub',
    flag: 'ru',
    value: 'rub',
    text: 'RUB',
  },
  {
    key: 'sgd',
    flag: 'sg',
    value: 'sgd',
    text: 'SGD',
  },
  {
    key: 'thb',
    flag: 'th',
    value: 'thb',
    text: 'THB',
  },
  {
    key: 'try',
    flag: 'tr',
    value: 'try',
    text: 'TRY',
  },
  {
    key: 'zar',
    flag: 'za',
    value: 'zar',
    text: 'ZAR',
  },
];
