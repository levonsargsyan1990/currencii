import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { Rates } from '../../api/rates/rates';

// Populating database with initial rates
Meteor.startup(() => {
  // Populating database with initial rates
  if (Rates.find().count() === 0) {
    const rates = [
      {
        sell: {
          currency: 'usd',
          fullName: 'US Dollar',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'gbp',
          fullName: 'British Pound',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'pln',
          fullName: 'Polish Zloty',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'amd',
          fullName: 'Armenian Dram',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'amd',
          fullName: 'Armenian Dram',
        },
        buy: {
          currency: 'usd',
          fullName: 'US Dollar',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'amd',
          fullName: 'Armenian Dram',
        },
        buy: {
          currency: 'gbp',
          fullName: 'British Pound',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'amd',
          fullName: 'Armenian Dram',
        },
        buy: {
          currency: 'rub',
          fullName: 'Russian Rouble',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'sek',
          fullName: 'Swedish Krona',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'aed',
          fullName: 'Dirham of United Arab Emirates',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'aud',
          fullName: 'Australian Dollar',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'bgn',
          fullName: 'Bulgaria Leva',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'cad',
          fullName: 'Canadian Dollar',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'chf',
          fullName: 'Swiss Franc',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'cny',
          fullName: 'China Yuan Renminbi',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'czk',
          fullName: 'Czech Koruna',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'dkk',
          fullName: 'Danish Krone',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'hkd',
          fullName: 'Hong Kong Dollar',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'hrk',
          fullName: 'Croatian Kuna',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'huf',
          fullName: 'Hungarian Forint',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'ils',
          fullName: 'New Israeli Sheqel',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'inr',
          fullName: 'Indian Rupee',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'jpy',
          fullName: 'Japanese Yen',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'mxn',
          fullName: 'Mexican Peso',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'nok',
          fullName: 'Norwegian Kroner',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'nzd',
          fullName: 'New Zealand Dollar',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'ron',
          fullName: 'Romania Lei',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'rub',
          fullName: 'Russian Rouble',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'sgd',
          fullName: 'Singapore Dollar',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'thb',
          fullName: 'Thailand Baht',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'try',
          fullName: 'Turkish Lira',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
      {
        sell: {
          currency: 'zar',
          fullName: 'South African Rand',
        },
        buy: {
          currency: 'eur',
          fullName: 'Euro',
        },
        guaranteedRate: {
          rate: 0.9,
        },
        marketRate: {
          rate: 0.88,
        },
      },
    ];

    const directRates = [...rates];
    const oppositeRates = rates.map(rate => ({ ...rate, sell: rate.buy, buy: rate.sell }));

    // Adding rates
    directRates.forEach(rate => Rates.insert(rate));
    // Adding opposite rates
    oppositeRates.forEach(rate => Rates.insert(rate));
  }

  if (Roles.getUsersInRole('admin').count() === 0) {
    const { email, password } = Meteor.settings.private.admin;
    const adminId = Accounts.createUser({ email, password });
    Meteor.users.update(adminId, {
      $set: {
        'emails.0.verified': true,
      },
    });
    Roles.addUsersToRoles(adminId, 'admin', Roles.GLOBAL_GROUP);
  }
});
