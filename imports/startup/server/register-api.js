// Importing email configurations and methods
import '../../api/email/email.js';
import '../../api/email/methods.js';

// Importing 'users' collection's methods and publications
import '../../api/users/methods.js';
import '../../api/users/server/publications.js';

// Importing 'images' collection's methods and publications
import '../../api/images/methods.js';
import '../../api/images/server/publications.js';

// Importing 'bankAccounts' colection
import '../../api/bankAccounts/methods.js';
import '../../api/bankAccounts/server/publications.js';

// Importing 'rates' colection
import '../../api/rates/methods.js';
import '../../api/rates/server/publications.js';

// Importing 'transactions' colection
import '../../api/transactions/methods.js';
import '../../api/transactions/server/publications.js';
