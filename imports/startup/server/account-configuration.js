import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

const { title, email } = Meteor.settings.public;

Accounts.validateLoginAttempt(function({ user, error }) {
  // Check the user's email is verified. If users may have multiple
  // email addresses (or no email address) you'd need to do something
  // more complex.
  if (error) {
    throw new Meteor.Error('login-error', error.reason);
  }
  if (user && user.emails[0].verified === true) {
    return true;
  }
  throw new Meteor.Error('email-not-verified', 'You must verify your email address before you can log in');
});

// Email settings
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

Accounts.emailTemplates.from = `${title} <${email}>`;

// Application name
Accounts.emailTemplates.siteName = title;

// Token expiration date in days
Accounts.config({
  loginExpirationInDays: 1,
});

Accounts.emailTemplates.verifyEmail.subject = () => 'Verify your new email';

Accounts.emailTemplates.verifyEmail.html = (user, url) => `
  <div>
    <p>
      Hello ${user.profile.firstName},
    </p>
    <br />
    <p>
      You changed your email address in ${title}. 
      In order your changes to take place, please verify your 
      your new email by clicking the link below.
    </p>
    <br />
    <a href="${url}">
      ${url}
    </a>
    <br />
    <p>
      Thanks
    </p>
  </div>`;

Accounts.urls.verifyEmail = token => Meteor.absoluteUrl(`verify-email/${token}`);

Accounts.emailTemplates.resetPassword.subject = () => 'Reset Password';

Accounts.emailTemplates.resetPassword.html = (user, url) => `
  <div>
    <p>
      Hello ${user.profile.firstName},
    </p>
    <br />
    <p>
      You can reset your password in ${title} by clicking the link below.
      If this email was sent to you by mistake, just ignore it.
    </p>
    <br />
    <a href="${url}">
      ${url}
    </a>
    <br />
    <p>
      Thanks
    </p>
  </div>`;

Accounts.urls.resetPassword = token => Meteor.absoluteUrl(`reset-password/${token}`);
