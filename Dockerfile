FROM lucidprogrammer/meteor-base:1.9 as build

WORKDIR /app

COPY . /app

RUN meteor npm i

EXPOSE 3000

RUN echo "$METEOR_SETTINGS"

RUN echo "$METEOR_SETTINGS" > settings.json

CMD ["meteor", "--settings", "settings.json"]